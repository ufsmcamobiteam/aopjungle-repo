**********************************************************************************************************
*
*  Instructions to install AOPJungle Eclipse Plugin
*  Author : Cristiano De Faveri
*  Department of Programming Languages and Database of Federal University of Santa Maria
*
*  This tool is part of the thesis developed by Cristiano De Faveri. Copies, distribution and its use
*  are only allowed under authorization of the author.
*
**********************************************************************************************************

This tutorial assumes :

a) You have some familiarity with Eclipse toolset.
b) You are using Eclipse 3.7 - Indigo, although it might work on further versions.

1. Install a SVN Client to checkout the AOPJungle project. SubEclipse can be a good choice
   Help, Install New Software -> http://subclipse.tigris.org/update_1.6.x/
   
2. Checkout AOPJungle from www.defavari.com.br/svn/ufsm/AOPJungle. 
   File, New, Other, SVN, Checkout Projects From SVN. Create a New Repository and inform the above URL.
   Inform the user and password Author sent to you.
   
3. Choose to Create a New Project from a Project Wizard and choose to create a Java Project.
   Name the project AOPJungle. Leave other options default.
   Click Finish and the project will be checked out. At this point, many compilation errors will arise. 

4. Configuring Build Libraries:
   Right click on the project name and choose build path, configure build path.
   Choose Libraries, Add external Libraries and choose log4j-1.2.14.jar, xpp3_min-1.1.4c.jar, xstream-1.4.2.jar
   from /lib of the project folder.
   
5. Convert project to Plugin Project:
   Right click on the project name and choose Configure, Convert Project to Plugin Projects. 
   Choose, AOPJungle and click Finish. The necessary libraries will be included.
       
4. Open folder META-INF, then file MANIFEST.MF and input :
   ==> Overview
   ID : AOPJungle
   Version : 1.0.0.qualifier
   Name : AOPJungle
   Activator : br.ufsm.aopjungle.Activator
   Enable : Activate this plugin when one of its classes is loaded
   Enable : This plugin is a singleton
   
   ==> Dependencies 
   org.eclipse.ui,
   org.eclipse.core.runtime,
   org.eclipse.core.resources;bundle-version="3.6.1",
   org.eclipse.jdt.core;bundle-version="3.6.2",
   org.eclipse.ajdt.core;bundle-version="2.1.2",
   org.apache.log4j;bundle-version="1.2.15"
   
   ==> Runtime, Classpath
   Add all jars from lib folder
   
   ==> Extensions
   Add org.eclipse.ui.view (This will create plugin.xml. Without this extension the plugin is not loaded)    
   
5. You should be able to run AOPJungle as an Eclipse Application.
   On the new Eclipse environment,open show view windows and choose AOPJungle view.
   
 Appendice :
 
 Manisfest file should look like :
 Manifest-Version: 1.0
Bundle-ManifestVersion: 2
Bundle-Name: AOPJungle
Bundle-SymbolicName: AOPJungle;singleton:=true
Bundle-Version: 1.0.0.qualifier
Require-Bundle: org.eclipse.ui;bundle-version="3.7.0",
 org.eclipse.core.runtime;bundle-version="3.7.0",
 org.eclipse.core.resources;bundle-version="3.7.100",
 org.eclipse.jdt.core;bundle-version="3.7.1",
 org.eclipse.ajdt.core;bundle-version="2.1.3",
 org.apache.log4j;bundle-version="1.2.13"
Bundle-ClassPath: .,
 libs/antlr-2.7.6.jar,
 libs/commons-collections-3.1.jar,
 libs/dom4j-1.6.1.jar,
 libs/hibernate-jpa-2.0-api-1.0.1.Final.jar,
 libs/hibernate3.jar,
 libs/hsqldb.jar,
 libs/javassist-3.12.0.GA.jar,
 libs/jta-1.1.jar,
 libs/log4j-1.2.14.jar,
 libs/slf4j-api-1.6.1.jar,
 libs/xpp3_min-1.1.4c.jar,
 libs/xstream-1.4.2.jar
Bundle-ActivationPolicy: lazy
Bundle-Activator: br.ufsm.aopjungle.Activator

plugin.xml should look like
<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.4"?>
<plugin>
   <extension
         point="org.eclipse.ui.views">
      <category
            name="AOPJungle"
            id="AOPJungle">
      </category>
      <view
            name="AOPJungle Browser"
            icon="icons/sample.gif"
            category="AOPJungle"
            class="br.ufsm.aopjungle.views.AOPJungleView"
            id="br.ufsm.aopjungle.views.AOPJungleView">
      </view>
   </extension>

</plugin>
 
build.properties file should look like
source.. = src/
output.. = bin/
bin.includes = META-INF/,\
               .,\
               libs/antlr-2.7.6.jar,\
               libs/commons-collections-3.1.jar,\
               libs/dom4j-1.6.1.jar,\
               libs/hibernate-jpa-2.0-api-1.0.1.Final.jar,\
               libs/hibernate3.jar,\
               libs/hsqldb.jar,\
               libs/javassist-3.12.0.GA.jar,\
               libs/jta-1.1.jar,\
               libs/log4j-1.2.14.jar,\
               libs/slf4j-api-1.6.1.jar,\
               libs/xpp3_min-1.1.4c.jar,\
               libs/xstream-1.4.2.jar

