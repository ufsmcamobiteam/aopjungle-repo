package br.ufsm.aopjungle.binding;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.MetaValue;

import br.ufsm.aopjungle.metamodel.aspectj.AOJAdviceDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareCompilationEnforcement;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareParents;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareSoft;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutCFlow;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJAttributeDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJContainerPlaceHolder;
import br.ufsm.aopjungle.metamodel.commons.AOJMember;
import br.ufsm.aopjungle.metamodel.commons.AOJMethodDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJAnnotationTypeDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJAnonymousClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJConstructorDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJEnumDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration;

@Entity
//@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class AOJBinding {
	@Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Binding_id")	
	private long id;
	private String name;
	protected Boolean affect;
	private String kind;
	private String relationshipKind;
    @Any(metaColumn = @Column(name = "object_link"))
    @AnyMetaDef(idType = "long", metaType = "string", metaValues = {
    @MetaValue(value = "ATR", targetEntity = AOJAttributeDeclaration.class),
    @MetaValue(value = "ADV", targetEntity = AOJAdviceDeclaration.class),
    @MetaValue(value = "CTR", targetEntity = AOJConstructorDeclaration.class),
    @MetaValue(value = "MET", targetEntity = AOJMethodDeclaration.class),
    @MetaValue(value = "DCE", targetEntity = AOJDeclareCompilationEnforcement.class),
    @MetaValue(value = "DEP", targetEntity = AOJDeclareParents.class),
    @MetaValue(value = "DES", targetEntity = AOJDeclareSoft.class),
    @MetaValue(value = "PCD", targetEntity = AOJPointcutDeclaration.class),
    @MetaValue(value = "PCC", targetEntity = AOJPointcutCFlow.class),
    @MetaValue(value = "CLA", targetEntity = AOJClassDeclaration.class),
    @MetaValue(value = "INT", targetEntity = AOJInterfaceDeclaration.class),
    @MetaValue(value = "ASP", targetEntity = AOJAspectDeclaration.class),
    @MetaValue(value = "ENU", targetEntity = AOJEnumDeclaration.class),
    @MetaValue(value = "PHL", targetEntity = AOJContainerPlaceHolder.class),
    @MetaValue(value = "ANN", targetEntity = AOJAnonymousClassDeclaration.class),
    @MetaValue(value = "ANO", targetEntity = AOJAnnotationTypeDeclaration.class)
     })
    @JoinColumn(name = "object_id")  
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
	private AOJMember object;
	private String bindingHandler;
	
	public AOJBinding() {

	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public AOJMember getObject() {
		return object;
	}

	public void setObject(AOJMember object) {
		this.object = object;
	}

	public String getBindingHandler() {
		return bindingHandler;
	}

	public void setBindingHandler(String handler) {
		this.bindingHandler = handler;
	}

	public String getRelationshipKind() {
		return relationshipKind;
	}

	public void setRelationshipKind(String relationshipKind) {
		this.relationshipKind = relationshipKind;
	}

}
