package br.ufsm.aopjungle.binding;

import org.eclipse.core.runtime.NullProgressMonitor;

public class AOJProgressMonitor extends NullProgressMonitor {
	private boolean isDone = false;
	@Override
	public void worked(int arg) {
		isDone = arg >= 0;
	}
	
	public boolean isDone() {
		return isDone;
	}
}
