package br.ufsm.aopjungle.binding;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

/**
 * Represents the in/out binding model <br>
 * <br>
 * in - Represents the modules that affects the current module/element <br>
 * out - Represents the modules the current module/element affects <br>
 * <br>
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@Entity
public class AOJBindingModel {
	@Id 
//    @GeneratedValue(strategy = GenerationType.TABLE)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "BindingModel_id")
	private long id;
	/**
	 * Other objects that affects this object
	 */
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name = "AOJBindingModel_Binding", 
		joinColumns = { @JoinColumn(name = "BindingModel_id") }, 
		inverseJoinColumns = { @JoinColumn(name = "Binding_id") })	
//	@JoinTable(name = "AOJBindingModel_in")
//	@Transient
	private List<AOJBinding> inBinding;
	
	/**
	 * Other objects which are affected by this object
	 */
	@ManyToMany(cascade=CascadeType.ALL)
//	@JoinTable(name = "AOJBindingModel_out")
//	@Transient
	private List<AOJBinding> outBinding;
	
	public AOJBindingModel() {

	}
	
	public long getNumberOfIn() {
		return getIn().size();
	}

	public long getNumberOfOut() {
		return getOut().size();
	}

	public List<AOJBinding> getIn() {
		if (null == inBinding)
			inBinding = new ArrayList<AOJBinding>();
		return inBinding;
	}
	
	public void setIn(List<AOJBinding> in) {
		this.inBinding = in;
	}
	
	public List<AOJBinding> getOut() {
		if (null == outBinding)
			outBinding = new ArrayList<AOJBinding>();
		return outBinding;
	}
	
	public void setOut(List<AOJBinding> out) {
		this.outBinding = out;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}