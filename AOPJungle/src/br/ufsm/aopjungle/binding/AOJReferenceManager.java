package br.ufsm.aopjungle.binding;

import java.util.HashMap;
import java.util.Map;

import br.ufsm.aopjungle.exception.StackObjectReferenceIllegalStateException;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareField;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareMethod;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareParents;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareSoft;
import br.ufsm.aopjungle.metamodel.commons.AOJBehaviourKind;
import br.ufsm.aopjungle.metamodel.commons.AOJMember;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;

/**
 * Holds maps for searching purposes
 * It's mainly used when relationship field filling is done after compilation 
 * 
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
public class AOJReferenceManager {
	/**
	 * This is singleton 
	 */
	private static AOJReferenceManager instance = new AOJReferenceManager();
	private Map<String, AOJMember> referenceMap = new HashMap<String, AOJMember>();
	
	public static AOJReferenceManager getInstance() {
		return instance;
	}
	
	public void addToMap (String handler, AOJMember member) {
		System.out.println("[Adding to map handler] " + handler);
		if ( (null != handler) && (null != member) && (! handler.isEmpty()) && (! referenceMap.containsKey(handler))) { 
			System.out.println("Added : " + handler);
			referenceMap.put(handler, member);
		}	
	}
	
	public Map<String, AOJMember> getReferenceMap() {
		return referenceMap;
	}
	
	public AOJTypeDeclaration getType (String handler) throws StackObjectReferenceIllegalStateException {
		AOJMember result = referenceMap.get(handler);
		if (null == result)
			return null;
		if (result instanceof AOJTypeDeclaration)
			return (AOJTypeDeclaration)result;
		throw new StackObjectReferenceIllegalStateException("Reference Map item does not match requested type : AOJTypeDeclaration. Current = " + result.getClass().getName() + " Handler : " + handler);
	}

	public AOJBehaviourKind getMethod (String handler) throws StackObjectReferenceIllegalStateException {
		AOJMember result = referenceMap.get(handler);
		if (null == result)
			return null;
		if (result instanceof AOJBehaviourKind)
			return (AOJBehaviourKind)result;
		throw new StackObjectReferenceIllegalStateException("Reference Map item does not match requested type : AOJBehaviourKind. Current = " + result.getClass().getName() + " Handler : " + handler);
	}

	public AOJDeclareField getInterTypeField(String handler) throws StackObjectReferenceIllegalStateException {
		AOJMember result = referenceMap.get(handler);
		if (null == result)
			return null;
		if (result instanceof AOJDeclareField)
			return (AOJDeclareField)result;
		throw new StackObjectReferenceIllegalStateException("Reference Map item does not match requested type : AOJDeclareField. Current = " + result.getClass().getName() + " Handler : " + handler);
	}
	
	public AOJDeclareMethod getInterTypeMethod(String handler) throws StackObjectReferenceIllegalStateException {
		AOJMember result = referenceMap.get(handler);
		if (null == result) {
			return null;
		}	
		if (result instanceof AOJDeclareMethod)
			return (AOJDeclareMethod)result;
		throw new StackObjectReferenceIllegalStateException("Reference Map item does not match requested type : AOJDeclareMethod. Current = " + result.getClass().getName() + " Handler : " + handler);
	}	
	
	public AOJDeclareParents getInterTypeParents(String handler) throws StackObjectReferenceIllegalStateException {
		AOJMember result = referenceMap.get(handler);
		if (null == result)
			return null;
		if (result instanceof AOJDeclareParents)
			return (AOJDeclareParents)result;
		throw new StackObjectReferenceIllegalStateException("Reference Map item does not match requested type : AOJDeclareParents. Current = " + result.getClass().getName() + " Handler : " + handler);
	}		
	
	public AOJDeclareSoft getDeclareSoft(String handler) throws StackObjectReferenceIllegalStateException {
		AOJMember result = referenceMap.get(handler);
		if (null == result)
			return null;
		if (result instanceof AOJDeclareSoft)
			return (AOJDeclareSoft)result;
		throw new StackObjectReferenceIllegalStateException("Reference Map item does not match requested type : AOJDeclareSoft. Current = " + result.getClass().getName() + " Handler : " + handler);
	}			
}
