package br.ufsm.aopjungle.plugin;

import br.ufsm.aopjungle.builder.AOJWorkspace;

public interface AOJPlugin {
	public void run (AOJWorkspace workspace);	 
}
