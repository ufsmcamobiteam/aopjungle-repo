package br.ufsm.aopjungle.util;

import java.io.LineNumberReader;
import java.io.Reader;
import java.util.List;

import org.aspectj.org.eclipse.jdt.core.dom.Block;
import org.aspectj.org.eclipse.jdt.core.dom.CatchClause;
import org.aspectj.org.eclipse.jdt.core.dom.DoStatement;
import org.aspectj.org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.aspectj.org.eclipse.jdt.core.dom.ForStatement;
import org.aspectj.org.eclipse.jdt.core.dom.IfStatement;
import org.aspectj.org.eclipse.jdt.core.dom.Statement;
import org.aspectj.org.eclipse.jdt.core.dom.SwitchStatement;
import org.aspectj.org.eclipse.jdt.core.dom.TryStatement;
import org.aspectj.org.eclipse.jdt.core.dom.WhileStatement;

public class AOJSourceService {
	public static long getNumberOfLines (Reader in) {
		LineNumberReader lnr = new LineNumberReader(in);
		
		return lnr.getLineNumber();
	}	
	
	/**
	 * Exclusions :
	 * 
	 * 1. Blank line
	 * 2. Headers
	 * 3. Comments
	 * 
	 * This is a simple line count for classes and aspects.
	 * Consider typed code only. Hidden statements like default constructor 
	 * is not considered
	 * 
	 * @param in Array of characteres to have line counted
	 * @return The quantitiy of lines in the source code
	 *  
	 */
	public static long getNumberOfLines (char[] in) {
		long linesCount = 0;
		char prevChar = ' ';
		for (int i = 0; i < in.length; i++) {			
			switch (in[i]) {
				case '\r': {
		        // The current line is terminated by a carriage return or by a carriage return immediately followed by a line feed.
					if ( (prevChar != ' ') && (prevChar != '\n') ) 
						linesCount++;
		            break;
		        }
		        case '\n': {
		        	if (prevChar == '\n')
		            	break;
		        	
		        	if ( (prevChar != '\r') && (prevChar != ' ') && (i < in.length)) {
		        		linesCount++;
		        		break;
		            }  
		        }
	        }
	        if (in[i] != ' ')
	        	prevChar = in[i];	
	    }
	    return linesCount;
	}	
	
	public static long getNumberOfStatements (List<Object> statements) {
		long natr = 0; //statements.size();
		for (int i = 0; i < statements.size(); i++) 			
			natr = natr + getSubStatements((Statement)statements.get(i));
		return natr;
	}

	@SuppressWarnings("unchecked")
	private static long getSubStatements(Statement statement) {
		long natr = 0;
		
		if (statement instanceof Block)
			natr = natr + (getNumberOfStatements ( ((Block)statement).statements() ));
		else {
			natr++;	
			if (statement instanceof SwitchStatement) {
				SwitchStatement lStatement = (SwitchStatement)statement;
				natr = natr + getNumberOfStatements(lStatement.statements());
			} else if (statement instanceof IfStatement) {
				IfStatement lStatement = (IfStatement)statement;
				natr = natr + (lStatement.getThenStatement() == null ? 0 : getSubStatements( lStatement.getThenStatement() ) ) ;
				natr = natr + (lStatement.getElseStatement() == null ? 0 : getSubStatements( lStatement.getElseStatement() ) );
			} else if (statement instanceof ForStatement) {
				ForStatement lStatement = (ForStatement)statement;
				natr = natr + getSubStatements ((lStatement).getBody()); 
			} else if (statement instanceof EnhancedForStatement) {
				EnhancedForStatement lStatement = (EnhancedForStatement)statement;
				natr = natr + getSubStatements ((lStatement).getBody());				
			} else if (statement instanceof WhileStatement) {
				WhileStatement lStatement = (WhileStatement)statement;
				natr = natr + getSubStatements ( lStatement.getBody() ); 
			} else if (statement instanceof DoStatement) {
				DoStatement lStatement = (DoStatement)statement;
				natr = natr + getSubStatements ( lStatement.getBody() );
			} else if (statement instanceof TryStatement) {
				TryStatement lStatement = (TryStatement)statement;
				natr = natr + getSubStatements ( lStatement.getBody() );
				natr = natr + getSubStatements ( lStatement.getFinally());
				for (Object cc: lStatement.catchClauses())
					natr = natr + getSubStatements ( ((CatchClause)cc).getBody());
			} 
		}
		
		return natr;
	}
	
}