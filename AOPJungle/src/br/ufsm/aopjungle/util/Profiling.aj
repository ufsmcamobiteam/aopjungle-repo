package br.ufsm.aopjungle.util;

import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;

public aspect Profiling {
	long startTime;
	long endTime;

	pointcut profilingAllProjectBuilding() : execution (* AOJWorkspace.loadProjects(*));
	pointcut profilingIndividualProjectBuild(AOJProject p) : execution (* AOJWorkspace.build(AOJProject)) && args(p);

	before() : profilingAllProjectBuilding() {
		System.out.println("Starting all projects " + System.nanoTime());
	}
	
	after() : profilingAllProjectBuilding() {
		System.out.println("Ending all projects " + System.nanoTime());
	}
	
	before(AOJProject p) : profilingIndividualProjectBuild(p) {
		startTime = System.nanoTime();
	}
	
	after(AOJProject p) : profilingIndividualProjectBuild(p) {
		endTime = System.nanoTime();
		System.out.println("Project: " + p.getName() + " starting " + startTime);
		System.out.println("Project: " + p.getName() + " ending " + endTime);
		System.out.println("Project: " + p.getName() + " elapsed " + (endTime - startTime)/1000000 + " Miliseconds");
	}	
}
