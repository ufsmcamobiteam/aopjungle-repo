package br.ufsm.aopjungle.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.JavaRuntime;

public class AOJClassLoader {
	
	private static List<URLClassLoader> loaders = null;
	
	private AOJClassLoader(){
		// do nothing
	}
	
	public static Class<?> forName(String fullyQualifiedName) throws ClassNotFoundException{

		for (URLClassLoader loader : getLoaders()) {
			return loader.loadClass(fullyQualifiedName);
		}
		return null;
	}

	private static List<URLClassLoader> getLoaders() {
		if (loaders == null) {
			loaders = new ArrayList<URLClassLoader>();
			for (IJavaProject project : getJavaProjects()) {
				try {
					String[] classPathEntries = JavaRuntime.computeDefaultRuntimeClassPath(project);
					List<URL> urlList = new ArrayList<URL>();
					for (int i = 0; i < classPathEntries.length; i++) {
						String entry = classPathEntries[i];
						IPath path = new Path(entry);
						URL url = url = path.toFile().toURI().toURL();
						urlList.add(url);
					}

					ClassLoader parentClassLoader = project.getClass().getClassLoader();
					
					URL[] urls = (URL[]) urlList.toArray(new URL[urlList.size()]);
					URLClassLoader classLoader = new URLClassLoader(urls, parentClassLoader);
					
					
					loaders.add(classLoader);

				} catch (CoreException | MalformedURLException e) {
					
				}
			}
		}

		return loaders;
	}
	
	private static List<IJavaProject>  getJavaProjects(){
		List<IJavaProject> javaProjects = new ArrayList<IJavaProject>();
		IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
		for(IProject project: projects){
			 try {
				project.open(null);
				IJavaProject javaProject = JavaCore.create(project);
				javaProjects.add(javaProject);
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
		return javaProjects;
	}

}
