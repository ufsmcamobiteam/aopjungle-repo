package br.ufsm.aopjungle.util;

import org.aspectj.org.eclipse.jdt.internal.compiler.ast.ASTNode;
import org.aspectj.org.eclipse.jdt.internal.compiler.ast.MethodDeclaration;
import org.aspectj.org.eclipse.jdt.internal.compiler.ast.TypeParameter;

public class AOJAsmUtils {
	public static String genSourceSignature(MethodDeclaration methodDeclaration) {
		StringBuffer output = new StringBuffer();
		ASTNode.printModifiers(methodDeclaration.modifiers, output);

		// Append Type Parameters if any
		TypeParameter types[] = methodDeclaration.typeParameters();
		if (types != null && types.length != 0) {
			output.append("<");
			for (int i = 0; i < types.length; i++) {
				if (i > 0) {
					output.append(", ");
				}
				types[i].printStatement(0, output);
			}
			output.append("> ");
		}

		methodDeclaration.printReturnType(0, output).append(methodDeclaration.selector).append('(');
		if (methodDeclaration.arguments != null) {
			for (int i = 0; i < methodDeclaration.arguments.length; i++) {
				if (i > 0)
					output.append(", ");
				methodDeclaration.arguments[i].print(0, output);
			}
		}
		output.append(')');
		if (methodDeclaration.thrownExceptions != null) {
			output.append(" throws ");
			for (int i = 0; i < methodDeclaration.thrownExceptions.length; i++) {
				if (i > 0)
					output.append(", ");
				methodDeclaration.thrownExceptions[i].print(0, output);
			}
		}
		return output.toString();
	}	
}
