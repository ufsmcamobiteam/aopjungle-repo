package br.ufsm.aopjungle.util;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

public class IconProvider {
	private static final String appPath = IconProvider.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	private static final String DEFAULTPATH = appPath + "icons/";
	private static String filePath = DEFAULTPATH+"defaulticon.jpg";
	
	private static Display getDisplay() {
		Display display = Display.getCurrent();
		if (display == null)
			display = Display.getDefault();
		return display;	
	}	
	
	public static Image getIcon(String fileName) {
		if ( fileName.length() == 0 )
			throw new IllegalArgumentException("Empty icon file name was found");
			filePath = DEFAULTPATH+fileName;
		    Image icon = new Image (getDisplay(), filePath);

		return icon;
	}
}
