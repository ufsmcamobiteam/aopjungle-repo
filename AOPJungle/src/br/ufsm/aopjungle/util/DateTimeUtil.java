package br.ufsm.aopjungle.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtil {
	private static Calendar cal = Calendar.getInstance();

	public static String getCurrentTimeAsString() {
		Date date = cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss:SS");
		String formattedDate=dateFormat.format(date);
		return formattedDate;
	}
	
	public static String getCurrentDateTimeAsString() {
		Date date = cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY, HH:mm:ss:SS");
		String formattedDate=dateFormat.format(date);
		return formattedDate;
	}
	
}
