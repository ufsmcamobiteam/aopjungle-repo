package br.ufsm.aopjungle.util;

import java.util.StringTokenizer;

import org.aspectj.asm.IProgramElement;

import br.ufsm.aopjungle.metamodel.aspectj.AOJAdviceKindEnum;

public class StringUtil {
	private static final String DELIMITER = "$";
	private static final String AJCHEADER = "ajc";
	private static final String INTERFIELDINIT = "interFieldInit";
	private static final String INTERMETHODINIT = "interMethod";
	private static final String DECLAREPARENTS = "declare_parents_";
	private static final String DECLARESOFT = "declare_soft_";
	private static final char NAMESEPARATOR = '_';
	private static final char DOT = '.';
	public static final short SOURCEINTERTYPE = 2;
	public static final short TARGETINTERTYPE = 2;
	public static String unQualify(String qualified) {
		return qualified.substring(qualified.lastIndexOf('.')+1);
	}
	
	public static String buildHandler (String arg0, String arg1, String arg2) {
		StringBuffer sb = new StringBuffer();
		sb.append(arg0);
		sb.append(DELIMITER);
		sb.append(arg1);
		sb.append(DELIMITER);
		sb.append(arg2);
		return sb.toString();
	}
	
	public static String buildHandlerAdvice(AOJAdviceKindEnum kind, String pack, String type, int position) {
		StringBuffer sb = new StringBuffer();
		sb.append(AJCHEADER);
		sb.append(DELIMITER);
		sb.append(getAdviceKindLabel(kind));
		sb.append(DELIMITER);
		sb.append(pack.replace(DOT, NAMESEPARATOR));
		sb.append(NAMESEPARATOR);
		sb.append(type);
		sb.append(DELIMITER);
		sb.append(position);
		return sb.toString();
	}
	
	private static Object getAdviceKindLabel(AOJAdviceKindEnum kind) {
		String result = kind.getName().toLowerCase();
		result = result.replace("returning", "Returning");
		result = result.replace("throwing", "Throwing");
		return result;
	}

	public static String getTokenfromByteCodeName(String byteCodeName, short field) {
		StringTokenizer tokens = new StringTokenizer(byteCodeName, DELIMITER);
		int i = 0;
		while (tokens.hasMoreTokens()) {
			String token = tokens.nextToken(); 
			if (i++ == field)
				return token.replace(NAMESEPARATOR, DOT);
		}
		return "";
	}

	public static String buildTypeHandler(String packageName, String name) {
		StringBuffer sb = new StringBuffer();
		sb.append(packageName);
		sb.append(DOT);
		sb.append(name);
		return sb.toString();
	}

	private static String buildDeclareHandler (String sourceType,
			String targetType, String field, String kind) {
		String stype, ttype;
		stype = sourceType.replace(DOT, NAMESEPARATOR);
		ttype = targetType.replace(DOT, NAMESEPARATOR);
		StringBuffer sb = new StringBuffer();
		sb.append(AJCHEADER);
		sb.append(DELIMITER);
		sb.append(kind);
		sb.append(DELIMITER);
		sb.append(stype);
		sb.append(DELIMITER);
		sb.append(ttype);
		sb.append(DELIMITER);
		sb.append(field);
		return sb.toString();		
	}
	//ajc$interMethod$com_ajtetris_aspects_TestAspect$FooClass$smile
	public static String buildDeclareFieldHandler(String sourceType,
			String targetType, String field) {
		return buildDeclareHandler (sourceType, targetType, field, INTERFIELDINIT);
	}

	public static String removeQualifiedOntype(String handler) {
		// This operation should not be exist since onType
		// should be FullyQualified. However, AJDT does not hold
		// declaredField.onType like this.
		// Fix it further.
		int index = 3;
		StringBuffer sb = new StringBuffer();
		StringTokenizer tokens = new StringTokenizer(handler, DELIMITER);
		int i = 0;
		while (tokens.hasMoreTokens()) {
			String token = tokens.nextToken(); 
			if (i++ == index) {
				int lastDot = token.lastIndexOf(NAMESEPARATOR);
				if (lastDot >=0 )
					sb.append(token.substring(lastDot + 1, token.length()));
			} else
				sb.append(token);
			if (tokens.hasMoreTokens())
				sb.append(DELIMITER);
		}
		return sb.toString();		
	}

	public static String buildDeclareMethodHandler(String sourceType,
			String targetType, String field) {
		return buildDeclareHandler (sourceType, targetType, field, INTERMETHODINIT);
	}

	public static String buildDeclareParentHandler(String owner, int position) {
		StringBuffer sb = new StringBuffer();
		sb.append(AJCHEADER);
		sb.append(DELIMITER);
		sb.append(owner.replace(DOT, NAMESEPARATOR));
		sb.append(DELIMITER);
		sb.append(DECLAREPARENTS);
		sb.append(position);
		return sb.toString();
	}

	public static String buildDeclareParentsdHandler(String packageName,
			IProgramElement parent, String bytecodeName) {
		StringBuffer sb = new StringBuffer();
		sb.append(AJCHEADER);
		sb.append(DELIMITER);
		sb.append(packageName.replace(DOT, NAMESEPARATOR));
		sb.append(NAMESEPARATOR);
		sb.append(parent.getName());
		sb.append(bytecodeName.substring(bytecodeName.indexOf(DELIMITER), bytecodeName.length()));
		return sb.toString();		
	}

	public static String buildDeclareSoftHandler(String owner, int position) {
		StringBuffer sb = new StringBuffer();
		sb.append(AJCHEADER);
		sb.append(DELIMITER);
		sb.append(owner.replace(DOT, NAMESEPARATOR));
		sb.append(DELIMITER);
		sb.append(DECLARESOFT);
		sb.append(position);
		return sb.toString();
	}
	
	public static String buildDeclareSoftHandler(String packageName,
			String linkLabelString, String bytecodeName) {
		StringBuffer sb = new StringBuffer();
		sb.append(AJCHEADER);
		sb.append(DELIMITER);
		sb.append(packageName.replace(DOT, NAMESEPARATOR));
		sb.append(NAMESEPARATOR);
		sb.append(linkLabelString.substring(0,linkLabelString.indexOf(DOT)));
		sb.append(DELIMITER);
		sb.append(bytecodeName.substring(bytecodeName.indexOf(DELIMITER)+1, bytecodeName.length()));
		return sb.toString();		
	}
}