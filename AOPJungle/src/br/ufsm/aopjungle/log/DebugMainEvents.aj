package br.ufsm.aopjungle.log;

import java.util.List;
import java.util.Map;

import org.aspectj.org.eclipse.jdt.core.dom.AnnotationTypeDeclaration;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.compiler.CategorizedProblem;

import br.ufsm.aopjungle.metamodel.commons.AOJCompilationUnit;
import br.ufsm.aopjungle.metamodel.commons.AOJPackageDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.aspectj.AOJungleVisitor;
import br.ufsm.aopjungle.aspectj.AOJBuildListener;

public aspect DebugMainEvents {
	
	pointcut cUnitProcessing(AOJPackageDeclaration pack) : 
		execution (* AOJPackageDeclaration.loadCompilationUnits())
	 && this(pack) && !within(DebugMainEvents);
	pointcut packProcessing(AOJProject project) : 
		execution (* AOJProject.loadPackages())
     && this(project) && !within(DebugMainEvents);
	pointcut createVisitor(AOJCompilationUnit cUnit) : 
		execution (* AOJCompilationUnit.createVisitor())
	 && this(cUnit) && !within(DebugMainEvents);
	pointcut createVisitorAnnDeclaration(AnnotationTypeDeclaration ann) : 
		execution (* AOJungleVisitor.visit(AnnotationTypeDeclaration))
	 && args(ann)	
	 && !within(DebugMainEvents);
	pointcut buildProject (AOJProject project) :
		execution (* AOJProject.build(..)) && this(project);
	
	pointcut startBuild (int kind, IProject project,
			IProject[] requiredProjects) : 
			execution (* AOJBuildListener.preAJBuild(int, IProject, IProject[]) )
					&& args (kind, project, requiredProjects);

	pointcut endBuild (int kind, IProject project, boolean noSourceChanges) : 
			execution (* AOJBuildListener.postAJBuild(int, IProject, boolean, ..) )
					&& args (kind, project, noSourceChanges,..);
	
	pointcut cleanBuild (IProject project):
		execution (* AOJBuildListener.postAJClean(IProject)) && args (project);
	
	after (int kind, IProject project, IProject[] requiredProjects) : 
		startBuild (kind, project, requiredProjects) {
		AOJLogger.getLogger().debug("Start building project: " + project.getName());
	}

	after (int kind, IProject project, boolean noSourceChanges) : 
		endBuild (kind,project,noSourceChanges) {
		AOJLogger.getLogger().debug("End building project: " + project.getName());
	}
	
	after (IProject project): cleanBuild (project) {
		AOJLogger.getLogger().info("Clean project was performed. Project: " + project.getName());
	}
	
	before(AnnotationTypeDeclaration ann) : createVisitorAnnDeclaration(ann) {
		AOJLogger.getLogger().debug("++++++ Processing annotation decl : " + ann.getName().toString());
	}
	
	before (AOJProject project) : packProcessing(project) {
		AOJLogger.getLogger().debug(">> Processing project : "+project.getName());
	}

	before (AOJPackageDeclaration pack) : cUnitProcessing(pack) {
		AOJLogger.getLogger().debug(">>> Processing package : "+pack.getName());
	}
	
	before (AOJCompilationUnit cUnit) : createVisitor(cUnit) {
		AOJLogger.getLogger().debug(">>>> Processing cunit : "+cUnit.getName());
		System.out.println(">>>> Processing cunit : "+cUnit.getName());
	}
	
	before (AOJProject project) : buildProject(project) {
		AOJLogger.getLogger().debug("Building project : "+ project.getName());
		
	}
	
}
