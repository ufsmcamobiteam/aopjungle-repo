package br.ufsm.aopjungle.aspectj;

import java.util.List;
import java.util.Map;

import org.aspectj.asm.IProgramElement;
import org.aspectj.asm.IRelationship;
import org.eclipse.ajdt.core.builder.IAJBuildListener;
import org.eclipse.ajdt.core.lazystart.IAdviceChangedListener;
import org.eclipse.ajdt.core.model.AJProjectModelFacade;
import org.eclipse.ajdt.core.model.AJProjectModelFactory;
import org.eclipse.ajdt.core.model.AJRelationshipManager;
import org.eclipse.ajdt.core.model.AJRelationshipType;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.compiler.CategorizedProblem;

import br.ufsm.aopjungle.binding.AOJBinding;
import br.ufsm.aopjungle.binding.AOJReferenceManager;
import br.ufsm.aopjungle.exception.IllegalReferenceMapState;
import br.ufsm.aopjungle.exception.StackObjectReferenceIllegalStateException;
import br.ufsm.aopjungle.log.AOJLogger;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareField;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareParents;
import br.ufsm.aopjungle.metamodel.commons.AOJMember;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.util.StringUtil;

public class AOJBuildListener implements IAJBuildListener /*BuildConfigListener*/ {
	public boolean finished = false;
	public boolean succeeded = false;
	private static AOJBuildListener instance;
	private static AJRelationshipType[] relationshipsAdvisesTypes = {AJRelationshipManager.ADVISES};
	private static AJRelationshipType[] relationshipsInterTypes = {AJRelationshipManager.DECLARED_ON};
	private static AJRelationshipType[] relationshipsSoftenExceptions = {AJRelationshipManager.SOFTENS};
	private static AJRelationshipType[] relationshipsDeclareAspects = {AJRelationshipManager.ASPECT_DECLARATIONS};

	public void reset() { 
		finished = false;
	}

	public static AOJBuildListener getInstance() {
		if (null == instance)
			instance = new AOJBuildListener();
		return instance;
	}
	
//	public void compileStarted(String buildConfigFile) {
//		
//	}
//
//	public void compileFinished(String buildConfigFile, int buildTime, boolean succeeded, boolean warnings) {
//        this.succeeded = succeeded;
//		finished = true;
//    } 

//    public void compileAborted(String buildConfigFile, String message) {
//    	
//    }

    public boolean getBuildFinished() { 
    	return finished; 
    }

    public boolean getBuildSucceeded() {
    	return succeeded; 
    }

//    @Override
//	public void configsListUpdated(List arg0) {
//		System.out.println("Compilation Config update " + arg0);
//	}
//
//	@Override
//	public void currConfigChanged(String arg0) {
//		System.out.println("Curr config changed " + arg0);
//	}

	@Override
	public void preAJBuild(int kind, IProject project,
			IProject[] requiredProjects) {
		finished = false;
		succeeded = false;
	}

	private static IProgramElement getProgramElement(IProgramElement e) {
		if ( (e.getKind().equals(IProgramElement.Kind.CLASS))
		||	(e.getKind().equals(IProgramElement.Kind.ASPECT)) )
			return e;	
			
		while (e!=null && !(e.getKind().equals(IProgramElement.Kind.METHOD) 
			|| e.getKind().equals(IProgramElement.Kind.CONSTRUCTOR) 
			|| e.getKind().equals(IProgramElement.Kind.ADVICE) 
			|| e.getKind().equals(IProgramElement.Kind.INTER_TYPE_METHOD) 
			|| e.getKind().equals(IProgramElement.Kind.INTER_TYPE_CONSTRUCTOR)))
			e = e.getParent();
		
		return e;
	}

	private static IProgramElement getType(IProgramElement e) {
		while(e!=null && !(e.getKind().equals(IProgramElement.Kind.ASPECT)) && ! (e.getKind().equals(IProgramElement.Kind.CLASS)))
			e = e.getParent();
		return e;
	}

	private static AOJMember getAOPJungleElement(IProgramElement e) throws StackObjectReferenceIllegalStateException {
		String handler;
		
		if ( (e.getKind().equals(IProgramElement.Kind.CLASS)) || (e.getKind().equals(IProgramElement.Kind.ASPECT)) ) {
			handler =  StringUtil.buildTypeHandler(e.getPackageName(), e.toSignatureString());
			return AOJReferenceManager.getInstance().getType(handler);
		}	

		if (e.getKind().equals(IProgramElement.Kind.ADVICE))
			handler =  getAdviceElement (e);
		else
			handler = StringUtil.buildHandler(e.getPackageName(), e.getParent().getName(), e.getSourceSignature());

		return AOJReferenceManager.getInstance().getMethod(handler);	
//		AOJMember method;
//		try {
//			method = AOJReferenceManager.getInstance().getMethod(handler);
//
//			if (null == method)
//				AOJLogger.getLogger().warn("Reference Manager :: Method Element not Found at MapReference " + handler);
//			
//			return method;
//		} catch (StackObjectReferenceIllegalStateException e1) {
//			e1.printStackTrace();
//			return null;
//		}

	}
	
	private static AOJTypeDeclaration getTypeElement(String handler) {
		AOJTypeDeclaration type;
		try {
			type = AOJReferenceManager.getInstance().getType(handler);

			if (null == type)
				AOJLogger.getLogger().warn("Reference Manager :: Type Element not Found at MapReference " + handler);
			
			return type;
		} catch (StackObjectReferenceIllegalStateException e) {
			e.printStackTrace();
			return null;
		}
	}	
	
	private static String getAdviceElement(IProgramElement e) {
		return e.getBytecodeName().substring(0, e.getBytecodeName().lastIndexOf("$"));
	}

	@Override
	public void postAJBuild(int kind, IProject project,
			boolean noSourceChanges,
			Map<IFile, List<CategorizedProblem>> newProblems) {
		finished = true;
		succeeded = newProblems.size() == 0;
		
		if (noSourceChanges)
			return;
		
		AJProjectModelFacade model = AJProjectModelFactory.getInstance().getModelForProject(project);
		
		try {
			loadAdvicesRelationship(model);
			loadInterTypeRelationship(model);
		} catch (StackObjectReferenceIllegalStateException e) {
			e.printStackTrace();
		} catch (IllegalReferenceMapState e) {
			e.printStackTrace();
		}
		
		try {
			loadSoftenRelationship(model);
		} catch (StackObjectReferenceIllegalStateException e) {
			e.printStackTrace();
		} catch (IllegalReferenceMapState e) {
			e.printStackTrace();
		}
		
		try {
			loadAspectDeclarationsRelationship(model);
		} catch (StackObjectReferenceIllegalStateException e) {
			e.printStackTrace();
		} catch (IllegalReferenceMapState e) {
			e.printStackTrace();
		}		
	}

	private void loadAspectDeclarationsRelationship(AJProjectModelFacade model) throws StackObjectReferenceIllegalStateException, IllegalReferenceMapState {
		List<IRelationship> relationships = model.getRelationshipsForProject(relationshipsDeclareAspects);
		for (IRelationship relationship: relationships) {
			IProgramElement sourceElement = model.getProgramElement(relationship.getSourceHandle());
//			print (sourceElement, "Source Aspects Declare");
			for (String s: (List<String>)relationship.getTargets()) {
				IProgramElement targetElement = model.getProgramElement(s);
//				print (targetElement, "Target Aspects Declare");
			}	
		}	
	}

	private void loadInterTypeRelationship(AJProjectModelFacade model) throws StackObjectReferenceIllegalStateException, IllegalReferenceMapState {
		List<IRelationship> relationships = model.getRelationshipsForProject(relationshipsInterTypes);
		for (IRelationship relationship: relationships) {
			IProgramElement sourceElement = model.getProgramElement(relationship.getSourceHandle());
			print(sourceElement, "Source inter type relation");
			AOJMember sourceType = null;
			String sourceHandler = "";

			if (sourceElement.getKind().equals(IProgramElement.Kind.INTER_TYPE_FIELD)) {
				sourceHandler = StringUtil.removeQualifiedOntype(sourceElement.getBytecodeName());
				sourceType = getInterTypeFieldElement(sourceHandler);
			} else if ( (sourceElement.getKind().equals(IProgramElement.Kind.INTER_TYPE_METHOD)) ||
					(sourceElement.getKind().equals(IProgramElement.Kind.INTER_TYPE_CONSTRUCTOR)) ) {
				sourceHandler = StringUtil.removeQualifiedOntype(sourceElement.getBytecodeName());
				sourceType = getInterTypeMethodElement(sourceHandler);
			} else if (sourceElement.getKind().equals(IProgramElement.Kind.DECLARE_PARENTS)) {
				sourceHandler = StringUtil.buildDeclareParentsdHandler (sourceElement.getPackageName(), sourceElement.getParent(), sourceElement.getBytecodeName());
				sourceType = getInterTypeParentsElement(sourceHandler);
			}
			
			// TODO : Declare Error / Declare Warning
			
			if (null == sourceType)
				throw new IllegalReferenceMapState ("Could not find source element on reference map : " + "[handler] " + sourceHandler + "[Kind] " + sourceElement.getKind());

			AOJBinding sourceBinding = createBinding(sourceType, sourceElement, relationship);

			for (String s: (List<String>)relationship.getTargets()) {
				IProgramElement targetElement = model.getProgramElement(s);
				if ( (null != targetElement) ) {
					print(targetElement, "Target inter type relation");
					AOJMember targetType = getTypeElement(StringUtil.buildTypeHandler(targetElement.getPackageName(),targetElement.getName()));
					AOJBinding targetBinding = createBinding(targetType, targetElement, relationship);
					try {
						loadInOutRelationShip (sourceType, sourceBinding, sourceElement, targetType, targetBinding, targetElement);
					} catch (IllegalArgumentException e) {
						AOJLogger.getLogger().warn("Could not add relationship at map, source or target element is null.");
					}
				}
			}
		}	
	}

	private void loadInOutRelationShip (AOJMember source, AOJBinding sourceBinding, IProgramElement sourceElement,
			AOJMember target, AOJBinding targetBinding, IProgramElement targetElement) {
		if ( (null != source) )  {
			source.getBindingModel().getOut().add(targetBinding);
			bindSourceOwner(source, targetBinding);
		}	
		
		if ( (null != target) ) {
			target.getBindingModel().getIn().add(sourceBinding);
			bindTargetOwner(target, sourceBinding);
		}	
	}
	
	private void bindTargetOwner(AOJMember target, AOJBinding sourceBinding) {
		if ( null != target.getOwner() )
			if (target.getOwner() instanceof AOJMember) {
				( (AOJMember) target.getOwner()).getBindingModel().getIn().add(sourceBinding);
				bindSourceOwner((AOJMember)target.getOwner(), sourceBinding);
			}	
		
	}

	private void bindSourceOwner(AOJMember source, AOJBinding targetBinding) {
		if ( null != source.getOwner() )
			if (source.getOwner() instanceof AOJMember) {
				( (AOJMember) source.getOwner()).getBindingModel().getOut().add(targetBinding);
				bindSourceOwner((AOJMember)source.getOwner(), targetBinding);
			}	
	}

	private AOJMember getDeclareSoftElement(String handler) throws StackObjectReferenceIllegalStateException {
		return AOJReferenceManager.getInstance().getDeclareSoft(handler);
	}

	private AOJMember getInterTypeMethodElement(String handler) throws StackObjectReferenceIllegalStateException {
		return AOJReferenceManager.getInstance().getInterTypeMethod(handler);
	}

	private AOJDeclareField getInterTypeFieldElement(String handler) throws StackObjectReferenceIllegalStateException {
		return AOJReferenceManager.getInstance().getInterTypeField(handler);
	}

	private AOJDeclareParents getInterTypeParentsElement(String handler) throws StackObjectReferenceIllegalStateException {
		return AOJReferenceManager.getInstance().getInterTypeParents(handler);
	}

	private void loadSoftenRelationship(AJProjectModelFacade model) throws StackObjectReferenceIllegalStateException, IllegalReferenceMapState {
		List<IRelationship> relationships = model.getRelationshipsForProject(relationshipsSoftenExceptions);
		for (IRelationship relationship: relationships) {
			IProgramElement sourceElement = model.getProgramElement(relationship.getSourceHandle());
//			print(sourceElement, "Source soft relation");
			AOJMember source = null;
			String sourceHandler = "";
			if (sourceElement.getKind().equals(IProgramElement.Kind.DECLARE_SOFT)) {
				sourceHandler = StringUtil.buildDeclareSoftHandler (sourceElement.getPackageName(), sourceElement.toLinkLabelString(), sourceElement.getBytecodeName());
				source = getDeclareSoftElement(sourceHandler);
			}
			
			if (null == source)
				throw new IllegalReferenceMapState ("Could not find source element on reference map : " + sourceHandler);

			AOJBinding sourceBinding = createBinding(source, sourceElement, relationship);

			for (String s: (List<String>)relationship.getTargets()) {
				IProgramElement targetElement = getProgramElement(model.getProgramElement(s));
				if ( (null != targetElement) ) {
//						print(targetElement, "Target soft relation");
					AOJMember target = getAOPJungleElement(targetElement);
					AOJBinding targetBinding = createBinding(target, targetElement, relationship);
					try {
						loadInOutRelationShip (source, sourceBinding, sourceElement, target, targetBinding, targetElement);					
					} catch (IllegalArgumentException e) {
						AOJLogger.getLogger().warn("Could not add relationship at map, source or target element is null.");
					}
				}
			}
		}	
	}	
	
	private void loadAdvicesRelationship(AJProjectModelFacade model) throws StackObjectReferenceIllegalStateException {
		List<IRelationship> relationships = model.getRelationshipsForProject(relationshipsAdvisesTypes);
		for (IRelationship relationship: relationships) {
			IProgramElement sourceElement = model.getProgramElement(relationship.getSourceHandle());
			AOJMember source = getAOPJungleElement(sourceElement);
//			print(sourceElement, "Source Advice relation ");
			AOJBinding sourceBinding = createBinding(source, sourceElement, relationship);
			for (String s: (List<String>)relationship.getTargets()) {
				IProgramElement targetElement = getProgramElement(model.getProgramElement(s));
				if ( (null != targetElement) && (null != targetElement.getSourceSignature()) ) {
					AOJMember target = getAOPJungleElement(targetElement);
					AOJBinding targetBinding = createBinding(target, targetElement, relationship);
//					print(targetElement, "Target Advice relation");
					try {
						loadInOutRelationShip (source, sourceBinding, sourceElement, target, targetBinding, targetElement);					
					} catch (IllegalArgumentException e) {
						AOJLogger.getLogger().warn("Could not add relationship at map, source or target element is null.");
					}
				} 
			}					
		}
	}

	private void print(IProgramElement programElement, String kind) {
		if (null == programElement) {
			AOJLogger.getLogger().warn("Printing null - Ignored");
			return;
		}
		System.out.println("--------> Printing " + kind);
		System.out.println("getFullyQualifiedName " + programElement.getFullyQualifiedName());
		System.out.println("getBytecodeName " + programElement.getBytecodeName());
		System.out.println("getBytecodeSignature " + programElement.getBytecodeSignature());
		System.out.println("getCorrespondingType " + programElement.getCorrespondingType());
		System.out.println("getCorrespondingTypeSignature " + programElement.getCorrespondingTypeSignature());
		System.out.println("getDeclaringType " + programElement.getDeclaringType());
		System.out.println("getFullyQualifiedName " + programElement.getFullyQualifiedName());
		System.out.println("getHandleIdentifier " + programElement.getHandleIdentifier());
		System.out.println("getName " + programElement.getName());
		System.out.println("getPackageName " + programElement.getPackageName());
		System.out.println("getSourceSignature " + programElement.getSourceSignature());
		System.out.println("getKind " + programElement.getKind());
		System.out.println("getDetails " + programElement.getDetails());
		System.out.println("getExtraInfo " + programElement.getExtraInfo());
		System.out.println("getSourceLocation " + programElement.getSourceLocation());
		System.out.println("getMessage " + programElement.getMessage());
		System.out.println("getAccessibility " + programElement.getAccessibility());
		System.out.println("getFormalComment " + programElement.getFormalComment());
		System.out.println("hashCode " + programElement.hashCode());
		System.out.println("toLabelString " + programElement.toLabelString());
		System.out.println("toLinkLabelString " + programElement.toLinkLabelString());
		System.out.println("toLongString " + programElement.toLongString());
		System.out.println("toSignatureString " + programElement.toSignatureString());
		System.out.println("getParameterNames " + programElement.getParameterNames());
		System.out.println("getParameterSignaturesSourceRefs " + programElement.getParameterSignaturesSourceRefs());
		System.out.println("getParameterTypes " + programElement.getParameterTypes());
		System.out.println("getParentTypes " + programElement.getParentTypes());
		System.out.println("getParent " + programElement.getParent().getBytecodeName());
	}

	private AOJBinding createBindingNull(IProgramElement programElement, IRelationship relationship) {
		AOJBinding result = new AOJBinding();
		result.setBindingHandler(programElement.getBytecodeName());
		result.setKind(programElement.getKind().toString());
		result.setRelationshipKind(relationship.getKind().toString());
		result.setName(programElement.getName());
		result.setObject(null);
		return result;
	}

//	private AOJBinding createMethodBinding (AOJMember method, IProgramElement programElement, IRelationship relationship) {
//		AOJBinding result = new AOJBinding();
//		result.setHandler(method.getHandler());
//		result.setKind(programElement.getKind().toString());
//		result.setRelationshipKind(relationship.getKind().toString());
//		result.setName(method.getName());
//		result.setObject(method);
//		return result;
//	}

//	private AOJBinding createTypeBinding (AOJMember type, IProgramElement programElement, IRelationship relationship) {
//		if (null == type)
//			return createBindingNull(programElement, relationship);
//		AOJBinding result = new AOJBinding();
//		result.setHandler(type.getHandler());
//		result.setKind(programElement.getKind().toString());
//		result.setRelationshipKind(relationship.getKind().toString());
//		result.setName(type.getName());
//		result.setObject(type);
//		return result;
//	}
	
	private AOJBinding createBinding (AOJMember type, IProgramElement programElement, IRelationship relationship) {
		if (null == type)
			return createBindingNull(programElement, relationship);
		AOJBinding result = new AOJBinding();
		result.setBindingHandler(type.getInternalHandler());
		result.setKind(programElement.getKind().toString());
		result.setRelationshipKind(relationship.getKind().toString());
		result.setName(type.getName());
		result.setObject(type);
		return result;
	}	
	
	@Override
	public void addAdviceListener(IAdviceChangedListener adviceListener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeAdviceListener(IAdviceChangedListener adviceListener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postAJClean(IProject project) {
	}
}
