package br.ufsm.aopjungle.aspectj;

import java.util.Stack;

import org.aspectj.org.eclipse.jdt.core.dom.AfterAdviceDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.AfterReturningAdviceDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.AfterThrowingAdviceDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.AjASTVisitor;
import org.aspectj.org.eclipse.jdt.core.dom.AjTypeDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.AnnotationTypeDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.AnnotationTypeMemberDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.AnonymousClassDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.AroundAdviceDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.AssertStatement;
import org.aspectj.org.eclipse.jdt.core.dom.BeforeAdviceDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.Block;
import org.aspectj.org.eclipse.jdt.core.dom.BreakStatement;
import org.aspectj.org.eclipse.jdt.core.dom.CflowPointcut;
import org.aspectj.org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.aspectj.org.eclipse.jdt.core.dom.ContinueStatement;
import org.aspectj.org.eclipse.jdt.core.dom.DeclareErrorDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.DeclareParentsDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.DeclarePrecedenceDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.DeclareSoftDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.DeclareWarningDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.DoStatement;
import org.aspectj.org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.aspectj.org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.EnumDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.ExpressionStatement;
import org.aspectj.org.eclipse.jdt.core.dom.FieldDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.ForStatement;
import org.aspectj.org.eclipse.jdt.core.dom.IfStatement;
import org.aspectj.org.eclipse.jdt.core.dom.InterTypeFieldDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.InterTypeMethodDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.LabeledStatement;
import org.aspectj.org.eclipse.jdt.core.dom.MarkerAnnotation;
import org.aspectj.org.eclipse.jdt.core.dom.MethodDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.PointcutDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.ReturnStatement;
import org.aspectj.org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.aspectj.org.eclipse.jdt.core.dom.SwitchStatement;
import org.aspectj.org.eclipse.jdt.core.dom.SynchronizedStatement;
import org.aspectj.org.eclipse.jdt.core.dom.ThrowStatement;
import org.aspectj.org.eclipse.jdt.core.dom.TryStatement;
import org.aspectj.org.eclipse.jdt.core.dom.TypeDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.aspectj.org.eclipse.jdt.core.dom.WhileStatement;

import br.ufsm.aopjungle.metamodel.aspectj.AOJAdviceDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAdviceKindEnum;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareCompilationEnforcement;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareField;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareMethod;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareParents;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclarePrecedence;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareSoft;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutCFlow;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJAnnotable;
import br.ufsm.aopjungle.metamodel.commons.AOJAnnotationDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJAttributeDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJBehaviourKind;
import br.ufsm.aopjungle.metamodel.commons.AOJClassHolder;
import br.ufsm.aopjungle.metamodel.commons.AOJCompilationUnit;
import br.ufsm.aopjungle.metamodel.commons.AOJConcreteTypeMember;
import br.ufsm.aopjungle.metamodel.commons.AOJContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJExpressionStatement;
import br.ufsm.aopjungle.metamodel.commons.AOJMethodDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.java.AOJAnnotationTypeDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJAnonymousClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJConstructorDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJEnhancedForStatement;
import br.ufsm.aopjungle.metamodel.java.AOJEnumDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJIfStatement;
import br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */

public class AOJungleVisitor extends AjASTVisitor {
	
	@XStreamOmitField
	private AOJCompilationUnit cUnit;
	@XStreamOmitField
	private static Stack<Object> elementStack = new Stack<Object>();
//	@XStreamOmitField
//	private int lineCount;

	public AOJungleVisitor(AOJCompilationUnit cUnit) {
		this.cUnit = cUnit;
	}

	public AOJProgramElement getCompilationUnit() {
		return cUnit;
	}
	
	@SuppressWarnings("unused")
	private AOJungleVisitor() {
		
	}
		
	@Override
	public boolean visit(AnonymousClassDeclaration node) {
		
		AOJAnonymousClassDeclaration aojNode = new AOJAnonymousClassDeclaration(node,cUnit);
		AOJClassHolder classHolder = getAOJClassHolder(getLastMemberFromStack());
		if (classHolder != null)
			classHolder.getAnonymousClasses().add(aojNode);
		
//		if (getLastMemberFromStack() instanceof AOJClassHolder) 
//			((AOJClassHolder)getLastMemberFromStack()).getAnonymousClasses().add(aojNode);
		
		elementStack.push(aojNode);
		return super.visit(node);
	}
	
//	public void addCurrentLineCount() {
//		lineCount++;
//	}
	
	@Override
	public void endVisit(AnonymousClassDeclaration node) {
		elementStack.pop();
		super.endVisit(node);
	}

	@Override
	public boolean visit(EnumConstantDeclaration node) {
		return super.visit(node);
	}
	
	@Override
	public boolean visit(EnumDeclaration node) {
		AOJContainer aojNode = new AOJEnumDeclaration (node, cUnit);
		cUnit.addType(aojNode);
		getElementStack().push(aojNode);
		return super.visit(node);
	}
	
	@Override
	public void endVisit(EnumDeclaration node) {
		getElementStack().pop();	
		super.endVisit(node);
	}
	
	public boolean visit(TypeDeclaration node) {
		AOJContainer aojNode;
		if (((AjTypeDeclaration) node).isAspect()) 
			aojNode = new AOJAspectDeclaration(node, cUnit);
		else if (((AjTypeDeclaration) node).isInterface()) 
			aojNode =  new AOJInterfaceDeclaration(node, cUnit);
		else  
			aojNode = new AOJClassDeclaration(node, cUnit);

		// Resolve whether type is anonymous or innerclass one. 
		if (aojNode instanceof AOJClassDeclaration) {	
			if ( (node.isLocalTypeDeclaration()) && (getElementStack().peek() instanceof AOJClassHolder) ) 
				((AOJClassHolder)getElementStack().peek()).getAnonymousClasses().add(aojNode);
			else if ( (node.isMemberTypeDeclaration()) && (getElementStack().peek() instanceof AOJClassHolder) ) 
				((AOJClassHolder)getElementStack().peek()).getInnerClasses().add(aojNode);
			else	
				cUnit.addType(aojNode);
		} else	
			cUnit.addType(aojNode);

		getElementStack().push(aojNode);
		return true; // visit children
	} 
  
	@Override
	public void endVisit(TypeDeclaration node) {
		getElementStack().pop();	
		super.endVisit(node);
	}

	public boolean visit(MethodDeclaration node) {	
		// TODO : Check this model because there're too many duplicated lines 
		if (getLastMemberFromStack() instanceof AOJContainer) {
			if (node.isConstructor()) {
				AOJConstructorDeclaration aojNode = new AOJConstructorDeclaration(node, (AOJProgramElement)getLastMemberFromStack());
     			((AOJConcreteTypeMember)((AOJContainer)getLastMemberFromStack()).getMembers()).getConstructors().add(aojNode);
    			elementStack.push(aojNode);
			} else {	
				AOJMethodDeclaration aojNode = new AOJMethodDeclaration(node, (AOJProgramElement)getLastMemberFromStack());
				((AOJContainer)getLastMemberFromStack()).getMembers().getMethods().add(aojNode);
    			elementStack.push(aojNode);
			}
			
		}
		
		return super.visit(node);
	}

	public void endVisit(MethodDeclaration node) {
		elementStack.pop();
	}

	@Override
	public boolean visit(FieldDeclaration node) {
		AOJAttributeDeclaration aojNode = new AOJAttributeDeclaration(node, (AOJProgramElement)getLastMemberFromStack());
		((AOJContainer)getLastMemberFromStack()).getMembers().getAttributes().add(aojNode);
		elementStack.push(aojNode);
		return super.visit(node);
	}

	@Override
	public void endVisit(FieldDeclaration node) {
		elementStack.pop();
		super.endVisit(node);
	}

	@Override
	public boolean visit(DeclarePrecedenceDeclaration node) {		
		AOJDeclarePrecedence aojNode = new AOJDeclarePrecedence(node, (AOJProgramElement)getLastMemberFromStack());
		if (getLastMemberFromStack() instanceof AOJAspectDeclaration) 
			((AOJAspectDeclaration) getLastMemberFromStack()).getMembers().getDeclarePrecedences().add(aojNode);
		elementStack.push(aojNode);
		return super.visit(node);
	}
	
	@Override
	public void endVisit(DeclarePrecedenceDeclaration node) {
		elementStack.pop();
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(DeclareErrorDeclaration node) {
		AOJDeclareCompilationEnforcement aojNode = new AOJDeclareCompilationEnforcement(node, (AOJProgramElement)getLastMemberFromStack(), node.getPointcut(), node.getMessage());
		if (getLastMemberFromStack() instanceof AOJAspectDeclaration) 
			((AOJAspectDeclaration) getLastMemberFromStack()).getMembers().getDeclareErrors().add(aojNode);
		elementStack.push(aojNode);
		return super.visit(node);
	}
	
	@Override
	public void endVisit(DeclareErrorDeclaration node) {
		elementStack.pop();
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(DeclareWarningDeclaration node) {
		AOJDeclareCompilationEnforcement aojNode = new AOJDeclareCompilationEnforcement(node, (AOJProgramElement)getLastMemberFromStack(), node.getPointcut(), node.getMessage());
		if (getLastMemberFromStack() instanceof AOJAspectDeclaration) 
			((AOJAspectDeclaration) getLastMemberFromStack()).getMembers().getDeclareWarnings().add(aojNode);
		elementStack.push(aojNode);			

		return super.visit(node);
	}

	@Override
	public void endVisit(DeclareWarningDeclaration node) {
		elementStack.pop();		
		super.endVisit(node);
	}
	
	public boolean visit(DeclareParentsDeclaration node) {
		AOJDeclareParents aojNode = new AOJDeclareParents(node, (AOJProgramElement)getLastMemberFromStack());
		if (getLastMemberFromStack() instanceof AOJAspectDeclaration) 
			((AOJAspectDeclaration) getLastMemberFromStack()).getMembers().getDeclareParents().add(aojNode);
		elementStack.push(aojNode);			
		return super.visit(node);
	}

	@Override
	public void endVisit(DeclareParentsDeclaration node) {
		elementStack.pop();		
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(MarkerAnnotation node) {
		AOJAnnotationDeclaration aojNode = new AOJAnnotationDeclaration(node,(AOJProgramElement)getLastMemberFromStack());			
		if (getLastMemberFromStack() instanceof AOJAnnotable) 
			((AOJAnnotable)getLastMemberFromStack()).getAnnotations().add(aojNode);
		elementStack.push(aojNode);					
		return super.visit(node);
	}

	@Override
	public void endVisit(MarkerAnnotation node) {
		elementStack.pop();		
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(AnnotationTypeMemberDeclaration node) {
		return super.visit(node);
	}
	
	@Override
	public boolean visit(AnnotationTypeDeclaration node) {
		AOJAnnotationTypeDeclaration aojNode = new AOJAnnotationTypeDeclaration(node, cUnit);
		cUnit.addType(aojNode);
		getElementStack().push(aojNode);
		return super.visit(node);
	}

	@Override
	public void endVisit(AnnotationTypeDeclaration node) {
		elementStack.pop();		
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(CflowPointcut node) {
		if (getLastMemberFromStack() instanceof AOJClassDeclaration) 
			((AOJClassDeclaration) getLastMemberFromStack()).getMembers().getcFlowPointcuts().add(new AOJPointcutCFlow(node, (AOJProgramElement)getLastMemberFromStack()));
		return super.visit(node);
	}
	
	@Override
	public boolean visit(InterTypeFieldDeclaration node) {
		AOJDeclareField aojNode = new AOJDeclareField(node, (AOJProgramElement)getLastMemberFromStack());
		if (getLastMemberFromStack() instanceof AOJAspectDeclaration) 
			((AOJAspectDeclaration) getLastMemberFromStack()).getMembers().getDeclareFields().add(aojNode);
		elementStack.push(aojNode);					
		return super.visit(node);
	}

	@Override
	public void endVisit(InterTypeFieldDeclaration node) {
		elementStack.pop();				
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(InterTypeMethodDeclaration node) {
		System.out.println("[InterTypeMethodDeclaration] " + node.getName());
		AOJDeclareMethod aojNode = new AOJDeclareMethod(node, (AOJProgramElement)getLastMemberFromStack());
		if (getLastMemberFromStack() instanceof AOJAspectDeclaration) 
			((AOJAspectDeclaration) getLastMemberFromStack()).getMembers().getAllDeclareMethods().add(aojNode);
		elementStack.push(aojNode);					

		return super.visit(node);
	}
	
	@Override
	public void endVisit(InterTypeMethodDeclaration node) {
		elementStack.pop();					
		super.endVisit(node);
	}

	@Override
	public boolean visit(Block node) {
		return super.visit(node);
	}

	@Override
	public boolean visit(IfStatement node) {
		AOJIfStatement aojNode = new AOJIfStatement(node, (AOJProgramElement)getLastMemberFromStack());
		
		AOJBehaviourKind behaviourKind = getAOJBehaviourKind(getLastMemberFromStack());
		if (behaviourKind != null)
			behaviourKind.getStatements().add(aojNode);

		
//		if (getLastMemberFromStack() instanceof AOJBehaviourKind) 
//			((AOJBehaviourKind) getLastMemberFromStack()).getStatements().add(aojNode);
		
		elementStack.push(aojNode);
		return super.visit(node);
		
	}
	
	@Override
	public void endVisit(IfStatement node) {
		elementStack.pop();					
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(ForStatement node) {
		return super.visit(node);
	}

	@Override
	public boolean visit(WhileStatement node) {
		return super.visit(node);
	}

	@Override
	public boolean visit(DoStatement node) {
		return super.visit(node);
	}

	@Override
	public boolean visit(TryStatement node) {
		return super.visit(node);
	}

	@Override
	public boolean visit(SwitchStatement node) {
		return super.visit(node);
	}

	@Override
	public boolean visit(SynchronizedStatement node) {
		return super.visit(node);
	}
	
	@Override
	public boolean visit(ReturnStatement node) {
		return super.visit(node);
	}

	@Override
	public boolean visit(ThrowStatement node) {
		return super.visit(node);
	}
	
	@Override
	public boolean visit(BreakStatement node) {
		return super.visit(node);
	}

	@Override
	public boolean visit(ContinueStatement node) {
		return super.visit(node);
	}
	
	@Override
	public boolean visit(ExpressionStatement node) {
		AOJExpressionStatement expression = new AOJExpressionStatement(node, (AOJProgramElement)getLastMemberFromStack());

		AOJBehaviourKind behaviourKind = getAOJBehaviourKind(getLastMemberFromStack());
		if (behaviourKind != null)
			behaviourKind.getStatements().add(expression);
		
		
//		if (getLastMemberFromStack() instanceof AOJBehaviourKind) 
//			((AOJBehaviourKind) getLastMemberFromStack()).getStatements().add(expression);
		
		elementStack.push(expression);	
		return super.visit(node);
	}
	
	@Override
	public void endVisit(ExpressionStatement node) {
		elementStack.pop();					
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(LabeledStatement node) {
		return super.visit(node);
	}
	
	@Override
	public boolean visit(AssertStatement node) {
		return super.visit(node);
	}
	
	@Override
	public boolean visit(EnhancedForStatement node) {
		
		AOJEnhancedForStatement aojNode = new AOJEnhancedForStatement(node, (AOJProgramElement)getLastMemberFromStack());
		AOJBehaviourKind behaviourKind = getAOJBehaviourKind(getLastMemberFromStack());
		if (behaviourKind != null)
			behaviourKind.getStatements().add(aojNode);

//		if (getLastMemberFromStack() instanceof AOJBehaviourKind) 
//			((AOJBehaviourKind) getLastMemberFromStack()).getStatements().add(aojNode);
		
		elementStack.push(aojNode);		
		return super.visit(node);
	}
	
	@Override
	public void endVisit(EnhancedForStatement node) {
		elementStack.pop();					
		super.endVisit(node); 
	}


	@Override
	public boolean visit(ConstructorInvocation node) {
		return super.visit(node);
	}

	@Override
	public boolean visit(SuperConstructorInvocation node) {
		return super.visit(node);
	}

	@Override
	public boolean visit(BeforeAdviceDeclaration node) {
		AOJAdviceDeclaration aojNode = new AOJAdviceDeclaration(node, (AOJProgramElement)getLastMemberFromStack(), AOJAdviceKindEnum.BEFORE);
//		AOJAdviceBefore aojNode = new AOJAdviceBefore(node, (AOJProgramElement)getLastMemberFromStack());
		if (getLastMemberFromStack() instanceof AOJAspectDeclaration) 
			((AOJAspectDeclaration) getLastMemberFromStack()).getMembers().getAllAdvices().add(aojNode);
		elementStack.push(aojNode);
		return super.visit(node);
	}

	@Override
	public void endVisit(BeforeAdviceDeclaration node) {
		elementStack.pop();
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(AfterAdviceDeclaration node) {
		AOJAdviceDeclaration aojNode = new AOJAdviceDeclaration(node, (AOJProgramElement)getLastMemberFromStack(), AOJAdviceKindEnum.AFTER);

		//AOJAdviceAfter aojNode = new AOJAdviceAfter(node, (AOJProgramElement)getLastMemberFromStack());
		if (getLastMemberFromStack() instanceof AOJAspectDeclaration) 
			((AOJAspectDeclaration) getLastMemberFromStack()).getMembers().getAllAdvices().add(aojNode);
		elementStack.push(aojNode);
		return super.visit(node);
	}

	@Override
	public void endVisit(AfterAdviceDeclaration node) {
		elementStack.pop();
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(AfterReturningAdviceDeclaration node) {
		AOJAdviceDeclaration aojNode = new AOJAdviceDeclaration(node, (AOJProgramElement)getLastMemberFromStack(), AOJAdviceKindEnum.AFTERRETURNING);
//		AOJAdviceAfterReturning aojNode = new AOJAdviceAfterReturning(node, (AOJProgramElement)getLastMemberFromStack());
		if (getLastMemberFromStack() instanceof AOJAspectDeclaration) 
			((AOJAspectDeclaration) getLastMemberFromStack()).getMembers().getAllAdvices().add(aojNode);
		elementStack.push(aojNode);
		return super.visit(node);
	}

	@Override
	public void endVisit(AfterReturningAdviceDeclaration node) {
		elementStack.pop();
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(AfterThrowingAdviceDeclaration node) {
		AOJAdviceDeclaration aojNode = new AOJAdviceDeclaration(node, (AOJProgramElement)getLastMemberFromStack(), AOJAdviceKindEnum.AFTERTHROWING);
//		AOJAdviceAfterThrowing aojNode = new AOJAdviceAfterThrowing(node, (AOJProgramElement)getLastMemberFromStack());
		if (getLastMemberFromStack() instanceof AOJAspectDeclaration) 
			((AOJAspectDeclaration) getLastMemberFromStack()).getMembers().getAllAdvices().add(aojNode);
		elementStack.push(aojNode);
		return super.visit(node);
	}

	@Override
	public void endVisit(AfterThrowingAdviceDeclaration node) {
		elementStack.pop();
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(AroundAdviceDeclaration node) {
		AOJAdviceDeclaration aojNode = new AOJAdviceDeclaration(node, (AOJProgramElement)getLastMemberFromStack(), AOJAdviceKindEnum.AROUND);
//		AOJAdviceAround aojNode = new AOJAdviceAround(node, (AOJProgramElement)getLastMemberFromStack());
		if (getLastMemberFromStack() instanceof AOJAspectDeclaration) 
			((AOJAspectDeclaration) getLastMemberFromStack()).getMembers().getAllAdvices().add(aojNode);
		elementStack.push(aojNode);
		return super.visit(node);
	}
	
	@Override
	public void endVisit(AroundAdviceDeclaration node) {
		elementStack.pop();
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(PointcutDeclaration node) {
		AOJPointcutDeclaration aojNode = new AOJPointcutDeclaration(node, (AOJProgramElement)getLastMemberFromStack());

		if (getLastMemberFromStack() instanceof AOJAspectDeclaration) 
			((AOJAspectDeclaration) getLastMemberFromStack()).getMembers().getPointcuts().add(aojNode);
		else  if (getLastMemberFromStack() instanceof AOJClassDeclaration) 
			((AOJClassDeclaration) getLastMemberFromStack()).getMembers().getPointcuts().add(aojNode);

		elementStack.push(aojNode);
		return super.visit(node);
	}	

	@Override
	public void endVisit(PointcutDeclaration node) {
		elementStack.pop();
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(DeclareSoftDeclaration node) {
		AOJDeclareSoft aojNode = new AOJDeclareSoft(node, (AOJProgramElement)getLastMemberFromStack());
		if (getLastMemberFromStack() instanceof AOJAspectDeclaration) 
			((AOJAspectDeclaration) getLastMemberFromStack()).getMembers().getDeclareSofts().add(aojNode);
		elementStack.push(aojNode);
		return super.visit(node);
	}
	
	@Override
	public void endVisit(DeclareSoftDeclaration node) {
		elementStack.pop();	
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(VariableDeclarationStatement node) {
		return super.visit(node);
	}
	
	public Object getLastMemberFromStack() {
		return elementStack.peek();
	}

	public static Stack<Object> getElementStack() {
		return elementStack;
	}
	
	public static AOJBehaviourKind getAOJBehaviourKind(Object object){
		if (object == null)
			return null;
		if (object instanceof AOJMethodDeclaration || object instanceof AOJConstructorDeclaration)
			return (AOJBehaviourKind)object;
		else
			return getAOJBehaviourKind(((AOJProgramElement)object).getOwner());
	}
	
	public static AOJClassHolder getAOJClassHolder(Object object){
//		if (object == null)
//			return null;
//		if (object instanceof AOJConstructorDeclaration || object instanceof AOJMethodDeclaration)
//			return (AOJClassHolder)object;
//		else
//			return getAOJClassHolder(((AOJProgramElement)object).getOwner());
		
		if (object == null)
			return null;
		if (object instanceof AOJClassHolder)
			return (AOJClassHolder)object;
		else
			return getAOJClassHolder(((AOJProgramElement)object).getOwner());

	}
	
	

//	public int getCurrentLineCount() {
//		return lineCount;
//	}
}