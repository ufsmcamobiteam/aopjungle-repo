package br.ufsm.aopjungle;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import org.eclipse.core.resources.IProject;

public class ChangeLoader {
	private static ClassLoader prevCl;

	public static void unChange () {
        Thread.currentThread().setContextClassLoader(prevCl);
	}
	
	public static void change (IProject project) {
		URL url = null;
		try {
			url = new URL(project.getFullPath().toString());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		prevCl = Thread.currentThread().getContextClassLoader();
		ClassLoader urlCl = URLClassLoader.newInstance(new URL[]{url}, prevCl);
	        Thread.currentThread().setContextClassLoader(urlCl);
//		    Context ctx = new InitialContext();
//		    ctx.close();
	}
}
