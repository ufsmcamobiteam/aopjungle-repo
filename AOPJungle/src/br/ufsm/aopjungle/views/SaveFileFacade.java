package br.ufsm.aopjungle.views;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class SaveFileFacade {
	private FileDialog dlg;

	public SaveFileFacade(Shell shell) {
		dlg = new FileDialog(shell, SWT.SAVE);
	}

	public String open() {
		String fileName = null;
		boolean done = false;

		while (!done) {
			fileName = dlg.open();
			if (fileName == null) {
				done = true;
			} else {
				File file = new File(fileName);
				if (file.exists()) {
					MessageBox mb = new MessageBox(dlg.getParent(), SWT.ICON_WARNING
							| SWT.YES | SWT.NO);

					mb.setMessage("The file " + fileName + " already exists. Do you want to replace it?");
					done = mb.open() == SWT.YES;
				} else {
					done = true;
				}
			}
		}
		return fileName;
	}

	public String getFileName() {
		return dlg.getFileName();
	}

	public List<String> getFileNames() {
		return Arrays.asList(dlg.getFileNames());
	}

	public List<String> getFilterExtensions() {
		return Arrays.asList(dlg.getFilterExtensions());
	}

	public List<String> getFilterNames() {
		return Arrays.asList(dlg.getFilterNames());
	}

	public String getFilterPath() {
		return dlg.getFilterPath();
	}

	public void setFileName(String string) {
		dlg.setFileName(string);
	}

	public void setFilterExtensions(List<String> extensions) {
		dlg.setFilterExtensions((String[])extensions.toArray());
	}

	public void setFilterNames(List<String> names) {
		dlg.setFilterNames((String[])names.toArray());
	}

	public void setFilterPath(String string) {
		dlg.setFilterPath(string);
	}

	public Shell getParent() {
		return dlg.getParent();
	}

	public int getStyle() {
		return dlg.getStyle();
	}

	public String getText() {
		return dlg.getText();
	}

	public void setText(String string) {
		dlg.setText(string);
	}
}