package br.ufsm.aopjungle.views;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import org.aspectj.org.eclipse.jdt.core.dom.AnonymousClassDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.TypePattern;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.DrillDownAdapter;
import org.eclipse.ui.part.ViewPart;

import br.ufsm.aopjungle.astor.AbstractMethodIntroduction;
import br.ufsm.aopjungle.astor.AdviceCodeDuplication;
import br.ufsm.aopjungle.astor.AnonymousPointcutDefinition;
import br.ufsm.aopjungle.astor.AstorSession;
import br.ufsm.aopjungle.astor.ConvertFunctionalInterfaceToDefaultFunctionalInterface;
import br.ufsm.aopjungle.astor.ConvertITMDtoDefaultMethod;
import br.ufsm.aopjungle.astor.ConvertInterfaceToDefaultMethod;
import br.ufsm.aopjungle.astor.DivergentChanges;
import br.ufsm.aopjungle.astor.DoublePersonality;
import br.ufsm.aopjungle.astor.EncloseForWithIfToFilter;
import br.ufsm.aopjungle.astor.EncloseFunctionalInterface;
import br.ufsm.aopjungle.astor.EncloseSort;
import br.ufsm.aopjungle.astor.FeatureEnvy;
import br.ufsm.aopjungle.astor.EncloseEnhancedFor;
import br.ufsm.aopjungle.astor.LargeAdvice;
import br.ufsm.aopjungle.astor.LargeAspect;
import br.ufsm.aopjungle.astor.LargePointcut;
import br.ufsm.aopjungle.astor.LazyAspect;
import br.ufsm.aopjungle.astor.SpeculativeGenerality;
import br.ufsm.aopjungle.binding.AOJBinding;
import br.ufsm.aopjungle.builder.AOJExporterXML;
import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.builder.AOJungleBuilder;
import br.ufsm.aopjungle.hql.AOJProjectDAO;
import br.ufsm.aopjungle.labs.AOJBindingOptionEnum;
import br.ufsm.aopjungle.labs.AffectedFilesReporter;
import br.ufsm.aopjungle.log.AOJLogger;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAdviceDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareCompilationEnforcement;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareField;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareMethod;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareParents;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclarePrecedence;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareSoft;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutExpression;
import br.ufsm.aopjungle.metamodel.commons.AOJAnnotable;
import br.ufsm.aopjungle.metamodel.commons.AOJAnnotationDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJAttributeDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJBehaviourKind;
import br.ufsm.aopjungle.metamodel.commons.AOJBindble;
import br.ufsm.aopjungle.metamodel.commons.AOJClassHolder;
import br.ufsm.aopjungle.metamodel.commons.AOJCompilationUnit;
import br.ufsm.aopjungle.metamodel.commons.AOJConcreteTypeMember;
import br.ufsm.aopjungle.metamodel.commons.AOJContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJException;
import br.ufsm.aopjungle.metamodel.commons.AOJInterfaceble;
import br.ufsm.aopjungle.metamodel.commons.AOJMember;
import br.ufsm.aopjungle.metamodel.commons.AOJModifier;
import br.ufsm.aopjungle.metamodel.commons.AOJModifierEnum;
import br.ufsm.aopjungle.metamodel.commons.AOJPackageDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJParameter;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.commons.AOJReferencedType;
import br.ufsm.aopjungle.metamodel.java.AOJAnonymousClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJConstructorDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJEnumDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration;
import br.ufsm.aopjungle.metrics.AOJContainerMetrics;
import br.ufsm.aopjungle.util.IconProvider;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
public class AOPJungleView extends ViewPart { 
	@XStreamOmitField
	public static final String ID = "br.ufsm.aopjungle.views.AOPJungleView";
	@XStreamOmitField
	private TreeViewer viewer;
	@XStreamOmitField
	private DrillDownAdapter drillDownAdapter;
	@XStreamOmitField
	private Action actionExportXML;
	@XStreamOmitField
	private Action actionExecuteAstor;
	@XStreamOmitField
	private Action actionExecuteQuery;
	@XStreamOmitField
	private Action doubleClickAction;

	class TreeObject implements IAdaptable {
		@XStreamOmitField
		private String name;
		@XStreamOmitField
		private TreeParent parent;
		
		public TreeObject(String name) {
			this.name = name;
		}
		public String getName() {
			return name;
		}
		public void setParent(TreeParent parent) {
			this.parent = parent;
		}
		public TreeParent getParent() {
			return parent;
		}
		public String toString() {
			return getName();
		}
		@SuppressWarnings("rawtypes")
		public Object getAdapter(Class key) {
			return null;
		}
	}
	
	class TreeParent extends TreeObject {
		@SuppressWarnings("rawtypes")
		@XStreamOmitField
		private ArrayList children;
		@SuppressWarnings("rawtypes")
		public TreeParent(String name) {
			super(name);
			children = new ArrayList();
		}
		@SuppressWarnings("unchecked")
		public void addChild(TreeObject child) {
			children.add(child);
			child.setParent(this);
		}
		public void removeChild(TreeObject child) {
			children.remove(child);
			child.setParent(null);
		}
		@SuppressWarnings("unchecked")
		public TreeObject [] getChildren() {
			return (TreeObject [])children.toArray(new TreeObject[children.size()]);
		}
		public boolean hasChildren() {
			return children.size()>0;
		}
	}

	
	class ViewContentProvider implements IStructuredContentProvider, 
										 ITreeContentProvider {
		@XStreamOmitField
		private AOJungleBuilder aopjunglebuilder;
		@XStreamOmitField
		private TreeParent invisibleRoot;

		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
		public void dispose() {
		}
		public Object[] getElements(Object parent) {
			if (parent.equals(getViewSite())) {
				if (invisibleRoot==null) initialize();
				return getChildren(invisibleRoot);
			}
			return getChildren(parent);
		}
		public Object getParent(Object child) {
			if (child instanceof TreeObject) {
				return ((TreeObject)child).getParent();
			}
			return null;
		}
		public Object [] getChildren(Object parent) {
			if (parent instanceof TreeParent) {
				return ((TreeParent)parent).getChildren();
			}
			return new Object[0];
		}
		public boolean hasChildren(Object parent) {
			if (parent instanceof TreeParent)
				return ((TreeParent)parent).hasChildren();
			return false;
		}
		
		private void loadContainerMetrics(AOJContainerMetrics metrics,
				TreeParent node) {
			TreeParent metricNode = new TreeParent("Metrics");
			node.addChild(metricNode);
			TreeParent locNode = new TreeParent("LOC " + metrics.getNumberOfLines());
			metricNode.addChild(locNode);
			TreeParent nocNode = new TreeParent("NOC " + metrics.getNumberOfClasses());
			metricNode.addChild(nocNode);
			TreeParent noaNode = new TreeParent("NOA " + metrics.getNumberOfAspects());
			metricNode.addChild(noaNode);
			TreeParent noiNode = new TreeParent("NOI " + metrics.getNumberOfInterfaces());
			metricNode.addChild(noiNode);
			TreeParent noeNode = new TreeParent("NOE " + metrics.getNumberOfEnums());
			metricNode.addChild(noeNode);
			TreeParent nonNode = new TreeParent("NON " + metrics.getNumberOfAnnotationDecls());
			metricNode.addChild(nonNode);
	
		}
		
//		private void loadingProjectInClassPath(){
//			List<IJavaProject> javaProjects = new ArrayList<IJavaProject>();
//			IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
//			for(IProject project: projects){
//				 try {
//					project.open(null /* IProgressMonitor */);
//					 IJavaProject javaProject = JavaCore.create(project);
//					 javaProjects.add(javaProject);
//				} catch (CoreException e) {
//					e.printStackTrace();
//				}
//			}
//			
//			List<URLClassLoader> loaders = new ArrayList<URLClassLoader>();
//			for(IJavaProject project : javaProjects){
//				try {
//					String[] classPathEntries = JavaRuntime.computeDefaultRuntimeClassPath(project);
//					List<URL> urlList = new ArrayList<URL>();
//					for (int i = 0; i < classPathEntries.length; i++) {
//						String entry = classPathEntries[i];
//						IPath path = new Path(entry);
//						URL url = url = path.toFile().toURI().toURL();
//						urlList.add(url);
//					}
//					//---------------------------------------
//					ClassLoader parentClassLoader = project.getClass().getClassLoader();
//					URL[] urls = (URL[]) urlList.toArray(new URL[urlList.size()]);
//					URLClassLoader classLoader = new URLClassLoader(urls, parentClassLoader);
//					loaders.add(classLoader);
//
//				} catch (CoreException e) {
//					e.printStackTrace();
//				} catch (MalformedURLException e) {
//					e.printStackTrace();
//				}	
//			}
//			
////			List<URLClassLoader> loaders = new ArrayList<URLClassLoader>();
////			for (IJavaProject javaProject : javaProjects) {
////				loaders.add(getProjectClassLoader(javaProject));
////			}
//			
//			for (URLClassLoader loader : loaders) {
//				try {
//					Class<?> clazz = loader.loadClass("fi.Interface04");
//					System.out.println(clazz.getName());
//				} catch (ClassNotFoundException e) {
//				}
//			}
//			//Class c = getClass("fi.Interface04");
//			//System.out.println(c.getName());
//
//
//		}
		
//		private Class getClass(String fullyQualifiedName){
//			for (URLClassLoader loader : loaders) {
//				try {
//					Class<?> clazz = loader.loadClass(fullyQualifiedName);
//					return clazz;
//				} catch (ClassNotFoundException e) {
//				}
//			}
//			return null;
//
//		}
//		
		private void initialize() {
			AOJLogger.getLogger().info("Initializing plugin AOP Jungle...");
			
			aopjunglebuilder = new AOJungleBuilder();
			List<TreeParent> projectTree = new ArrayList<TreeParent>(); 
			List<AOJProject> projects = aopjunglebuilder.getWorkspace().getProjects();
			try {
				aopjunglebuilder.getWorkspace().loadProjects(AOJBindingOptionEnum.BINDING_ON);
			} catch (JavaModelException e) {
				e.printStackTrace();
			} catch (CoreException e) {
				e.printStackTrace();
			}
			
			for (AOJProject project : projects) {		
//				for (AOJAspectBindingModel b : project.getBindings())
//					AOJAdviceListener.print(b);
				TreeParent projectNode = new TreeParent(project.getName());  
				projectTree.add(projectNode);
				loadContainerMetrics (project.getMetrics(), projectNode);
				List<AOJPackageDeclaration> packages = project.getPackages();
				for (AOJPackageDeclaration pack : packages) {
					TreeParent packageNode = new TreeParent(pack.getMetaName());
					projectNode.addChild(packageNode);
					loadContainerMetrics (pack.getMetrics(), packageNode);
					List<AOJCompilationUnit> cUnits = pack.getCompilationUnits();
					for (AOJCompilationUnit cUnit : cUnits) {
						for (AOJContainer typeDeclaration : cUnit.getDeclaredTypes()) {
							loadTypeDeclaration(packageNode, typeDeclaration);
						}
					}
				}	
			}

			TreeParent root = new TreeParent("AOP Projects");
			for (TreeParent p : projectTree) {
				root.addChild(p);				
			}
				
			invisibleRoot = new TreeParent("");
			invisibleRoot.addChild(root);
		}
		
		private void loadTypeDeclaration(TreeParent upperNode, AOJContainer typeDeclaration) {
			TreeParent typeNode = new TreeParent(typeDeclaration.getMetaName()+"."+typeDeclaration.getName());
			upperNode.addChild(typeNode);
			loadParent(typeDeclaration, typeNode);
			loadTypeModifier(typeDeclaration, typeNode);
			loadAttributes(typeDeclaration, typeNode);
			loadMethods(typeDeclaration, typeNode);
			loadMetrics(typeDeclaration, typeNode);
			loadFunctionalInterface(typeDeclaration, typeNode);
			
			if (typeDeclaration instanceof AOJBindble)
				loadBindings((AOJBindble)typeDeclaration, typeNode);
			if (typeDeclaration.getMembers() instanceof AOJConcreteTypeMember)
				loadConstructors ((AOJConcreteTypeMember)typeDeclaration.getMembers(), typeNode);
			
			if (typeDeclaration instanceof AOJClassHolder) 
				loadNestedClasses((AOJClassHolder)typeDeclaration, typeNode);

			if (typeDeclaration instanceof AOJAnnotable) 
				loadAnnotations((AOJAnnotable)typeDeclaration, typeNode, true);

			if (typeDeclaration instanceof AOJInterfaceble)
				loadInterfaces((AOJInterfaceble)typeDeclaration, typeNode);
			
			if (typeDeclaration instanceof AOJAspectDeclaration) {
				AOJAspectDeclaration aspect = (AOJAspectDeclaration)typeDeclaration;
				loadAspectView(aspect,typeNode);
			}
			
			if (typeDeclaration instanceof AOJEnumDeclaration)
				loadEnumConstants((AOJEnumDeclaration)typeDeclaration, typeNode);
		}
		
		/**
		 * @param typeDeclaration
		 * @param typeNode
		 */
		private void loadMetrics(AOJContainer typeDeclaration,
				TreeParent typeNode) {
			TreeParent metricNode = new TreeParent("Metrics");
			typeNode.addChild(metricNode);
			TreeParent locNode = new TreeParent("LOC " + typeDeclaration.getMetrics().getNumberOfLines());
			metricNode.addChild(locNode);
			TreeParent dtreeNode = new TreeParent("NDESC " + typeDeclaration.getMetrics().getNumberOfDescendents());
			metricNode.addChild(dtreeNode);
			TreeParent upsizeNode = new TreeParent("NASC " + typeDeclaration.getMetrics().getNumberOfAscendents());
			metricNode.addChild(upsizeNode);
			TreeParent affInNode = new TreeParent("NAFFin " + typeDeclaration.getMetrics().getNumberOfInAffects());
			metricNode.addChild(affInNode);
			TreeParent affOutNode = new TreeParent("NAFFout " + typeDeclaration.getMetrics().getNumberOfOutAffects());
			metricNode.addChild(affOutNode);
		}
		
		private void loadEnumConstants(AOJEnumDeclaration typeDeclaration,
				TreeParent typeNode) {
			TreeParent node = newPlaceHolder(typeNode, "Constants");
			for (String c : typeDeclaration.getConstants()) {
				TreeParent nodeC = new TreeParent(c);
				node.addChild(nodeC);
			}	
		}
/*
		private void loadInnerClass(AOJTypeDeclaration typeDeclaration,
				TreeParent typeNode) {
			if (typeDeclaration instanceof AOJClassDeclaration) {
				String classType = "";
				if ( ((AOJClassDeclaration) typeDeclaration).isAnonymous() ) 
					classType = "Anonymous.True";
				else if ( ((AOJClassDeclaration) typeDeclaration).isInnerClass() ) 
					classType = "Innerclass.True";
				
				if (! classType.isEmpty() ) 
					newPlaceHolder(typeNode, classType);
			}
		}
*/		
		private void loadInterfaces(AOJInterfaceble typeInterfaceble,
				TreeParent typeNode) {
			
			if (typeInterfaceble.getImplementedInterfaces().size() > 0) {
				TreeParent node = newPlaceHolder(typeNode, "Implements");
				for (AOJReferencedType intf : typeInterfaceble.getImplementedInterfaces()) {
					TreeParent nodeIntf = new TreeParent(intf.getName());
					node.addChild(nodeIntf);
				}				
			}
		}
		
		private TreeParent newPlaceHolder(TreeParent typeNode, String name) {
			TreeParent node = new TreeParent(name); 
			typeNode.addChild(node);
			return node;
		}
		
		private void loadParent(AOJContainer typeDeclaration,
				TreeParent typeNode) {
			if (typeDeclaration.getParent() != null) {
				TreeParent node = newPlaceHolder(typeNode, "Extends");
				TreeParent nodeExtends = new TreeParent(typeDeclaration.getParent().getFullQualifiedName());
				node.addChild(nodeExtends);
			}	
		}
		

		private void loadMethods(AOJContainer typeDeclaration, TreeParent typeNode) {
			for (AOJBehaviourKind methodDeclaration : typeDeclaration.getMembers().getMethods()) {
				TreeParent methodNode = new TreeParent(methodDeclaration.getMetaName()+"."+methodDeclaration.getName());
				typeNode.addChild(methodNode);
				loadAnnotations(methodDeclaration, methodNode, true);
				loadMethodReturnType(methodDeclaration, methodNode);
				loadMethodParams(methodDeclaration, methodNode);
				loadMethodThrowExceptions(methodDeclaration, methodNode);
				loadMethodModifier(methodDeclaration, methodNode);
				loadNestedClasses(methodDeclaration, methodNode);
				loadMethodMetrics(methodDeclaration, methodNode);
				if (typeDeclaration instanceof AOJBindble)
					loadBindings((AOJBindble)methodDeclaration, methodNode);
				
			}
		}
		
		private void loadConstructors(AOJConcreteTypeMember members,
				TreeParent typeNode) {
			for (AOJConstructorDeclaration constructorDeclaration : members.getConstructors()) {
				TreeParent constructorNode = new TreeParent(constructorDeclaration.getMetaName()+"."+constructorDeclaration.getName());
				typeNode.addChild(constructorNode);
				loadMethodParams(constructorDeclaration, constructorNode);
				loadMethodThrowExceptions(constructorDeclaration, constructorNode);
				loadMethodModifier(constructorDeclaration, constructorNode);
				loadMethodMetrics(constructorDeclaration, constructorNode);				
				if (constructorDeclaration instanceof AOJBindble)
					loadBindings((AOJBindble)constructorDeclaration, constructorNode);
			}
		}
			
		private void loadMethodMetrics(AOJBehaviourKind methodDeclaration,			
			TreeParent methodNode) {
			TreeParent metricNode = new TreeParent("Metrics");
			methodNode.addChild(metricNode);
			TreeParent nstaNode = new TreeParent("NSTA " + methodDeclaration.getMetrics().getNumberOfStatements());
			metricNode.addChild(nstaNode);
			TreeParent locNode = new TreeParent("LOC " + methodDeclaration.getMetrics().getNumberOfLines());
			metricNode.addChild(locNode);
			TreeParent affInNode = new TreeParent("NAFFin " + methodDeclaration.getMetrics().getNumberOfInAffects());
			metricNode.addChild(affInNode);
			TreeParent affOutNode = new TreeParent("NAFFout " + methodDeclaration.getMetrics().getNumberOfOutAffects());
			metricNode.addChild(affOutNode);
		}
		
		private void loadBindings(AOJBindble binding, TreeParent node) {
			TreeParent bindingNode = new TreeParent("Bindings");
			node.addChild(bindingNode);
			TreeParent out = new TreeParent("Out");
			bindingNode.addChild(out);
			TreeParent in = new TreeParent("In");
			bindingNode.addChild(in);
			
			for (AOJBinding b: binding.getBindingModel().getOut()) {
				TreeParent affectsNodeData = new TreeParent("(" + b.getKind() + ") " + b.getBindingHandler());
				out.addChild(affectsNodeData);
			}
			for (AOJBinding b: binding.getBindingModel().getIn()) {
				TreeParent affectsNodeData = new TreeParent("(" + b.getKind() + ") " + b.getBindingHandler());
				in.addChild(affectsNodeData);
			}
	}
	
	private void loadMethodModifier(AOJMember methodDeclaration,
				TreeParent methodNode) {
			AOJModifier aojMethodModifier = methodDeclaration.getModifiers();
            if (aojMethodModifier.getModifiers().size() > 0) {
    			TreeParent modifiersMethodNode = new TreeParent("Modifiers");
    			methodNode.addChild(modifiersMethodNode);
    			for (AOJModifierEnum modifier : aojMethodModifier.getModifiers()) {
    				TreeParent modifierNode = new TreeParent(modifier.toString().toLowerCase());
    				modifiersMethodNode.addChild(modifierNode);
    			}
            }
        }
		
		private void loadMethodThrowExceptions(
				AOJBehaviourKind methodDeclaration, TreeParent methodNode) {
			if (methodDeclaration.getThrownExceptions().size() > 0) {
				TreeParent exceptionsMethodNode = newPlaceHolder(methodNode, "ThrowExceptions");
				List<AOJException> exceptions = methodDeclaration.getThrownExceptions();
				int i = 0;
				for (AOJException exception : exceptions) {
					TreeParent exceptionMethodNode = new TreeParent("Type"+i+++"."+exception.getName());
					exceptionsMethodNode.addChild(exceptionMethodNode);
				}
			}		
		}
		
		private void loadMethodParams(AOJBehaviourKind methodDeclaration,
				TreeParent methodNode) {
			if (methodDeclaration.getParameters().size() > 0) {
				TreeParent paramsMethodNode = new TreeParent("Params");
				methodNode.addChild(paramsMethodNode);
				List<AOJParameter> params = methodDeclaration.getParameters();
				int i = 0;
				for (AOJParameter param : params) {
					TreeParent paramMethodNode = new TreeParent("Param"+i+++"."+param.getName());
					paramsMethodNode.addChild(paramMethodNode);
					TreeParent paramTypeMethodNode = new TreeParent("Type."+param.getType().getName());
					paramMethodNode.addChild(paramTypeMethodNode);
				}
			}	
		}
		
		private void loadMethodReturnType(
				AOJBehaviourKind methodDeclaration, TreeParent methodNode) {
			methodNode.addChild(new TreeParent("ReturnType." + methodDeclaration.getReturnType().getName()));
		}
		
		private void loadAttributes(AOJContainer typeDeclaration,
				TreeParent typeNode) {
			for (AOJAttributeDeclaration attributeDeclaration : typeDeclaration.getMembers().getAttributes()) {
				TreeParent attributeNode = new TreeParent(attributeDeclaration.getMetaName()+"."+attributeDeclaration.getName());
				typeNode.addChild(attributeNode);
				loadAnnotations((AOJAnnotable)attributeDeclaration, attributeNode, true);
				loadAttributeType(attributeDeclaration, attributeNode);
				loadAttributeModifier(attributeDeclaration, attributeNode);	
				loadNestedClasses(attributeDeclaration, attributeNode);
				if (attributeDeclaration instanceof AOJBindble)
					loadBindings((AOJBindble)attributeDeclaration, attributeNode);
			}
		}
		
		private void loadNestedClasses(AOJClassHolder holderDeclaration,
				TreeParent node) {
			for (AOJContainer clazz : holderDeclaration.getAnonymousClasses()) 
				loadTypeDeclaration (node, clazz);

			for (AOJContainer clazz : holderDeclaration.getInnerClasses())  
				loadTypeDeclaration (node, clazz);
		}
		
		private void loadAnnotations(
				AOJAnnotable node, TreeParent treeParent, boolean createPlaceHolder) {
			TreeParent treeNode = null;
			if ( (createPlaceHolder) && (node.getAnnotations().size() > 0)) {
				TreeParent annotationNode = new TreeParent("Annotations");
				treeParent.addChild(annotationNode);
				treeNode = annotationNode;
			} else {
				treeNode = treeParent;
			}			

			for (AOJAnnotationDeclaration annotationDeclaration : node.getAnnotations()) {
				TreeParent annotationDeclarationNode = new TreeParent("@"+annotationDeclaration.getName());
				treeNode.addChild(annotationDeclarationNode);
			}
		}

		private void loadAttributeModifier(
				AOJAttributeDeclaration attributeDeclaration,
				TreeParent attributeNode) {
			AOJModifier aojAttributeModifier = attributeDeclaration.getModifiers();						

			if (aojAttributeModifier.getModifiers().size() > 0) {
				TreeParent modifiersAttributeNode = new TreeParent("Modifiers");	
				attributeNode.addChild(modifiersAttributeNode);
				for (AOJModifierEnum modifier : aojAttributeModifier.getModifiers()) {
					TreeParent modifierNode = new TreeParent(modifier.toString().toLowerCase());
					modifiersAttributeNode.addChild(modifierNode);
				}
			} 
		}
			
		private void loadAttributeType(
				AOJAttributeDeclaration attributeDeclaration,
				TreeParent attributeNode) {
			attributeNode.addChild(new TreeParent("Type." + attributeDeclaration.getType().getName()));
		}
		
		private void loadTypeModifier(AOJContainer typeDeclaration,
				TreeParent typeNode) {
			AOJModifier aojTypeModifier = typeDeclaration.getModifiers();
			if (aojTypeModifier.getModifiers().size() > 0) {
				TreeParent modifiersTypeNode = new TreeParent("Modifiers");
				typeNode.addChild(modifiersTypeNode);
				for (AOJModifierEnum modifier : aojTypeModifier.getModifiers()) {
					TreeParent modifierNode = new TreeParent(modifier.toString().toLowerCase());
					modifiersTypeNode.addChild(modifierNode);
				}
			}
		}
		
		private void loadFunctionalInterface(AOJContainer typeDeclaration, TreeParent typeNode) {
			if ((typeDeclaration instanceof AOJAnonymousClassDeclaration) ) {
				AOJAnonymousClassDeclaration anonymous = (AOJAnonymousClassDeclaration) typeDeclaration;
				
				TreeParent modifiersTypeNode = new TreeParent("FunctionalInterface");
				typeNode.addChild(modifiersTypeNode);
				
				TreeParent modifierNode = new TreeParent(anonymous.isFunctionalInterface().toString());
				modifiersTypeNode.addChild(modifierNode);
			}
			if (typeDeclaration instanceof AOJInterfaceDeclaration) {
				AOJInterfaceDeclaration i = (AOJInterfaceDeclaration) typeDeclaration;
				
				TreeParent modifiersTypeNode = new TreeParent("FunctionalInterface");
				typeNode.addChild(modifiersTypeNode);
				
				TreeParent modifierNode = new TreeParent(i.isFunctionalInterface().toString());
				modifiersTypeNode.addChild(modifierNode);
			}
		}
			
		private void loadCompileWarningsAndErrors(AOJAspectDeclaration aspect, TreeParent typeNode) {
			if (aspect.getMembers().getDeclareWarnings() != null) {
				for (AOJDeclareCompilationEnforcement declare : aspect.getMembers().getDeclareWarnings()) {
					TreeParent compileWarningNode = new TreeParent("Compile.Warnings");
					typeNode.addChild(compileWarningNode);
					compileWarningNode.addChild(new TreeParent("Pointcut."+declare.getPointcutAsString()));
					compileWarningNode.addChild(new TreeParent("Message."+declare.getMessage()));
				}
			}	
				
			if (aspect.getMembers().getDeclareErrors() != null) {
				for (AOJDeclareCompilationEnforcement declare : aspect.getMembers().getDeclareErrors()) {
					TreeParent compileErrorsNode = new TreeParent("Compile.Errors");
					typeNode.addChild(compileErrorsNode);
					compileErrorsNode.addChild(new TreeParent("Pointcut."+declare.getPointcutAsString()));
					compileErrorsNode.addChild(new TreeParent("Message."+declare.getMessage()));
				}	
			}
		}
		
		private void loadAspectView(AOJAspectDeclaration aspect, TreeParent typeNode) {
			loadPrivileged(aspect, typeNode);
			loadDeclarePrecedences(aspect , typeNode);
			loadDeclareParents(aspect , typeNode);			
			loadDeclareFields(aspect , typeNode);
			loadDeclareMethods(aspect, typeNode);
			loadDeclareConstructors(aspect, typeNode);			
			loadDeclareSoft(aspect, typeNode);			
			loadCompileWarningsAndErrors(aspect, typeNode);
			loadPointcutDeclaration(aspect, typeNode);
			loadPerClause(aspect, typeNode);			
			loadAdvices(aspect, typeNode);
		}
			
		private void loadDeclareSoft(AOJAspectDeclaration aspect,
				TreeParent typeNode) {
			if (aspect.getMembers().getDeclareSofts().size() > 0) {
				 TreeParent node = newPlaceHolder(typeNode, "Declare.Soft");
				 for (AOJDeclareSoft declareSoft : aspect.getMembers().getDeclareSofts()) {
						loadDeclareSoftPointcuts(declareSoft, node);
						loadDeclareSoftExceptionPatterns(declareSoft, node);
						if (declareSoft instanceof AOJBindble)
							loadBindings((AOJBindble)declareSoft, node);
						loadDeclareSoftMetrics(declareSoft, node);
				 }
				 
			}			
		}
		
		private void loadDeclareSoftMetrics(AOJDeclareSoft declareSoft,
				TreeParent node) {
			TreeParent metricNode = new TreeParent("Metrics");
			node.addChild(metricNode);
			TreeParent affInNode = new TreeParent("NAFFin " + declareSoft.getMetrics().getNumberOfInAffects());
			metricNode.addChild(affInNode);
			TreeParent affOutNode = new TreeParent("NAFFout " + declareSoft.getMetrics().getNumberOfOutAffects());
			metricNode.addChild(affOutNode);
		}
		
		private void loadDeclareSoftExceptionPatterns(
				AOJDeclareSoft declareSoft, TreeParent nodeDeclareSoftName) {
			TreeParent node1 = newPlaceHolder(nodeDeclareSoftName, "Exception");
			node1.addChild(new TreeParent(declareSoft.getExceptionPatterns()));
		}
		
		private void loadDeclareSoftPointcuts(AOJDeclareSoft declareSoft,
				TreeParent nodeDeclareSoftName) {
			TreeParent node1 = newPlaceHolder(nodeDeclareSoftName, "Pointcut");
			node1.addChild(new TreeParent(declareSoft.getPointcut().getCode()));
		}
		
		private void loadPerClause(AOJAspectDeclaration aspect, TreeParent typeNode) {
			if ( aspect.getPerClause() != null ) {
				TreeParent node = new TreeParent("Instanciation");				
				typeNode.addChild(node);
				TreeParent node2 = new TreeParent("Type");
				node.addChild(node2);
				TreeParent node3 = new TreeParent("Pointcut");
				node.addChild(node3);				
				AOJPointcutExpression pcExpression = new AOJPointcutExpression(aspect.getPerClause());
				node2.addChild(new TreeParent(pcExpression.getTypeAsString(AOJPointcutExpression.FATSTRING)));
				node3.addChild(new TreeParent(pcExpression.getCode()));							
			}			
		}
		
//		private void loadPointcutCFlow(AOJAspectDeclaration aspect,
//				TreeParent typeNode) {
//			for (AOJPointcutCFlow pointcutCFlow : aspect.getMembers().getcFlowPointcuts()) {
//				TreeParent pointcutNode = new TreeParent(pointcutCFlow.getMetaName()+"."+pointcutCFlow.getName());
//				typeNode.addChild(pointcutNode);
//			}			
//		}
		
		private void loadDeclareConstructors(AOJAspectDeclaration aspect,
				TreeParent typeNode) {
			if (aspect.getMembers().getDeclareConstructors().size() > 0) {
				 TreeParent node = newPlaceHolder(typeNode, "Declare.Constructors");
				 for (AOJDeclareMethod declareConstructor : aspect.getMembers().getDeclareConstructors()) 
					 loadDeclareMethodNode(node, declareConstructor);	
			}	 
		}
			
		private void loadDeclareMethods(AOJAspectDeclaration aspect,
				TreeParent typeNode) {
			if (aspect.getMembers().getDeclareMethods().size() > 0) {
				 TreeParent node = newPlaceHolder(typeNode, "Declare.Methods");
				for (AOJDeclareMethod declareMethod : aspect.getMembers().getDeclareMethods())  
					loadDeclareMethodNode(node, declareMethod);
			}	
		}
		
		private void loadDeclareMethodMetrics(AOJDeclareMethod declareMethod,
				TreeParent node) {
			TreeParent metricNode = new TreeParent("Metrics");
			node.addChild(metricNode);
			TreeParent affInNode = new TreeParent("NAFFin " + declareMethod.getMetrics().getNumberOfInAffects());
			metricNode.addChild(affInNode);
			TreeParent affOutNode = new TreeParent("NAFFout " + declareMethod.getMetrics().getNumberOfOutAffects());
			metricNode.addChild(affOutNode);
		}
		
		private void loadDeclareMethodNode(TreeParent node,
				AOJDeclareMethod declareMethod) {
			TreeParent nodeDeclareMethodName = new TreeParent("Name."+declareMethod.getName());
			node.addChild(nodeDeclareMethodName);
			loadAnnotations(declareMethod, nodeDeclareMethodName, true);
			loadDeclareMethodReturnType(declareMethod, nodeDeclareMethodName);
			loadDeclareMethodParams(declareMethod, nodeDeclareMethodName);
			loadDeclareMethodThrowExceptions(declareMethod, nodeDeclareMethodName);
			loadDeclareMethodModifier(declareMethod, nodeDeclareMethodName);
			loadDeclareMethodApplyOnType(declareMethod, nodeDeclareMethodName);
			if (declareMethod instanceof AOJBindble)
				loadBindings((AOJBindble)declareMethod, nodeDeclareMethodName);
			loadDeclareMethodMetrics(declareMethod, node);
		}
		
		private void loadDeclareMethodApplyOnType(
				AOJDeclareMethod declareMethod, TreeParent node) {
			TreeParent nodeDeclareMethodOnType = new TreeParent("ApplyOnType."+declareMethod.getOnType());
			node.addChild(nodeDeclareMethodOnType);			
		}
			
		private void loadDeclareMethodModifier(AOJDeclareMethod declareMethod,
				TreeParent node) {
			AOJModifier aojMethodModifier = declareMethod.getModifiers();
			if (aojMethodModifier.getModifiers().size() > 0) {
				TreeParent modifiersMethodNode = new TreeParent("Modifiers");
				node.addChild(modifiersMethodNode);

				for (AOJModifierEnum modifier : aojMethodModifier.getModifiers()) {
					TreeParent modifierNode = new TreeParent(modifier.toString().toLowerCase());
					modifiersMethodNode.addChild(modifierNode);
				}
			}	
		}
		
		private void loadDeclareMethodThrowExceptions(
				AOJDeclareMethod declareMethod, TreeParent node) {		
			if (declareMethod.getThrownExceptions().size() > 0) {
				List<AOJException> exceptions = declareMethod.getThrownExceptions();
				TreeParent exceptionsMethodNode = newPlaceHolder(node, "ThrowExceptions");
				int i = 0;
				for (AOJException exception : exceptions) {
					TreeParent exceptionMethodNode = new TreeParent("Type"+i+++"."+exception.getName());
					exceptionsMethodNode.addChild(exceptionMethodNode);
				}				
			}
		}
		
		private void loadDeclareMethodParams(AOJDeclareMethod declareMethod,
				TreeParent node) {
			if (declareMethod.getParameters().size() > 0) {				
				TreeParent paramsMethodNode = new TreeParent("Params");
				node.addChild(paramsMethodNode);
				List<AOJParameter> params = declareMethod.getParameters();
				int i = 0;
				for (AOJParameter param : params) {
					TreeParent paramMethodNode = new TreeParent("Param"+i+++"."+param.getName());
					paramsMethodNode.addChild(paramMethodNode);
					TreeParent paramTypeMethodNode = new TreeParent("Type."+param.getType().getName());
					paramMethodNode.addChild(paramTypeMethodNode);
				}
			}	
		}
		
		private void loadDeclareMethodReturnType(
				AOJDeclareMethod declareMethod, TreeParent node) {
			node.addChild(new TreeParent("ReturnType." + declareMethod.getReturnType().getName()));
		}
		
		private void loadDeclareFields(AOJAspectDeclaration aspect,
				TreeParent typeNode) {

			TreeParent node = null;
			
			if (aspect.getMembers().getDeclareFields().size() > 0) {
				node = newPlaceHolder(typeNode, "Declare.Fields");
				for (AOJDeclareField declareField : aspect.getMembers().getDeclareFields()) {
					TreeParent nodeDeclareFieldName = new TreeParent("Name."+declareField.getName());
					node.addChild(nodeDeclareFieldName);

					loadDeclareFieldType(nodeDeclareFieldName, declareField);
					loadFieldApplyOnType(nodeDeclareFieldName, declareField);
					loadDeclareFieldModifier(nodeDeclareFieldName, declareField);
					if (declareField instanceof AOJBindble)
						loadBindings((AOJBindble)declareField, nodeDeclareFieldName);
					loadDeclareFieldsMetrics(declareField, node);
				}
			}
			
		}
		
		private void loadDeclareFieldsMetrics(AOJDeclareField declareField,
				TreeParent node) {
			TreeParent metricNode = new TreeParent("Metrics");
			node.addChild(metricNode);
			TreeParent affInNode = new TreeParent("NAFFin " + declareField.getMetrics().getNumberOfInAffects());
			metricNode.addChild(affInNode);
			TreeParent affOutNode = new TreeParent("NAFFout " + declareField.getMetrics().getNumberOfOutAffects());
			metricNode.addChild(affOutNode);
		}

		private void loadDeclareFieldModifier(TreeParent node,
				AOJDeclareField declareField) {
			AOJModifier aojDeclareFieldModifiers = declareField.getModifiers();

			if (aojDeclareFieldModifiers.getModifiers().size() > 0) {
				TreeParent nodeDeclareFieldModifier = new TreeParent("Modifiers");
				node.addChild(nodeDeclareFieldModifier);
				for (AOJModifierEnum modifier : aojDeclareFieldModifiers.getModifiers()) {
					TreeParent modifierNode = new TreeParent(modifier.toString().toLowerCase());
					nodeDeclareFieldModifier.addChild(modifierNode);
				}
			}
		}
		private void loadFieldApplyOnType(TreeParent node,
				AOJDeclareField declareField) {
			TreeParent nodeDeclareFieldOnType = new TreeParent("ApplyOnType."+declareField.getOnType());
			node.addChild(nodeDeclareFieldOnType);
		}

		private void loadDeclareFieldType(TreeParent node,
				AOJDeclareField declareField) {
			TreeParent nodeDeclareFieldType = new TreeParent("Type."+declareField.getType().getName());
			node.addChild(nodeDeclareFieldType);
		}

		private void loadDeclareParents(AOJAspectDeclaration aspect, TreeParent typeNode) {
			TreeParent node = null;
			
			List<AOJDeclareParents> extds = aspect.getMembers().getDeclareParentsExtension();
			List<AOJDeclareParents> impls = aspect.getMembers().getDeclareParentsImplementation();
			
			if ( (extds.size() > 0 ) || (impls.size() > 0) ) {
				node = newPlaceHolder(typeNode, "Declare.Parents");
				loadDeclareParentsMetrics(aspect, node);
			}	
			
			if (extds.size() > 0) {				
				TreeParent node2 = newPlaceHolder(node, "Extends");
				for (AOJDeclareParents declareParent : extds) {
					loadParents(node2, declareParent);
				}
			}
				
			if (impls.size() > 0) {								
				TreeParent node2 = newPlaceHolder(node, "Implements");								
				for (AOJDeclareParents declareParent : impls) {
					loadImplements(node2, declareParent);
				}
			}

			
		}
		
		private void loadDeclareParentsMetrics(AOJAspectDeclaration aspect,
				TreeParent node) {
			int inAff = 0, outAff = 0;
			for (AOJDeclareParents declareParents: aspect.getMembers().getDeclareParents()) {
				inAff += declareParents.getMetrics().getNumberOfInAffects();
				outAff += declareParents.getMetrics().getNumberOfOutAffects();
			}
			
			TreeParent metricNode = new TreeParent("Metrics");
			node.addChild(metricNode);
			TreeParent affInNode = new TreeParent("NAFFin " + inAff);
			metricNode.addChild(affInNode);
			TreeParent affOutNode = new TreeParent("NAFFout " + outAff);
			metricNode.addChild(affOutNode);
		}
		
		private void loadImplements(TreeParent node, AOJDeclareParents declareParent) {
			TreeParent nodeChild = new TreeParent("Type."+declareParent.getChildType().getTypePatternExpression().toString());
			node.addChild(nodeChild);
			for (TypePattern type  : declareParent.getParentType()) {
				TreeParent nodeParent = new TreeParent("Interface."+type.getTypePatternExpression().toString());
				node.addChild(nodeParent);
			}	
		}

		private void loadParents(TreeParent node,
				AOJDeclareParents declareParent) {
			TreeParent nodeChild = new TreeParent("Child."+declareParent.getChildType().getTypePatternExpression().toString());
			node.addChild(nodeChild);
			TreeParent nodeParent = new TreeParent("Super."+declareParent.getParentType().get(0).getTypePatternExpression().toString());
			node.addChild(nodeParent);
		}
		
		private void loadAdvices(AOJAspectDeclaration aspect, TreeParent typeNode) {
			loadAdviceBefore(aspect, typeNode);
			loadAdviceAfter(aspect, typeNode);
			loadAdviceAfterReturning(aspect, typeNode);
			loadAdviceAfterThrowing(aspect, typeNode);
			loadAdviceAround(aspect, typeNode);
		}
		private void loadAdviceAround(AOJAspectDeclaration aspect,
				TreeParent typeNode) {
			for (AOJAdviceDeclaration adviceDeclaration : aspect.getMembers().getAdvicesAround()) {
				TreeParent adviceNode = new TreeParent(adviceDeclaration.getMetaName());
				typeNode.addChild(adviceNode);
				loadAnnotations(adviceDeclaration, adviceNode, true);				
				loadAdvicePointcut(adviceDeclaration, adviceNode);
				loadAdviceParams(adviceDeclaration, adviceNode);
				loadAdviceThrowException(adviceDeclaration, adviceNode);
				loadAdviceReturnType(adviceDeclaration, adviceNode);
				if (adviceDeclaration instanceof AOJBindble)
					loadBindings((AOJBindble)adviceDeclaration, adviceNode);
				loadAdviceMetrics(adviceDeclaration, adviceNode);				
			}
		}
		
		private void loadAdviceReturnType(AOJAdviceDeclaration adviceDeclaration,
				TreeParent adviceNode) {
			adviceNode.addChild(new TreeParent("ReturnType." + adviceDeclaration.getReturnType().getName()));
		}
		
		private void loadAdvicePointcut(AOJAdviceDeclaration adviceDeclaration,
				TreeParent adviceNode) {
			TreeParent pointcutNode = new TreeParent("Pointcut."+adviceDeclaration.getPointcut().getCode());
			adviceNode.addChild(pointcutNode);
		}
		
		private void loadAdviceAfterThrowing(AOJAspectDeclaration aspect, TreeParent typeNode) {
			for (AOJAdviceDeclaration adviceDeclaration : aspect.getMembers().getAdvicesAfterThrowing()) {
				TreeParent adviceNode = new TreeParent(adviceDeclaration.getMetaName());
				typeNode.addChild(adviceNode);
				loadAnnotations(adviceDeclaration, typeNode, true);
				loadAdvicePointcut(adviceDeclaration, adviceNode);
				loadAdviceParams(adviceDeclaration, adviceNode);
				loadAdviceThrowException(adviceDeclaration, adviceNode);
				if (adviceDeclaration instanceof AOJBindble)
					loadBindings((AOJBindble)adviceDeclaration, adviceNode);
				loadAdviceMetrics(adviceDeclaration, adviceNode);
			}
		}
		
		private void loadAdviceAfterReturning(AOJAspectDeclaration aspect, TreeParent typeNode) {
			for (AOJAdviceDeclaration adviceDeclaration : aspect.getMembers().getAdvicesAfterReturning()) {
				TreeParent adviceNode = new TreeParent(adviceDeclaration.getMetaName());
				typeNode.addChild(adviceNode);
				loadAnnotations(adviceDeclaration, adviceNode, true);				
				loadAdvicePointcut(adviceDeclaration, adviceNode);
				loadAdviceParams(adviceDeclaration, adviceNode);
				loadAdviceThrowException(adviceDeclaration, adviceNode);
				if (adviceDeclaration instanceof AOJBindble)
					loadBindings((AOJBindble)adviceDeclaration, adviceNode);
				loadAdviceMetrics(adviceDeclaration, adviceNode);				
			}
		}

		private void loadAdviceAfter(AOJAspectDeclaration aspect, TreeParent typeNode) {
			for (AOJAdviceDeclaration adviceDeclaration : aspect.getMembers().getAdvicesAfter()) {
				TreeParent adviceNode = new TreeParent(adviceDeclaration.getMetaName());
				typeNode.addChild(adviceNode);
				loadAnnotations(adviceDeclaration, adviceNode, true);
				loadAdvicePointcut(adviceDeclaration, adviceNode);
				loadAdviceParams(adviceDeclaration, adviceNode);
				loadAdviceThrowException(adviceDeclaration, adviceNode);
				if (adviceDeclaration instanceof AOJBindble)
					loadBindings((AOJBindble)adviceDeclaration, adviceNode);
				loadAdviceMetrics(adviceDeclaration, adviceNode);
			}
		}
		
		private void loadAdviceBefore(AOJAspectDeclaration aspect,
				TreeParent typeNode) {
			for (AOJAdviceDeclaration adviceDeclaration : aspect.getMembers().getAdvicesBefore()) {
				TreeParent adviceNode = new TreeParent(adviceDeclaration.getMetaName());
				typeNode.addChild(adviceNode);

				loadAnnotations(adviceDeclaration, adviceNode, true);
				loadAdvicePointcut(adviceDeclaration, adviceNode);
				loadAdviceParams(adviceDeclaration, adviceNode);
				loadAdviceThrowException(adviceDeclaration, adviceNode);
				if (adviceDeclaration instanceof AOJBindble)
					loadBindings((AOJBindble)adviceDeclaration, adviceNode);
				loadAdviceMetrics(adviceDeclaration, adviceNode);

			}
		}
		
//		private void loadAdviceBindings(AOJAdviceDeclaration adviceDeclaration,
//				TreeParent adviceNode) {
//			TreeParent bindingNode = new TreeParent("Bindings");
//			adviceNode.addChild(bindingNode);
//			TreeParent affectsNode = new TreeParent("Affects ");
//			bindingNode.addChild(affectsNode);
//			TreeParent affectedNode = new TreeParent("Affected By");
//			bindingNode.addChild(affectedNode);
//			
//			for (AOJBinding b: adviceDeclaration.getBindingModel().getOut()) {
//				TreeParent affectsNodeData = new TreeParent("Affects (" + b.getKind() + ")" + b.getHandler());
//				affectsNode.addChild(affectsNodeData);
//			}
//			for (AOJBinding b: adviceDeclaration.getBindingModel().getIn()) {
//				TreeParent affectsNodeData = new TreeParent("Affected by (" + b.getKind() + ")" + b.getHandler());
//				affectedNode.addChild(affectsNodeData);
//			}	
//		}
		
		private void loadAdviceMetrics(AOJAdviceDeclaration adviceDeclaration,
				TreeParent adviceNode) {
			TreeParent metricNode = new TreeParent("Metrics");
			adviceNode.addChild(metricNode);
			TreeParent nstaNode = new TreeParent("NSTA " + adviceDeclaration.getMetrics().getNumberOfStatements());
			metricNode.addChild(nstaNode);
			TreeParent locNode = new TreeParent("LOC " + adviceDeclaration.getMetrics().getNumberOfLines());
			metricNode.addChild(locNode);
			TreeParent affInNode = new TreeParent("NAFFin " + adviceDeclaration.getMetrics().getNumberOfInAffects());
			metricNode.addChild(affInNode);
			TreeParent affOutNode = new TreeParent("NAFFout " + adviceDeclaration.getMetrics().getNumberOfOutAffects());
			metricNode.addChild(affOutNode);
		}
		
		private void loadAdviceThrowException(
				AOJBehaviourKind adviceDeclaration, TreeParent adviceNode) {
			if (adviceDeclaration.getThrownExceptions().size() > 0) {
		    	TreeParent exceptionsMethodNode = new TreeParent("ThrowExceptions");
	    		adviceNode.addChild(exceptionsMethodNode);
	    		List<AOJException> exceptions = adviceDeclaration.getThrownExceptions();
	    		int i = 0;
	    		for (AOJException exception : exceptions) {
	    			TreeParent exceptionMethodNode = new TreeParent("Type"+i+++"."+exception.getName());
	    			exceptionsMethodNode.addChild(exceptionMethodNode);
		    	}
			}	
		}
		
		private void loadAdviceParams(AOJBehaviourKind adviceDeclaration,
				TreeParent adviceNode) {
			if (adviceDeclaration.getParameters().size() > 0) {
 			    TreeParent paramsMethodNode = new TreeParent("Params");
				adviceNode.addChild(paramsMethodNode);
				List<AOJParameter> params = adviceDeclaration.getParameters();
				int i = 0;
				for (AOJParameter param : params) {
					TreeParent paramMethodNode = new TreeParent("Param"+i+++"."+param.getName());
					paramsMethodNode.addChild(paramMethodNode);
					TreeParent paramTypeMethodNode = new TreeParent("Type."+param.getType().getName());
					paramMethodNode.addChild(paramTypeMethodNode);
				}
			}	
		}

		private void loadPointcutDeclaration(AOJAspectDeclaration aspect,
				TreeParent typeNode) {
			for (AOJPointcutDeclaration pointcutDeclaration : aspect.getMembers().getPointcuts()) {
				TreeParent pointcutNode = new TreeParent(pointcutDeclaration.getMetaName()+"."+pointcutDeclaration.getName());
				typeNode.addChild(pointcutNode);
				loadAnnotations(pointcutDeclaration, pointcutNode, true);	
				loadPointcutModifier(pointcutDeclaration, pointcutNode);
				loadPointcutCode(pointcutDeclaration, pointcutNode);
			}
		}
		private void loadPointcutCode(
				AOJPointcutDeclaration pointcutDeclaration,
				TreeParent pointcutNode) {
			TreeParent pointcutCodeNameNode = new TreeParent("Code");
			pointcutNode.addChild(pointcutCodeNameNode);
			TreeParent pointcutCodeNode = new TreeParent(pointcutDeclaration.getPointcutExpression().getCode());
			pointcutCodeNameNode.addChild(pointcutCodeNode);
		}
		
		private void loadPointcutModifier(
				AOJPointcutDeclaration pointcutDeclaration,
				TreeParent pointcutNode) {	
			AOJModifier aojPointcutModifier = pointcutDeclaration.getModifiers();						
            if (aojPointcutModifier.getModifiers().size() > 0) {
            	TreeParent modifiersPointcutNode = new TreeParent("Modifiers");	
            	pointcutNode.addChild(modifiersPointcutNode);
            	
            	for (AOJModifierEnum modifier : aojPointcutModifier.getModifiers()) {
            		TreeParent modifierNode = new TreeParent(modifier.toString().toLowerCase());
            		modifiersPointcutNode.addChild(modifierNode);
            	}
            }		
		}
		
		private void loadDeclarePrecedences(AOJAspectDeclaration aspect, 
				TreeParent typeNode) {
			if (aspect.getMembers().getDeclarePrecedences() != null) 
				for (AOJDeclarePrecedence precedence : aspect.getMembers().getDeclarePrecedences() ) {
					if (precedence.toString().length() > 0) {
						TreeParent precedenceNode = new TreeParent("Precedences");
						typeNode.addChild(precedenceNode);
						int i = 1;
						for (String expression : precedence.getExpressions())
							precedenceNode.addChild(new TreeParent(i+++"."+expression));
					}
				}	
		}

		private void loadPrivileged(AOJAspectDeclaration aspect,
				TreeParent typeNode) {
			typeNode.addChild(new TreeParent(aspect.isPriviledge()?"Privileged.ON":"Privileged.OFF"));			
		}
	}
	
	class ViewLabelProvider extends LabelProvider {

		public String getText(Object obj) {
			return obj.toString();
		}
		public Image getImage(Object obj) {
			String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
			if (obj instanceof TreeParent)
			   imageKey = ISharedImages.IMG_OBJ_FOLDER;
			return PlatformUI.getWorkbench().getSharedImages().getImage(imageKey);
		}
	}
	class NameSorter extends ViewerSorter {
	}

	public AOPJungleView() {
	}
	
	public void createPartControl(Composite parent) {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		drillDownAdapter = new DrillDownAdapter(viewer);
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setSorter(new NameSorter());
		viewer.setInput(getViewSite());

		// Create the help context id for the viewer's control
		PlatformUI.getWorkbench().getHelpSystem().setHelp(viewer.getControl(), "AOPJungle.viewer");
		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		contributeToActionBars();
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				AOPJungleView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(actionExportXML);
		manager.add(new Separator());
		manager.add(actionExecuteAstor);
		manager.add(actionExecuteQuery);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(actionExportXML);
		manager.add(actionExecuteAstor);
		manager.add(actionExecuteQuery);
		manager.add(new Separator());
		drillDownAdapter.addNavigationActions(manager);
		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}
	
	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(actionExportXML);
		manager.add(actionExecuteAstor);
		manager.add(actionExecuteQuery);
		manager.add(new Separator());
		drillDownAdapter.addNavigationActions(manager);
	}

	private void makeActions() {
		actionExportXML = new Action() {
			public void run() {
				SaveFileFacade saveFile = new SaveFileFacade(new Shell(new Display()));
				String fileName = saveFile.open();
				if (! fileName.isEmpty()) { 
					try {
						AOJExporterXML.generate(fileName, AOJWorkspace.getInstance());
						showMessage("All projects were exported to " + fileName);	
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
		        }
			}
		};
		
		actionExportXML.setText("XML");
		actionExportXML.setToolTipText("Export to XML");
		actionExportXML.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("downloadxml.gif")));
		
		actionExecuteAstor = new Action() {
			public void run() {
				showCodeSmell();
			}
		};	

		actionExecuteAstor.setText("Astor");
		actionExecuteAstor.setToolTipText("Run Astor Tool");
//		actionExecuteAstor.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("astorlogo.gif")));
		actionExecuteAstor.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("blue_lambda_24.png")));

		actionExecuteQuery = new Action() {
			public void run() {
				long startTime = System.currentTimeMillis();
				System.out.println("*** Start persistence process *** ");
				AOJProjectDAO projectDAO = new AOJProjectDAO(AOJWorkspace.getInstance());
				projectDAO.saveAll();
				System.out.println("*** Persistence process ended in " +  (System.currentTimeMillis() - startTime) / 1000 +  " seconds ");
			}
		};
		
		actionExecuteQuery.setText("Query");
		actionExecuteQuery.setToolTipText("Run Query Tool");
		actionExecuteQuery.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("folders.gif")));

		//actionExecuteQuery.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("queryastorlogo.gif")));

		
//				PlatformUI.getWorkbench().getSharedImages().
//				getImageDescriptor(ISharedImages.IMG_LCL_LINKTO_HELP));

		doubleClickAction = new Action() {
			public void run() {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection)selection).getFirstElement();
				showMessage("Double-click detected on "+obj.toString());
			}
		};
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}
	private void showMessage(String message) {
		MessageDialog.openInformation(
			viewer.getControl().getShell(),
			"AOP Jungle View",
			message);
	}

	private void showCodeSmell() {
		AstorSession badSmellEngine = AstorSession.getInstance();
		badSmellEngine.clearEngines();
//		badSmellEngine.registerEngine(new DivergentChanges());
//		badSmellEngine.registerEngine(new LargeAspect());
//		badSmellEngine.registerEngine(new LargePointcut());
//		badSmellEngine.registerEngine(new LargeAdvice());
//		badSmellEngine.registerEngine(new AbstractMethodIntroduction());
//		badSmellEngine.registerEngine(new AnonymousPointcutDefinition());
//		badSmellEngine.registerEngine(new SpeculativeGenerality());
//		badSmellEngine.registerEngine(new AdviceCodeDuplication());
//		badSmellEngine.registerEngine(new FeatureEnvy());
//		badSmellEngine.registerEngine(new LazyAspect());
//		badSmellEngine.registerEngine(new DoublePersonality());
		badSmellEngine.registerEngine(new EncloseFunctionalInterface());
		badSmellEngine.registerEngine(new EncloseEnhancedFor());
		badSmellEngine.registerEngine(new EncloseSort());
		badSmellEngine.registerEngine(new EncloseForWithIfToFilter());
		badSmellEngine.registerEngine(new ConvertFunctionalInterfaceToDefaultFunctionalInterface());
		badSmellEngine.registerEngine(new ConvertInterfaceToDefaultMethod());
		badSmellEngine.registerEngine(new ConvertITMDtoDefaultMethod());
		
		
		badSmellEngine.run();
		badSmellEngine.printReport();
		try {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView("Astor.ResultView");
		} catch (PartInitException e) {
			AOJLogger.getLogger().error("Error on create Code Smell Result View : " + e.getMessage());
		}
	}
	
	public void setFocus() {
		viewer.getControl().setFocus();
	}
}