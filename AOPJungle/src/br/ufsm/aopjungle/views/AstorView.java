package br.ufsm.aopjungle.views;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import br.ufsm.aopjungle.astor.AstorSession;
import br.ufsm.aopjungle.astor.CodeSmellEngine;
import br.ufsm.aopjungle.astor.CodeSmellReport;
import br.ufsm.aopjungle.astor.util.AstorProperties;
import br.ufsm.aopjungle.builder.AOJExporterXML;
import br.ufsm.aopjungle.util.IconProvider;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class AstorView extends ViewPart {
	@XStreamOmitField
	public static final String ID = "br.ufsm.aopjungle.views.AstorView";
	@XStreamOmitField
	private TreeViewer viewer;
	@XStreamOmitField
	private Action actionExport;
	@XStreamOmitField
	private Action doubleClickAction;

	class TreeObject implements IAdaptable {
		@XStreamOmitField
		private String name;
		@XStreamOmitField
		private TreeParent parent;
		
		public TreeObject(String name) {
			this.name = name;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			 this.name = name;
		}
		public void setParent(TreeParent parent) {
			this.parent = parent;
		}
		public TreeParent getParent() {
			return parent;
		}
		public String toString() {
			return getName();
		}
		public Object getAdapter(Class key) {
			return null;
		}
	}
	
	class TreeParent extends TreeObject {
		@XStreamOmitField
		private ArrayList children;
		public TreeParent(String name) {
			super(name);
			children = new ArrayList();
		}
		public void addChild(TreeObject child) {
			children.add(child);
			child.setParent(this);
		}
		public TreeParent getChild(int index) {
			return (TreeParent) children.get(index);
		}
		public void setChild(int index ,TreeObject child) {
			children.set(index, child);
			//child.setParent(this);
		}
		public void removeChild(TreeObject child) {
			children.remove(child);
			child.setParent(null);
		}
		public TreeObject [] getChildren() {
			return (TreeObject [])children.toArray(new TreeObject[children.size()]);
		}
		public boolean hasChildren() {
			return children.size()>0;
		}
	}
	
    class TreeElement extends TreeParent {
		public TreeElement(String name) {
			super(name);
		}
    }
		
	class TreeCodeSmell extends TreeParent {
		public TreeCodeSmell(String name) {
			super(name);
		}
	}

	class TreeProject extends TreeParent {
		public TreeProject(String name) {
			super(name);
		}
	}
	
	class TreePackage extends TreeParent {
		public TreePackage(String name) {
			super(name);
		}
	}	

	class TreeType extends TreeParent {
		public TreeType(String name) {
			super(name);
		}
	}

	class ViewContentProvider implements IStructuredContentProvider, 
										 ITreeContentProvider {
		@XStreamOmitField
		private AstorSession session = AstorSession.getInstance();
		@XStreamOmitField
		private TreeParent invisibleRoot;

		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
		public void dispose() {
		}
		public Object[] getElements(Object parent) {
			if (parent.equals(getViewSite())) {
				if (invisibleRoot==null) initialize();
				return getChildren(invisibleRoot);
			}
			return getChildren(parent);
		}
		public Object getParent(Object child) {
			if (child instanceof TreeObject) {
				return ((TreeObject)child).getParent();
			}
			return null;
		}
		public Object [] getChildren(Object parent) {
			if (parent instanceof TreeParent) {
				return ((TreeParent)parent).getChildren();
			}
			return new Object[0];
		}
		public boolean hasChildren(Object parent) {
			if (parent instanceof TreeParent)
				return ((TreeParent)parent).hasChildren();
			return false;
		}
		
		private void initialize() {
			invisibleRoot = new TreeParent("");
			for (CodeSmellEngine engine : session.getRegisteredEngines()) {
				TreeParent codeSmellNode = new TreeCodeSmell(engine.getLabel());
				if (!engine.getReport().isEmpty())
					codeSmellNode = new TreeCodeSmell(engine.getLabel()+" -> "+engine.getReport().size());
				
				invisibleRoot.addChild(codeSmellNode);
				for (CodeSmellReport report : engine.getReport()) {
					TreeParent projectNode = (TreeProject)getNode(codeSmellNode, report.getProjectName());
					if (projectNode == null) {
						projectNode = new TreeProject(report.getProjectName());
						codeSmellNode.addChild(projectNode);
					}	
					
					TreeParent packageNode = (TreePackage)getNode(projectNode, report.getPackageName());
					if (packageNode == null) {
						packageNode = new TreePackage(report.getPackageName());
						projectNode.addChild(packageNode);
					}	
					
					TreeParent typeNameNode = (TreeType)getNode(packageNode, report.getContainerName());
					if (typeNameNode == null) {
						typeNameNode = new TreeType(report.getContainerName());
						packageNode.addChild(typeNameNode);
					}	
					for (String element : report.getElements()) {
						TreeParent elementNode = new TreeElement(element);
						typeNameNode.addChild(elementNode);
					}
//					if (!report.getElements().isEmpty()){
//						TreeParent elementNode = new TreeElement("Size: "+report.getElements().size());
//						typeNameNode.addChild(elementNode);
//					}
				}
			}			
		}
		
		private TreeParent getNode (TreeParent parent, String name) {
			TreeObject[] objects = parent.getChildren();
			int i = 0;
			while ( (i < objects.length) && (! objects[i].getName().equals(name))) {
				i++;
			}
			return i < objects.length ? (TreeParent)objects[i] : null;
		}
	}
	
	class ViewLabelProvider extends LabelProvider {
		
		public String getText(Object obj) {
			return obj.toString();
		}
		
		public Image getImage(Object obj) {
			
			if (obj instanceof TreeCodeSmell)
				if ( ((TreeCodeSmell)obj).hasChildren() ) 
					return IconProvider.getIcon("redicon.jpeg");
				else
					return IconProvider.getIcon("greenicon.jpeg");
			else	
			if (obj instanceof TreeProject)
				return IconProvider.getIcon("ajproject.gif");
			else	
			if (obj instanceof TreePackage)
				return IconProvider.getIcon("package.gif");
			else	
            if (obj instanceof TreeType)
				return IconProvider.getIcon("aspect.gif");
			else	
			if (obj instanceof TreeElement)
				return IconProvider.getIcon("report.gif");
			
			return IconProvider.getIcon("folders.gif");
		}
	}

	class NameSorter extends ViewerSorter {

	}

	public AstorView() {
	
	}
	
	public void createPartControl(Composite parent) {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setSorter(new NameSorter());
		viewer.setInput(getViewSite());

		makeActions();
		hookContextMenu();
		//hookDoubleClickAction();
		contributeToActionBars();
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				AstorView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(actionExport);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(actionExport);
	}
	
	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(actionExport);
	}

	private void makeActions() {
		actionExport = new Action() {
			public void run() {
				// TODO : Open dialog box to choose folder and filename
/*
				Shell shell = new Shell(new Display());
				shell.setSize(400, 400);
				shell.setText("Save as");
				FileDialog dialog = new FileDialog(shell, SWT.SAVE);
		        String[] filterExt = { "*.xml" };
		        dialog.setFilterExtensions(filterExt);
		        String fileName = dialog.open();
				fileName += dialog.getFileName();
*/
		        String fileName = AstorProperties.getExportFilePathName();
		        if (! fileName.isEmpty()) { 
		        	try {
		        		AOJExporterXML.generate(fileName, AstorSession.getInstance());
		        		showMessage("Code smell report was generated in " + fileName);	
		        	} catch (FileNotFoundException e) {
		        		e.printStackTrace();
		        	} catch (IOException e) {
		        		e.printStackTrace();
		        	}
		        }
			}
		};
		
		actionExport.setText("XML");
		actionExport.setToolTipText("Export to XML");
		actionExport.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
			getImageDescriptor(ISharedImages.IMG_ETOOL_SAVE_EDIT));
		
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(
			viewer.getControl().getShell(),
			//"Astor View",
			"Lambda Refactorings",
			message);
	}

	public void setFocus() {
		viewer.getControl().setFocus();
	}
}
