package br.ufsm.aopjungle.metrics;

import javax.persistence.Entity;

@Entity
public class AOJAspectMetrics extends AOJTypeMetrics {
	private long numberOfPointcuts;
	private long numberOfAdvices;
	private long numberOfInterMethod;
	private long numberOfInterField;
	private long numberOfDeclMessage;
	private long numberOfDeclSoft;
	private long numberOfDeclExtends;
	private long numberOfDeclImplements;
	private long numberOfCrossCuttingMembers;
	private long numberOfInterTypeDeclarations;
	
	public AOJAspectMetrics() {
	
	}
	
	public long getNumberOfPointcuts() {
		return numberOfPointcuts;
	}
	public void setNumberOfPointcuts(long numberOfPointcuts) {
		this.numberOfPointcuts = numberOfPointcuts;
	}
	public long getNumberOfInterMethod() {
		return numberOfInterMethod;
	}
	public void setNumberOfInterMethod(long numberOfInterMethod) {
		this.numberOfInterMethod = numberOfInterMethod;
	}
	public long getNumberOfAdvices() {
		return numberOfAdvices;
	}
	public void setNumberOfAdvices(long numberOfAdvices) {
		this.numberOfAdvices = numberOfAdvices;
	}
	public long getNumberOfInterField() {
		return numberOfInterField;
	}
	public void setNumberOfInterField(long numberOfInterField) {
		this.numberOfInterField = numberOfInterField;
	}
	public long getNumberOfDeclMessage() {
		return numberOfDeclMessage;
	}
	public void setNumberOfDeclMessage(long numberOfDeclMessage) {
		this.numberOfDeclMessage = numberOfDeclMessage;
	}
	public long getNumberOfDeclSoft() {
		return numberOfDeclSoft;
	}
	public void setNumberOfDeclSoft(long numberOfDeclSoft) {
		this.numberOfDeclSoft = numberOfDeclSoft;
	}
	public long getNumberOfDeclExtends() {
		return numberOfDeclExtends;
	}
	public void setNumberOfDeclExtends(long numberOfDeclExtends) {
		this.numberOfDeclExtends = numberOfDeclExtends;
	}
	public long getNumberOfDeclImplements() {
		return numberOfDeclImplements;
	}
	public void setNumberOfDeclImplements(long numberOfDeclImplements) {
		this.numberOfDeclImplements = numberOfDeclImplements;
	}
	public long getNumberOfCrossCuttingMembers() {
		return numberOfCrossCuttingMembers;
	}
	public void setNumberOfCrossCuttingMembers(long numberOfCrossCuttingMembers) {
		this.numberOfCrossCuttingMembers = numberOfCrossCuttingMembers;
	}
	public long getNumberOfInterTypeDeclarations() {
		return numberOfInterTypeDeclarations;
	}
	public void setNumberOfInterTypeDeclarations(
			long numberOfInterTypeDeclarations) {
		this.numberOfInterTypeDeclarations = numberOfInterTypeDeclarations;
	}
	
}
