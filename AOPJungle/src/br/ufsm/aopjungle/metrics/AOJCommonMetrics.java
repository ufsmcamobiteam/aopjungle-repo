package br.ufsm.aopjungle.metrics;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
public class AOJCommonMetrics extends AOJAbstractMetrics {
	private long numberOfLines;
	private long numberOfInAffects; 
	private long numberOfOutAffects; 
	
	public AOJCommonMetrics() {
	
	}
	
	public long getNumberOfInAffects() {
		return numberOfInAffects;
	}

	public void setNumberOfInAffects(long numberOfInAffects) {
		this.numberOfInAffects = numberOfInAffects;
	}

	public long getNumberOfOutAffects() {
		return numberOfOutAffects;
	}

	public void setNumberOfOutAffects(long numberOfOutAffects) {
		this.numberOfOutAffects = numberOfOutAffects;
	}	
	
	public long getNumberOfLines() {
		return numberOfLines;
	}
	
	public void setNumberOfLines(long numberOfLines) {
		this.numberOfLines = numberOfLines;
	}
}
