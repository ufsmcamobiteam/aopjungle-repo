package br.ufsm.aopjungle.metrics;

import java.util.Map;

import org.aspectj.asm.IProgramElement;
import org.aspectj.org.eclipse.jdt.core.dom.AdviceDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.AnonymousClassDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.DeclareDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.EnumDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.FieldDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.MethodDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.PointcutDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.TypeDeclaration;

import br.ufsm.aopjungle.aspectj.AOJBuildListener;
import br.ufsm.aopjungle.aspectj.AOJungleVisitor;
import br.ufsm.aopjungle.binding.AOJBinding;
import br.ufsm.aopjungle.binding.AOJReferenceManager;
import br.ufsm.aopjungle.exception.StackObjectReferenceIllegalStateException;
import br.ufsm.aopjungle.log.AOJLogger;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJMember;
import br.ufsm.aopjungle.metamodel.commons.AOJPackageDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration;
 
public aspect AOJMetricsProcessor {
	/* Type pointcuts */
	pointcut typeEndVisit(AOJungleVisitor visitor) : execution (* AOJungleVisitor.endVisit(TypeDeclaration+)) && this(visitor);  
	pointcut enumEndVisit(AOJungleVisitor visitor) : execution (* AOJungleVisitor.endVisit(EnumDeclaration+)) && this(visitor);  
	pointcut anonymousTypeEndVisit(AOJungleVisitor visitor) : execution (* AOJungleVisitor.endVisit(AnonymousClassDeclaration)) && this(visitor);  
	
	/* Method-like pointcuts */
	pointcut methodEndVisit(AOJungleVisitor visitor) : execution (* AOJungleVisitor.endVisit(MethodDeclaration)) && this(visitor);  
	pointcut adviceEndVisit(AOJungleVisitor visitor) : execution (* AOJungleVisitor.endVisit(AdviceDeclaration+)) && this(visitor);  

	/* Pointcut pointcuts */
	pointcut pointcutEndVisit(AOJungleVisitor visitor) : execution (* AOJungleVisitor.endVisit(PointcutDeclaration)) && this(visitor);  
	
	/* Inter-type pointcuts */
	pointcut declareDeclarationEndVisit(AOJungleVisitor visitor) : execution (* AOJungleVisitor.endVisit(DeclareDeclaration+)) && this(visitor);  

	/* Field pointcuts */
	pointcut fieldEndVisit(AOJungleVisitor visitor) : execution (* AOJungleVisitor.endVisit(FieldDeclaration)) && this(visitor);  

	/* Declare statments pointcuts */
	pointcut declareFieldEndVisit(AOJMember source, AOJBinding sourceBinding,
			IProgramElement sourceElement, AOJMember target, AOJBinding targetBinding,
			IProgramElement targetElement) : 
		execution (* AOJBuildListener.loadInOutRelationShip 
				(AOJMember, AOJBinding, IProgramElement,
			     AOJMember, AOJBinding, IProgramElement))
			     && args(source, sourceBinding, sourceElement,target, targetBinding, targetElement);  
//	pointcut declareMethodEndVisit(AOJungleVisitor visitor) : execution (* AOJungleVisitor.endVisit(InterTypeMethodDeclaration)) && this(visitor);  
//	pointcut declareParentsEndVisit(AOJungleVisitor visitor) : execution (* AOJungleVisitor.endVisit(DeclareParentsDeclaration)) && this(visitor);  
//	pointcut declareSoftEndVisit(AOJungleVisitor visitor) : execution (* AOJungleVisitor.endVisit(DeclareSoftDeclaration)) && this(visitor);  
	
	/* Project and Package pointcuts */
	pointcut packageConstructor(AOJPackageDeclaration pack) : execution (public AOJPackageDeclaration.new(..)) && this(pack);
	pointcut projectConstructor(AOJProject project) : execution (public AOJProject.new(..)) && this(project);

	/* Update Late Metrics */
	pointcut loadMetrics(AOJMetrics metrics) : execution (* AOJMetrics.loadMetrics()) && this(metrics);
	 
	/* Advices to calculate metrics */
	before (AOJungleVisitor visitor) : methodEndVisit(visitor) 
	                                || adviceEndVisit(visitor) 
	                                || typeEndVisit(visitor) 
	                                || enumEndVisit(visitor)
	                                || anonymousTypeEndVisit(visitor)
	                                || pointcutEndVisit(visitor)
	                                || declareDeclarationEndVisit(visitor)
	                                || fieldEndVisit(visitor) {
		if (visitor.getLastMemberFromStack() instanceof AOJMetrics)
			( (AOJMetrics)visitor.getLastMemberFromStack() ).loadMetrics();
	}
	
	after (AOJMember source, AOJBinding sourceBinding,
			IProgramElement sourceElement, AOJMember target, AOJBinding targetBinding,
			IProgramElement targetElement) : 
				declareFieldEndVisit(source, sourceBinding, sourceElement,target, 
						targetBinding, targetElement) {
			if (source instanceof AOJMetrics)
				( (AOJMetrics)source ).loadMetrics();
	}
	
	after (AOJMetrics metrics) : loadMetrics(metrics) {
		loadMetricsUpper (metrics);
	}
	
	private void loadMetricsUpper (AOJMetrics metrics) {
		if (metrics.getOwner() instanceof AOJMetrics) {
			((AOJMetrics)(metrics.getOwner())).loadMetrics();
//			loadMetricsUpper((AOJMetrics)(metrics.getOwner()));
		}	
	}
	
	after (AOJPackageDeclaration pack) : packageConstructor(pack) {
		pack.loadMetrics();
	}

	after (AOJProject project) : projectConstructor(project) {
		for (Map.Entry<String, AOJMember> map : AOJReferenceManager.getInstance().getReferenceMap().entrySet()) {
			if (isInheritanceCandidate(map.getValue())) {
				AOJTypeDeclaration type = (AOJTypeDeclaration)map.getValue();
				resolveAscendentMetrics(type);
				resolveDescendentMetrics(type);
				type.loadMetrics();
			}
		}
		project.loadMetrics();
	}
	
	/**
	 * The strategy to count Ascendent is pretty ugly because AJDT does not help
	 * so much in such task.
	 * First we try to find the superTypes in the local referenceType map. We traverse
	 * all superTypes until we cannot find at the referenceType map. 
	 * Second phase try to find the type by its ClassLoader
	 * We do not use ClassLoader for internal types because a type
	 * might not be loaded yet at this time leading to wrong ascendent counting. 
	 * 
	 * @param type The type which ascendent should be counted
	 */
	private void resolveAscendentMetrics(final AOJTypeDeclaration type) {
		int _numberOfAscendents = 0;
		// We first try to find Ascendent by Reference table
		AOJContainer _type = type.getSuperType(); 
		AOJContainer _currentType = type; 
		try {
			while (null != _type)  {
				_type = AOJReferenceManager.getInstance().getType(_type.getInternalHandler());
				if (null != _type) {
					_numberOfAscendents++;
					_currentType = _type;
					_type = _type.getSuperType();
				}	
			} 
		} catch (StackObjectReferenceIllegalStateException e) {}

		// Then we try to find Ascendent by ClassLoader
		_numberOfAscendents += findByClassLoader (_currentType.getSuperType());
		type.getMetrics().setNumberOfAscendents(_numberOfAscendents); 
	}
	
	private int findByClassLoader(final AOJContainer type) {
		int result = 0;
		if (null == type) 
			return result;
		if (isTop(type))
			return ++result;
		
		Class<?> c;
		try {
			c = Class.forName(type.getFullQualifiedName());
			do {
				result++;
				c = c.getSuperclass();
			} while (null != c);

		} catch (ClassNotFoundException e) {
			AOJLogger.getLogger().error(e);
		}
		return result;
	}
	
	private boolean isTop(final AOJContainer type) {
		return type.getInternalHandler() == "java.lang.Object";
	}

	private boolean isInheritanceCandidate(AOJMember value) {
		return (value instanceof AOJClassDeclaration) 
			|| (value instanceof AOJAspectDeclaration)
			|| (value instanceof AOJInterfaceDeclaration);
	}
	
	private void resolveDescendentMetrics(final AOJContainer type) {
		long _numberOfDescendents = 0;
		AOJContainer lSuperType = type.getSuperType();
		while (null != lSuperType) {
			try {
				lSuperType = AOJReferenceManager.getInstance().getType(lSuperType.getInternalHandler());
				if (null != lSuperType) {
					_numberOfDescendents = lSuperType.getMetrics().getNumberOfDescendents() + 1;
					lSuperType.getMetrics().setNumberOfDescendents(_numberOfDescendents);
					lSuperType = lSuperType.getSuperType();
				}	
			} catch (StackObjectReferenceIllegalStateException e) {
				AOJLogger.getLogger().error(e);
			}
		}
	}
}
