package br.ufsm.aopjungle.metrics;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
public class AOJBehaviorMetrics extends AOJCommonMetrics {
	private long numberOfStatements;
	private long fanIn; // every incoming call
	private long fanOut; // every call to another module

	public AOJBehaviorMetrics() {
	
	}
	
	public long getNumberOfStatements() {
		return numberOfStatements;
	}
	
	public void setNumberOfStatements(long numberOfStatements) {
		this.numberOfStatements = numberOfStatements;
	}

	public long getFanIn() {
		return fanIn;
	}

	public void setFanIn(long fanIn) {
		this.fanIn = fanIn;
	}

	public long getFanOut() {
		return fanOut;
	}

	public void setFanOut(long fanOut) {
		this.fanOut = fanOut;
	}
}
