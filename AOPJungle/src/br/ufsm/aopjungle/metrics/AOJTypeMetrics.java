package br.ufsm.aopjungle.metrics;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
public class AOJTypeMetrics extends AOJCommonMetrics {
	private long numberOfDescendents;
	private long numberOfAscendents;	
	private long numberOfMethods;
	private long numberOfFields;
	private long numberOfConstructors;
	
	public AOJTypeMetrics() {
	}
	
	public long getNumberOfDescendents() {
		return numberOfDescendents;
	}

	public void setNumberOfDescendents(long numberOfChildren) {
		this.numberOfDescendents = numberOfChildren;
	}

	public long getNumberOfAscendents() {
		return numberOfAscendents;
	}

	public void setNumberOfAscendents(long numberOfParents) {
		this.numberOfAscendents = numberOfParents;
	}

	public long getNumberOfConstructors() {
		return numberOfConstructors;
	}

	public void setNumberOfConstructors(long numberOfConstructors) {
		this.numberOfConstructors = numberOfConstructors;
	}

	public long getNumberOfFields() {
		return numberOfFields;
	}

	public void setNumberOfFields(long numberOfFields) {
		this.numberOfFields = numberOfFields;
	}

	public long getNumberOfMethods() {
		return numberOfMethods;
	}

	public void setNumberOfMethods(long numberOfMethods) {
		this.numberOfMethods = numberOfMethods;
	}
}
