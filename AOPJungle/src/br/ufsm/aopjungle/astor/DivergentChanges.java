package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.List;

import br.ufsm.aopjungle.astor.util.AstorProperties;
import br.ufsm.aopjungle.astor.util.NGram;
import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;

public class DivergentChanges extends CodeSmellImpl {
	private NGram simFunction = new NGram();
	
	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) 
			for (AOJAspectDeclaration aspect : project.getAspects())
				findDivergentChanges(aspect);
	}

	private void findDivergentChanges(AOJAspectDeclaration aspect) {
		List<AOJPointcutDeclaration> pointcut = aspect.getMembers().getPointcuts();
		for (int i = 0; i <= aspect.getMembers().getPointcuts().size() - 2; i++) 
			for (int j = i+1; j <= aspect.getMembers().getPointcuts().size() - 1; j++) 
				analyzeExpressions(aspect, pointcut.get(i), pointcut.get(j));
	}
	
	private void analyzeExpressions (AOJTypeDeclaration aspect, 
			AOJPointcutDeclaration pc1, AOJPointcutDeclaration pc2) {
		
		for (int i = 0; i <= pc1.getPointcutExpression().getPrimitives().size() - 1;i++) 
			for (int j = 0; j <= pc2.getPointcutExpression().getPrimitives().size() - 1;j++) {
				String str1 = pc1.getPointcutExpression().getPrimitives().get(i).getCode();
				String str2 = pc2.getPointcutExpression().getPrimitives().get(j).getCode(); 
				double similarity = similarity(str1, str2);
				if (similarity > AstorProperties.getCodeSimilarityDivergentChange()/100) {
					String message = String.format("Pointcut expressions %s and %s have %f of similarity", str1, str2, similarity);
					List<String> list = new ArrayList<String>();
					list.add(message);
					registerReport(list, aspect);
				}
			}
	}
		
	/**
	 * @param arg0 String 1 to compare
	 * @param arg1 String 2 to compare
	 * @return similarity index of two strings (0 a 1), 0 completely different, 1 equals
	 */
	private double similarity(String arg0, String arg1) {
		return simFunction.getSimilarity(arg0, arg1, arg1.length());
	}

	@Override
	public String getLabel() {
		return "Divergent Changes";
	}
}