package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

public final class AstorSession {
	@XStreamOmitField
	private static AstorSession instance = new AstorSession();
	private List<CodeSmellEngine> codeSmellList = new ArrayList<CodeSmellEngine>();
	
	private AstorSession() {
	}
	
	public void printReport() {
		for (CodeSmellEngine engine : codeSmellList) {
			System.out.println("\nPrinting Report for " + engine.getClass().getName());
			for (CodeSmellReport report : engine.getReport())
				print(report);
		}
	}
	
	private void print(CodeSmellReport report) {
		System.out.println(String.format("%s, %s, %s, %s, %s, ", 
				report.getProjectName(), 
				report.getPackageName(),
				report.getCompilationUnitName(), 
				report.getContainerName(),
				report.getContainerType()));
		for (String message : report.getElements()) {
			System.out.println(String.format("|--- %s ", message));
		}	
	}

	public void run() {
		for (CodeSmellEngine engine : codeSmellList) 
			engine.run();
	}
	
	public List<CodeSmellEngine> getRegisteredEngines() {
		return codeSmellList;
	}
	
	public void clearEngines() {
		if (codeSmellList != null) 
			codeSmellList.clear();
	}
	
	public void registerEngine(CodeSmellEngine engine) {
		if (codeSmellList == null)
			codeSmellList = new ArrayList<CodeSmellEngine>();
		
		if (! containsEngine(engine))
			codeSmellList.add(engine);
	}

	private boolean containsEngine(CodeSmellEngine engine) {
		int i = -1;
		do {
			i++;
		} while ((i < codeSmellList.size()) && (!codeSmellList.get(i).getClass().equals(engine.getClass())));
		
		return i != codeSmellList.size();
	}

	public static AstorSession getInstance() {
		return instance;
	}
}	
