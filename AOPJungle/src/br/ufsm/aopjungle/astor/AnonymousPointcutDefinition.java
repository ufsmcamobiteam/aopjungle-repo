package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.List;

import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAdviceDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;

public class AnonymousPointcutDefinition extends CodeSmellImpl {
	@Override
	public String getLabel() {
		return "Anonymous Pointcut Definition";
	}
	
	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) 
			for (AOJAspectDeclaration aspect : project.getAspects())
				findAnonymousPointcutDefinition(aspect);
	}

	private void findAnonymousPointcutDefinition(AOJAspectDeclaration aspect) {
		for (AOJAdviceDeclaration advice : aspect.getMembers().getAllAdvices()) {
			if (advice.getPointcut().isAnonymous()) {
				String message = String.format("Anonymous pointcut declaration was found on advice : %s, pointcut : %s", advice.getKind(), advice.getPointcut().getCode());
				List<String> list = new ArrayList<String>();
				list.add(message);
				registerReport(list, aspect);
			}
		}
	}
}
