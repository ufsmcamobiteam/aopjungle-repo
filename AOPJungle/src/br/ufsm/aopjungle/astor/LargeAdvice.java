package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.List;

import br.ufsm.aopjungle.astor.util.AstorProperties;
import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAdviceDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;

public class LargeAdvice extends CodeSmellImpl {
	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) 
			for (AOJAspectDeclaration aspect : project.getAspects())
				largeAdvices(aspect);
	}

	private void largeAdvices(AOJAspectDeclaration aspect) {
		for (AOJAdviceDeclaration advice : aspect.getMembers().getAllAdvices()) {
			if (advice.getMetrics().getNumberOfLines() > AstorProperties.getMethodLikeMax()) {
				String message = String.format("Number of lines in advice %s is large (%d)", advice.getKind(), advice.getMetrics().getNumberOfLines());
				List<String> list = new ArrayList<String>();
				list.add(message);
				registerReport(list, aspect);
			}	
		}
	}

	@Override
	public String getLabel() {
		return "Large Advice";
	}	
}
