package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.List;

import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.commons.AOJContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJMethodDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.commons.AOJStatement;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJConstructorDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJEnhancedForStatement;

public class EncloseEnhancedFor extends CodeSmellImpl {
	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) {
			for (AOJTypeDeclaration clazz : project.getClasses())
				findRefactorings(clazz);
		}
	}

	private void findRefactorings(AOJTypeDeclaration clazz) {

		// Constructors
		for (AOJConstructorDeclaration constructor : ((AOJClassDeclaration)clazz).getMembers().getConstructors() )  {
			for (AOJContainer container : constructor.getAnonymousClasses()){
				for(AOJMethodDeclaration methodAnonymous : container.getMembers().getMethods()) {
					for(AOJStatement statement : methodAnonymous.getStatements()) 
						print(statement, clazz);
				}
			}
			
			for(AOJStatement statement : constructor.getStatements())
				print(statement, clazz);
		}

		// methods
		for (AOJMethodDeclaration method : clazz.getMembers().getMethods() )  {
			for (AOJContainer container : method.getAnonymousClasses()){
				for(AOJMethodDeclaration methodAnonymous : container.getMembers().getMethods()) {
					for(AOJStatement statement : methodAnonymous.getStatements()) 
						print(statement, clazz);
				}
			}
			
			for(AOJStatement statement : method.getStatements()) 
				print(statement, clazz);
		}
	}

	private void print(AOJStatement statement, 	AOJTypeDeclaration clazz) {
		if (statement instanceof AOJEnhancedForStatement) {
			String message = String.format("Enhanced For - %s",	((AOJEnhancedForStatement) statement).getCode());
			List<String> list = new ArrayList<String>();
			list.add(message);
			registerReport(list, clazz);
		}
	}

	@Override
	public String getLabel() {
		return "Convert Enhanced For to Lambda Enhanced For";
//		return "Enclose Enhanced For";
	}
}
