package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.List;

import br.ufsm.aopjungle.astor.util.AstorProperties;
import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;

public class LargePointcut extends CodeSmellImpl {
	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) 
			for (AOJAspectDeclaration aspect : project.getAspects())
				findLargeAspect(aspect);
	}

	private void findLargeAspect(AOJAspectDeclaration aspect) {
		List<AOJPointcutDeclaration> pointcuts = aspect.getMembers().getPointcuts();
		for (AOJPointcutDeclaration pointcut : pointcuts) {
			if (pointcut.getPointcutExpression().getCode().length() > AstorProperties.getLargePointcutDefinitionMax()) {
				String message = String.format("Large pointcut %s. More than %d characteres", pointcut.getName(), pointcut.getPointcutExpression().getCode().length());
				List<String> list = new ArrayList<String>();
				list.add(message);
				registerReport(list, aspect);
			}
		}	
	}

	@Override
	public String getLabel() {
		return "Large Pointcut";
	}	
}