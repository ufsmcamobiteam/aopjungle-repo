package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import br.ufsm.aopjungle.metamodel.commons.AOJContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJPackageDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

public abstract class CodeSmellImpl implements CodeSmellEngine {
	@XStreamOmitField
	private Properties properties;
	private List<CodeSmellReport> report = new ArrayList<CodeSmellReport>();

	public void run(Properties properties) {
		this.setProperties(properties);
	}
	
	public AOJProject getOwner(AOJPackageDeclaration pack) {
		return (AOJProject)pack.getOwner();
	}

	public CodeSmellImpl() {
		super();
		report.clear();
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public List<CodeSmellReport> getReport() {
		return report;
	}

	public void setReport(List<CodeSmellReport> report) {
		this.report = report;
	}
	
	public String getLabel() {
		return "CodeSmell";
	}
	
	public void registerReport(List<String> messages, AOJContainer type) {
		getReport().add(new CodeSmellReport(getProject(type).getName(), 
			       getPack(type).getName(), getCunit(type).getName(),
			       type.getName(), type.getMetaName(), messages));
	}

	private AOJProgramElement getCunit(AOJContainer type) {
		return type.getOwner();
	}

	private AOJProgramElement getPack(AOJContainer type) {
		return getCunit(type).getOwner();
	}

	private AOJProgramElement getProject(AOJContainer type) {
		return getPack(type).getOwner();
	}
}