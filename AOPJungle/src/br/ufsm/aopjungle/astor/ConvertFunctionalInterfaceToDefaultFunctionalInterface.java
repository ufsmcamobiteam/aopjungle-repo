package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.commons.AOJMethodDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJParameter;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration;

public class ConvertFunctionalInterfaceToDefaultFunctionalInterface extends CodeSmellImpl {
	
	private Collection<FunctionalInterfaceDefault> list = null;
	
	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()){
			for (AOJInterfaceDeclaration _interface : project.getInterface())
				findRefactorings(_interface);
		}
	}

	private void findRefactorings(AOJInterfaceDeclaration _interface) {
		if (_interface.isFunctionalInterface()){
			List<String> listInterfaces = getSimilarDefaultFunctionalInterface(_interface);
			if (listInterfaces.size() > 0) {
				System.out.println(_interface.getName()+" - "+ _interface.getSuperType().getFullQualifiedName());
				String message = String.format( "Convert Functional Interface to: "+ listInterfaces.toString());
				List<String> list = new ArrayList<String>();
				list.add(message);
				registerReport(list, _interface);
			}
		}
		
	}

	@Override
	public String getLabel() {
		return "Convert Functional Interface To Default Functional Interface";
	}
	
	private Boolean equalsParametersType(FunctionalInterfaceDefault def, AOJMethodDeclaration method){
		if (def.getParametersType().length != method.getParameters().size())
			return Boolean.FALSE;
		
		if (def.getParametersType().length == 0 && method.getParameters().size() == 0)
			return Boolean.TRUE;
		
		Collection<String> listParameters = new ArrayList<>();
		for (AOJParameter methodParameter : method.getParameters())
			listParameters.add(methodParameter.getType().getName());
			
		for (String defParameter : def.getParametersType())
			for (String parameter : listParameters)
				if (!isGenericType(defParameter)) 
					if (defParameter.toLowerCase().equals(parameter.toLowerCase()) ){
						listParameters.remove(parameter);
						break;
					}
				
		for (String defParameter : def.getParametersType())
			for (String parameter : listParameters)
				if (isGenericType(defParameter)){
					listParameters.remove(parameter);
					break;
				}
					
			
		if (listParameters.size() == 0)
			return Boolean.TRUE;
		return Boolean.FALSE;
	}
	
	private List<String> getSimilarDefaultFunctionalInterface(AOJInterfaceDeclaration _interface){

		List<String> list = new ArrayList<String>();
		
		System.out.println(_interface.getFullQualifiedName()+" owner = "+ _interface.getOwner().getName());
		
		//the method is in the inheritance interface (AOPJungle not store interface inheritance) 
		if (_interface.getMembers().getMethods().isEmpty())
			return list; 
			
			
		AOJMethodDeclaration method = _interface.getMembers().getMethods().get(0);
		
		for (FunctionalInterfaceDefault def : getFunctionalInterfacesDefault()){
			Boolean aux = Boolean.FALSE;
			// if equals genericType or equals Type 
			if ((isGenericType(def.returnType).equals(Boolean.TRUE) && isGenericType(method.getReturnType().getName()).equals(Boolean.TRUE))) 
				aux = Boolean.TRUE;
			else if (def.returnType.toLowerCase().equals(method.getReturnType().getName().toLowerCase()))
				aux = Boolean.TRUE;
			else if ((isGenericType(def.returnType) && !method.getReturnType().getName().toLowerCase().equals("void")))
				aux = Boolean.TRUE;
			
			if (aux && equalsParametersType(def, method))
				list.add(def.getName());
		}
		
		return list;
	}
	

	public Collection<FunctionalInterfaceDefault> getFunctionalInterfacesDefault() {
		if (list == null) {
			list = new ArrayList<FunctionalInterfaceDefault>();
			
			list.add(new FunctionalInterfaceDefault("BiConsumer", "void", new String[]{"T","U"}));
			list.add(new FunctionalInterfaceDefault("BiFunction", "R", new String[]{"T", "U"}));
			list.add(new FunctionalInterfaceDefault("BinaryOperator", "R", new String[]{"T", "U"}));
			list.add(new FunctionalInterfaceDefault("BiPredicate", "boolean", new String[]{"T", "U"}));
			list.add(new FunctionalInterfaceDefault("BooleanSupplier", "boolean", new String[]{}));
			list.add(new FunctionalInterfaceDefault("Consumer", "void", new String[]{"T"}));
			list.add(new FunctionalInterfaceDefault("DoubleBinaryOperator", "double", new String[]{"double", "double"}));
			list.add(new FunctionalInterfaceDefault("DoubleConsumer", "void", new String[]{"double"}));
			list.add(new FunctionalInterfaceDefault("DoubleFunction", "R", new String[]{"double"}));
			list.add(new FunctionalInterfaceDefault("DoublePredicate", "boolean", new String[]{"double"}));
			list.add(new FunctionalInterfaceDefault("DoubleSupplier", "double", new String[]{}));
			list.add(new FunctionalInterfaceDefault("DoubleToIntFunction", "int", new String[]{"double"}));
			list.add(new FunctionalInterfaceDefault("DoubleToLongFunction", "long", new String[]{"double"}));
			list.add(new FunctionalInterfaceDefault("DoubleUnaryOperator", "double", new String[]{"double"}));
			list.add(new FunctionalInterfaceDefault("Function", "R", new String[]{"T"}));
			list.add(new FunctionalInterfaceDefault("IntBinaryOperator", "int", new String[]{"int", "int"}));
			list.add(new FunctionalInterfaceDefault("IntConsumer", "void", new String[]{"int"}));
			list.add(new FunctionalInterfaceDefault("IntFunction", "R", new String[]{"int"}));
			list.add(new FunctionalInterfaceDefault("IntPredicate", "boolean", new String[]{"int"}));
			list.add(new FunctionalInterfaceDefault("IntSupplier", "int", new String[]{}));
			list.add(new FunctionalInterfaceDefault("IntToDoubleFunction", "double", new String[]{"int"}));
			list.add(new FunctionalInterfaceDefault("IntToLongFunction", "long", new String[]{"int"}));
			list.add(new FunctionalInterfaceDefault("IntUnaryOperator", "int", new String[]{"int"}));
			list.add(new FunctionalInterfaceDefault("LongBinaryOperator", "long", new String[]{"long", "long"}));
			list.add(new FunctionalInterfaceDefault("LongConsumer", "void", new String[]{"long"}));
			list.add(new FunctionalInterfaceDefault("LongFunction", "R", new String[]{"long"}));
			list.add(new FunctionalInterfaceDefault("LongPredicate", "boolean", new String[]{"long"}));
			list.add(new FunctionalInterfaceDefault("LongSupplier", "long", new String[]{}));
			list.add(new FunctionalInterfaceDefault("LongToDoubleFunction", "double", new String[]{"long"}));
			list.add(new FunctionalInterfaceDefault("LongToIntFunction", "int", new String[]{"long"}));
			list.add(new FunctionalInterfaceDefault("LongUnaryOperator", "long", new String[]{"long"}));
			list.add(new FunctionalInterfaceDefault("ObjDoubleConsumer", "void", new String[]{"T", "double"}));
			list.add(new FunctionalInterfaceDefault("ObjIntConsumer", "void", new String[]{"T", "int"}));
			list.add(new FunctionalInterfaceDefault("ObjLongConsumer", "void", new String[]{"T", "long"}));
			list.add(new FunctionalInterfaceDefault("Predicate", "boolean", new String[]{"T"}));
			list.add(new FunctionalInterfaceDefault("Supplier", "T", new String[]{}));
			list.add(new FunctionalInterfaceDefault("ToDoubleBiFunction", "double", new String[]{"T", "U"}));
			list.add(new FunctionalInterfaceDefault("ToDoubleFunction", "double", new String[]{"T"}));
			list.add(new FunctionalInterfaceDefault("ToIntBiFunction", "int", new String[]{"T", "U"}));
			list.add(new FunctionalInterfaceDefault("ToIntFunction", "int", new String[]{"T"}));
			list.add(new FunctionalInterfaceDefault("ToLongBiFunction", "long", new String[]{"T", "U"}));
			list.add(new FunctionalInterfaceDefault("ToLongFunction", "long", new String[]{"T"}));
			list.add(new FunctionalInterfaceDefault("UnaryOperator", "R", new String[]{"T"}));

		}
		return list;
	}

	private Boolean isGenericType(String arg){
		switch (arg.toUpperCase()){
		case "T": return true;
		case "R": return true;
		case "E": return true;
		case "K": return true;
		case "N": return true;
		case "V": return true;
		case "S": return true;
		case "U": return true;
		default : return false;
		}
		 
	}
	
	class FunctionalInterfaceDefault {
		private String name;
		private String returnType;
		private String[] parametersType;
		public FunctionalInterfaceDefault(String name, String returnType, String[] parametersType){
			this.name = name;
			this.returnType = returnType;
			this.parametersType = parametersType;
		}
		
		public String getReturnType() {
			return returnType;
		}
		public void setReturnType(String returnType) {
			this.returnType = returnType;
		}
		public String[] getParametersType() {
			return parametersType;
		}
		public void setParametersType(String[] parametersType) {
			this.parametersType = parametersType;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}

}

