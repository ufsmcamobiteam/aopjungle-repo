package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.List;

import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareMethod;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;

public class ConvertITMDtoDefaultMethod extends CodeSmellImpl {
	@Override
	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) 
			for (AOJAspectDeclaration aspect : project.getAspects())
				findRefactorings(aspect);
		
	}

	private void findRefactorings(AOJAspectDeclaration aspect) {
		for (AOJDeclareMethod interTypeMethod : aspect.getMembers().getDeclareMethods()) {
			int size = interTypeMethod.getStatementsBody();
			if (size == 0 || (size == 1 && !interTypeMethod.getReturnType().getName().equals("void"))){
				String message = String.format("Inter-type method declaration: %s.%s", interTypeMethod.getOnType(), interTypeMethod.getName());
				List<String> list = new ArrayList<String>();
				list.add(message);
				registerReport(list, aspect);
			}
		}
	}

	@Override
	public String getLabel() {
		return "Convert Inter-Type Method Declaration to Default Interface Method";
	}

}
