package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.List;

import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareMethod;
import br.ufsm.aopjungle.metamodel.commons.AOJModifierEnum;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;

public class AbstractMethodIntroduction extends CodeSmellImpl {
	@Override
	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) 
			for (AOJAspectDeclaration aspect : project.getAspects())
				abstractMethodIntroduction(aspect);
		
	}

	private void abstractMethodIntroduction(AOJAspectDeclaration aspect) {
		for (AOJDeclareMethod interTypeMethod : aspect.getMembers().getDeclareMethods()) {
			if (interTypeMethod.getModifiers().exists(AOJModifierEnum.ABSTRACT)) {
				String message = String.format("Inter-type declaration of abstract method was found in aspect: %s, method : %s", aspect.getName(), interTypeMethod.getName());
				List<String> list = new ArrayList<String>();
				list.add(message);
				registerReport(list, aspect);
			}
		}
	}

	@Override
	public String getLabel() {
		return "Abstract Method Introduction";
	}

}
