package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.List;

import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;

public class FeatureEnvy extends CodeSmellImpl {

	@Override
	public String getLabel() {
		return "Feature Envy";
	}
	
	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) 
			for (AOJClassDeclaration clazz : project.getClasses())
				findFeatureEnvy(clazz);
	}

	// TODO: Check whether there are only one aspect using the pc to trigger the
	// badsmell 
	private void findFeatureEnvy(AOJClassDeclaration clazz) {
		if (clazz.getMembers().getPointcuts().size() > 0) {
			String message = String.format("Pointcut declaration was found on class : %s", clazz.getFullQualifiedName());
			List<String> list = new ArrayList<String>();
			list.add(message);
			registerReport(list, clazz);
		}
	}	
}
