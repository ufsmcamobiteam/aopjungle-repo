package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAdviceDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class AdviceCodeDuplication extends CodeSmellImpl {
	@XStreamOmitField
	private Hashtable<String, AOJAdviceDeclaration> adviceBody = new Hashtable<String, AOJAdviceDeclaration>();
	
	@Override
	public String getLabel() {
		return "Advice Code Duplication";
	}
	
	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) 
			for (AOJAspectDeclaration aspect : project.getAspects())
				findDuplicatedCode(aspect);
	}

	private void findDuplicatedCode(AOJAspectDeclaration aspect) {
		for (AOJAdviceDeclaration advice : aspect.getMembers().getAllAdvices()) {
			if (adviceBody.containsKey(advice.getCode())) {
				AOJAdviceDeclaration matchAdvice = adviceBody.get(advice.getCode());
				AOJTypeDeclaration matchAspect = (AOJTypeDeclaration)matchAdvice.getOwner();
				String message = String.format("There are some indications that advices implement duplicated code : Aspect : %s :: Advice %s with Aspect %s :: Advice %s", 
						aspect.getFullQualifiedName(), advice.getKind(), matchAspect.getName(),  matchAdvice.getKind());
				List<String> list = new ArrayList<String>();
				list.add(message);
				registerReport(list, aspect);
			} else
				adviceBody.put(advice.getCode(), advice);	
		}
	}
}
