package br.ufsm.aopjungle.astor;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.commons.AOJCompilationUnit;
import br.ufsm.aopjungle.metamodel.commons.AOJImportDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJMethodDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.commons.AOJReferencedType;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.util.AOJClassLoader;

public class ConvertInterfaceToDefaultMethod extends CodeSmellImpl {
	
	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()){
			for (AOJClassDeclaration clazz : project.getClasses())
				findRefactorings(clazz);
		}
	}

	private void findRefactorings(AOJClassDeclaration clazz) {
		if (!clazz.getImplementedInterfaces().isEmpty()){
			for (AOJReferencedType referenced : clazz.getImplementedInterfaces()){
				AOJInterface i = loadInterface(referenced.getName(), clazz);
				if (i != null)
					for (AOJMethodDeclaration method : clazz.getMembers().getMethods()){
						List<String> list = new ArrayList<String>();
						for (String methodInterface : i.getMethods()) {
							if (method.getName().equalsIgnoreCase(methodInterface)){
								int size = method.getStatementsBody();
								if (size == 0 || (size == 1 && !method.getReturnType().getName().equals("void"))){
									String message = String.format( "Class "+ clazz.getName() +": "+ method.getName() + " ("+ i.getFullyQualifiedName() + ")");
									list.add(message);
								}
							}
						}
						if (list.size()>0)
							registerReport(list, clazz);
					}
			}
		}
		
	}

	@Override
	public String getLabel() {
		return "Convert Abstract Interface Method To Default Method";
	}
	
	
	private AOJInterface loadInterface(String name, AOJClassDeclaration _class) {
		// getting the simple name ex. Service<String> --> Service
		if (name.indexOf("<") != -1)
			name = name.substring(0, name.indexOf("<"));
		try {
			Class<?> c = Class.forName(name);
			return new AOJInterface(c);
			
		} catch (ClassNotFoundException e) {
			try {
				Class<?> c = AOJClassLoader.forName(name);
				return new AOJInterface(c);
				
			} catch (ClassNotFoundException e1) {
				name = getFullyQualifiedNameFromImport(name, _class);
				try {
					Class<?> c1 = Class.forName(name);
					return new AOJInterface(c1);
					
				} catch (ClassNotFoundException e2) {
					try {
						Class<?> c1 = AOJClassLoader.forName(name);
						return new AOJInterface(c1);
					
					} catch (ClassNotFoundException e3) {					
						return null;
					}
				}
				
			}
		}
	}
	
//	private AOJInterface loadInterface(String name, AOJClassDeclaration _class){
//		// getting the simple name ex. Service<String> --> Service
//		if (name.indexOf("<") != -1)
//			name = name.substring(0, name.indexOf("<"));
//		try {
//			Class<?> c = Class.forName(name);
//			return new AOJInterface(c);
//			
//		} catch (ClassNotFoundException e) {
//			Class<?> c = AOJClassLoader.forName(name);
//			if (c != null)
//				return new AOJInterface(c);
//			else {
//				name = getFullyQualifiedNameFromImport(name, _class);
//				try {
//					Class<?> c1 = Class.forName(name);
//					return new AOJInterface(c1);
//					
//				} catch (ClassNotFoundException e1) {
//					Class<?> c1 = AOJClassLoader.forName(name);
//					if (c1 != null)
//						return new AOJInterface(c1);
//					return null;
//				}
//			}
//		}
//	}
	
	private class AOJInterface {
		private String fullyQualifiedName;
		private Collection<String> methods;
		
		public AOJInterface(Class<?> c){
			this.fullyQualifiedName = c.getName();
			this.methods = new ArrayList<String>();
			for (Method m : c.getMethods())
				methods.add(m.getName());
		}
		
		public String getFullyQualifiedName() {
			return fullyQualifiedName;
		}
		public Collection<String> getMethods() {
			return methods;
		}
	}
	
	private String getFullyQualifiedNameFromImport(String name, AOJProgramElement owner) {
		String fullQualifiedName;
		AOJCompilationUnit cu = getCompilationUnit(owner);
		if (null != cu)
			for (AOJImportDeclaration imp : cu.getImportsDeclaration()) {
				if (imp.getCode().indexOf(name) != -1)
					return (imp.qualifiedName());	
				fullQualifiedName = findByWildcard(imp, name);
				if (! fullQualifiedName.isEmpty())
					return fullQualifiedName;
			}
		
		fullQualifiedName = findAtJavaLang(name);
		if (! fullQualifiedName.isEmpty())
			return fullQualifiedName;
		
		return owner.getPackage().getName() + "." + name; // If no type was found, the extends type is located at the same package
	}
	
	private String findAtJavaLang(String name) {
		String fullQualifiedName = "java.lang." + name;
		if (forClass (fullQualifiedName))  
			return fullQualifiedName;		
		return "";
	}
	
	private String findByWildcard(AOJImportDeclaration imp, String name) {
		if (imp.getCode().indexOf("*") != -1) { // there's not * on import declaration
			String fullQualifiedName = imp.qualifiedName().replace("*", name); // replace * by type name
			if (forClass (fullQualifiedName)) // try to find by classloader 
				return fullQualifiedName;
		}	

		return "";
	}

	private boolean forClass(String fullQualifiedName) {
		boolean result = true;
		try {
			Class.forName(fullQualifiedName);
		} catch (ClassNotFoundException e) {
			result = false;
		}
		return result;
	}

	private AOJCompilationUnit getCompilationUnit(AOJProgramElement owner) {
		if (owner instanceof AOJCompilationUnit)
			return (AOJCompilationUnit)owner;
		if (owner instanceof AOJProject)
			return null;
		return getCompilationUnit(owner.getOwner());
	}

}

