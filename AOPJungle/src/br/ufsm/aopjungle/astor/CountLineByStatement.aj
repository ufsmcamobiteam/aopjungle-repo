package br.ufsm.aopjungle.astor;

import org.aspectj.org.eclipse.jdt.core.dom.AjASTVisitor;
import org.aspectj.org.eclipse.jdt.core.dom.Statement;

import br.ufsm.aopjungle.aspectj.AOJungleVisitor;

public aspect CountLineByStatement {
	pointcut visitStatements(AOJungleVisitor visitor) : 
		execution(* AjASTVisitor+.visit(Statement+)) && this(visitor);

	before (AOJungleVisitor visitor) : visitStatements (visitor) {
//		visitor.addCurrentLineCount();
	}
}
