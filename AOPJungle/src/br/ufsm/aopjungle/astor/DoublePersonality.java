package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import br.ufsm.aopjungle.astor.util.AstorProperties;
import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.commons.AOJReferencedType;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class DoublePersonality extends CodeSmellImpl {
	@XStreamOmitField
	private Hashtable<AOJReferencedType, List<AOJAspectDeclaration>> interfaceTable = new Hashtable<AOJReferencedType, List<AOJAspectDeclaration>>();
	
	@Override
	public String getLabel() {
		return "Double Personality";
	}

	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) 
			for (AOJAspectDeclaration aspect : project.getAspects())
				loadInterfaces(aspect);
		findDoublePersonality();
	}

	private void findDoublePersonality() {
		for (Map.Entry<AOJReferencedType, List<AOJAspectDeclaration>> intfTable : interfaceTable.entrySet()) {
			if (intfTable.getValue().size() > AstorProperties.getDoublePersonalityInterfaces()) {
				for (AOJTypeDeclaration aspect : intfTable.getValue()) {
					String message = String.format("Some indication of double personality was found in aspect : %s", aspect.getFullQualifiedName());
					List<String> list = new ArrayList<String>();
					list.add(message);
					registerReport(list, aspect);
				}
			}	
		}
	}
		
	private void loadInterfaces(AOJAspectDeclaration aspect) {
		for (AOJReferencedType intf : aspect.getImplementedInterfaces()) {
			List<AOJAspectDeclaration> aspects = interfaceTable.get(intf);
			if (aspects == null) 
				aspects = new ArrayList<AOJAspectDeclaration>(); 
			aspects.add(aspect);
			interfaceTable.put(intf, aspects); 
		}
	}
}
