package br.ufsm.aopjungle.builder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import com.thoughtworks.xstream.XStream;

public class AOJExporterXML extends AOJExporter {
	//private static XStream xstream;
	
	public static void generate(String fileName, Object obj) throws IOException {
		XStream xstream = new XStream();
		Writer out = new BufferedWriter (new FileWriter(new File (fileName)));
		xstream.autodetectAnnotations(true);
		xstream.toXML(obj, out);		
		out.close();
	}
}
