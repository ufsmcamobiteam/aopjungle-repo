package br.ufsm.aopjungle.builder;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.JavaModelException;

import br.ufsm.aopjungle.aspectj.AOJCompiler;
import br.ufsm.aopjungle.binding.AOJProgressMonitor;
import br.ufsm.aopjungle.exception.AOJCompilerException;
import br.ufsm.aopjungle.labs.AOJBindingOptionEnum;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.resource.ProjectResources;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
public class AOJWorkspace {
	@XStreamOmitField
	private static AOJWorkspace workspace;
	private List<AOJProject> projects;
	private AOJProject currentProject;
	
	public static AOJWorkspace getInstance() {
		if (workspace == null) 
			workspace = new AOJWorkspace();
		
		return workspace;
	}
	
	private AOJWorkspace() {
//		AOJungleASMVisitor visitor = new AOJungleASMVisitor(); 
//		AjBuildManager.setAsmHierarchyBuilder(visitor);		
	}

	public void build() {
		try {
			AOJProgressMonitor monitor = new AOJProgressMonitor();
			ResourcesPlugin.getWorkspace().build(IncrementalProjectBuilder.FULL_BUILD, 
					monitor);
		} catch (CoreException e) {
			e.printStackTrace();
		}		
	}

	public void build(AOJProject project) {
		System.out.println("Starting compilation project : " + project.getName());
		AOJCompiler builder = new AOJCompiler();
		try {
			builder.build(project);
		} catch (AOJCompilerException e) {
			e.printStackTrace();
		}
		
//		try {
//			AOJProgressMonitor monitor = new AOJProgressMonitor();
//			ResourcesPlugin.getWorkspace().getRoot().getProject(project.getName()).build(
//				IncrementalProjectBuilder.FULL_BUILD, 
//				monitor);
//		} catch (CoreException e) {
//			e.printStackTrace();
//		}
		System.out.println("Ends compilaton project : " + project.getName());
	}	
	
	public void setProjects (List<AOJProject> projects) {
		this.projects = projects;
	}
	
	public List<AOJProject> getProjects() {
		if (projects == null) 
			projects = new ArrayList<AOJProject>();
		return projects;
	}

	public void loadProjects(AOJBindingOptionEnum setBindings) throws JavaModelException, CoreException {
//		if (setBindings == AOJBindingOptionEnum.BINDING_ON) 
//			AJBuilder.addAdviceListener(new AOJAdviceListener());
		List<IProject> workspaceProjects = new ArrayList<IProject>();
		workspaceProjects = ProjectResources.getInstance().getProjects();
		//projects = null;
		for (IProject project : workspaceProjects) {	
			if (project.isNatureEnabled("org.eclipse.jdt.core.javanature")) {
				project.open(new NullProgressMonitor());
				AOJProject projectNode = new AOJProject(project);
				currentProject = projectNode;
				getProjects().add(projectNode);
				build(projectNode); // Compile project to traverse nodes
			}
		}
		
//		build();
	}
	
	public AOJProject getCurrentProject() {
		return currentProject;
	}
}
