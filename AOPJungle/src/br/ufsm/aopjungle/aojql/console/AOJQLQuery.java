package br.ufsm.aopjungle.aojql.console;

import java.util.List;

import br.ufsm.aopjungle.exception.AOJQueryFailException;

public interface AOJQLQuery {
	public AOJResultSet execute (String query, List objectList) throws AOJQueryFailException;
	public AOJResultSet execute (String query) throws AOJQueryFailException;
	public String getBanner();
	public String getCursor();
}
