package br.ufsm.aopjungle.aojql.console;

public class AOJQLTreeNode {
	private String label;
	private AOJQLTreeParent parent;

	public AOJQLTreeNode (String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public void setParent(AOJQLTreeParent parent) {
		this.parent = parent;
	}
	public AOJQLTreeParent getParent() {
		return parent;
	}
}
