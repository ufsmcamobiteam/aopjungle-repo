package br.ufsm.aopjungle.aojql.console;

 
public class AOJResultSet {
	private AOJQLTreeParent root;
	
	public AOJResultSet() {
		setRoot(new AOJQLTreeParent("Results"));
	}
	
	public AOJQLTreeParent getRoot() {
		return root;
	}

	public void setRoot(AOJQLTreeParent root) {
		this.root = root;
	}

	public void clear() {
		getRoot().clear();
	}
}



