package br.ufsm.aopjungle.labs;

import org.aspectj.tools.ajc.Main;
import org.aspectj.asm.*;
import org.aspectj.asm.IProgramElement.Kind;
import org.aspectj.bridge.*;
import org.eclipse.ajdt.core.model.AJProjectModelFacade;
import org.eclipse.ajdt.core.model.AJProjectModelFactory;
import org.eclipse.core.resources.IProject;

import br.ufsm.aopjungle.aspectj.AOJWalker;
import br.ufsm.aopjungle.resource.ProjectResources;

import java.io.*;
import java.util.*;

public class AffectedFilesReporter implements Runnable {

	public void execute () {
     
	}
	
	public static void main(String[] args) throws IOException {
		Main main = new Main();
		PrintStream toConfig = System.out;
		FileOutputStream fin = null;
		if ((args.length > 1) && ("-to".equals(args[0]))) {
			File config = new File(args[1]);
			fin = new FileOutputStream(config);
			toConfig = new PrintStream(fin, true);
			String[] temp = new String[args.length-2];
			System.arraycopy(args, 2, temp, 0, temp.length);
			args = temp;
		}

		Runnable runner = new AffectedFilesReporter(null /*toConfig*/);
		main.setCompletionRunner(runner);
		// should add -emacssym to args if not already there
		main.runMain(args, false);
		
		if (null != fin) {
			fin.close();
		}   		
	}
	
	final PrintStream sink;

	public AffectedFilesReporter(PrintStream sink) {
		this.sink = (null == sink ? System.out : sink);
	}

	public void run() {
//		IHierarchy hierarchy = AsmManager.getDefault().getHierarchy();
		AsmManager asmManager;
		if (null != AsmManager.lastActiveStructureModel)
			asmManager = AsmManager.lastActiveStructureModel;
		else
			asmManager = AsmManager.createNewStructureModel(Collections.<File, String> emptyMap());
		asmManager.addListener(new AOJListener());
		IHierarchy hierarchy = asmManager.getHierarchy();
		if (null == hierarchy) {
			sink.println("# no structure model - use -emacssym option");
			return;
		}
		List <IProgramElement> nodes = new LinkedList<IProgramElement>();
		List <IProgramElement> newNodes = new LinkedList<IProgramElement>();
		// root could be config file or blank root - either way, use kids
		nodes.addAll(hierarchy.getRoot().getChildren());
		hierarchy.getRoot().walk(new AOJWalker());
		while (0 < nodes.size()) {
			for (ListIterator it = nodes.listIterator(); it.hasNext();) {
				IProgramElement node = (IProgramElement) it.next();
				System.out.println("node : " + node.getDeclaringType()); 
				if (isAffectedElement(node)) {
					List<IRelationship> affects = findAffects(node);
					if (! affects.isEmpty()) {
						sink.println ("  relationship node " + node.getName() + "(" + node.getKind() + ")");
						for (IRelationship a : affects)
							sink.println ("     "+ a.getName() + " " + a.getTargets());
					}	
				}	
				
				newNodes.addAll(node.getChildren());
				it.remove();
			}
			nodes.addAll(newNodes);
			newNodes.clear();
		}
	} 

	private boolean isAffectedElement(IProgramElement node) {
		return ( (node.getKind() != Kind.INTER_TYPE_METHOD) &&
				(node.getKind() != Kind.INTER_TYPE_FIELD) &&
				(node.getKind() != Kind.INTER_TYPE_CONSTRUCTOR) &&
				(node.getKind() != Kind.SOURCE_FOLDER) &&
				(node.getKind() != Kind.IMPORT_REFERENCE) &&
				(node.getKind() != Kind.FILE) &&
				(node.getKind() != Kind.FILE_ASPECTJ) &&
				(node.getKind() != Kind.FILE_JAVA) &&
				(node.getKind() != Kind.FILE_LST) &&
				(node.getKind() != Kind.INTER_TYPE_PARENT));
	}

	private List<IRelationship> findAffects(final IProgramElement baseNode) {
		List<IRelationship> result = new ArrayList<IRelationship>();
		final IRelationshipMap map  = AsmManager.lastActiveStructureModel.getRelationshipMap();     
		List <IProgramElement> nodes = new LinkedList<IProgramElement>();
		List <IProgramElement> newNodes = new LinkedList<IProgramElement>();
		nodes.add(baseNode);
		while (0 < nodes.size()) {
			for (ListIterator<IProgramElement> iter = nodes.listIterator(); iter.hasNext();) {
				IProgramElement node = (IProgramElement) iter.next();
				List<IRelationship> relations = map.get(node);
				if (null != relations) {
					for (Iterator<IRelationship> riter = relations.iterator(); 
							riter.hasNext();) {
						IRelationship rel = (IRelationship) riter.next();
						IRelationship.Kind kind = rel.getKind();
						if ( ( ((kind == IRelationship.Kind.ADVICE) 
									|| (kind == IRelationship.Kind.DECLARE_INTER_TYPE)) )
						 )	
							result.add(rel);
					}
				}
				iter.remove();
				newNodes.addAll(node.getChildren());
			}
			nodes.addAll(newNodes);
			newNodes.clear();
		}
		return result;		
	}

	private boolean isAffected(final IProgramElement fileNode) {
		final IRelationshipMap map  = 
				AsmManager.lastActiveStructureModel.getRelationshipMap();     
		List <IProgramElement> nodes = new LinkedList<IProgramElement>();
		List <IProgramElement> newNodes = new LinkedList<IProgramElement>();
		nodes.add(fileNode);
		while (0 < nodes.size()) {
			for (ListIterator<IProgramElement> iter = nodes.listIterator(); 
					iter.hasNext();) {
				IProgramElement node = (IProgramElement) iter.next();
				List<IRelationship> relations = map.get(node);
				if (null != relations) {
					for (Iterator<IRelationship> riter = relations.iterator(); 
							riter.hasNext();) {
						IRelationship.Kind kind =
								((IRelationship) riter.next()).getKind();
						if ((kind == IRelationship.Kind.ADVICE)
								|| (kind == IRelationship.Kind.DECLARE_INTER_TYPE)) {
							return true;
						}
					}
				}
				iter.remove();
				newNodes.addAll(node.getChildren());
			}
			nodes.addAll(newNodes);
			newNodes.clear();
		}
		return false;
	}	
}
