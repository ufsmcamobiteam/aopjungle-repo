package br.ufsm.aopjungle.metamodel.commons;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.hibernate.annotations.ForeignKey;

@Entity
@ForeignKey(name="primitivetype_fk")
@DiscriminatorValue("PRI")
public class AOJPrimitiveType  extends AOJType {
	protected AOJPrimitiveType() {

	}
	
	public AOJPrimitiveType(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
	}

}

