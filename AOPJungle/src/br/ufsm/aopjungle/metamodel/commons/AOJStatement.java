package br.ufsm.aopjungle.metamodel.commons;

import javax.persistence.Entity;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;

/**
 * @author Janio Elias Teixeira Junior
 * 
 * This class represents a statement  in the system
 * 
 */

@Entity
public class AOJStatement  extends AOJMember {



	protected AOJStatement() {
		
	}
	
	public AOJStatement(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
	}

	@Override
	public int getModifiersAsInteger() {
		return 0;
	}

	@Override
	protected void loadHandler() {
		
	}

	
}
