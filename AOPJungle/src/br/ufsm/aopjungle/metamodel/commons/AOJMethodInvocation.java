package br.ufsm.aopjungle.metamodel.commons;

import org.aspectj.org.eclipse.jdt.core.dom.Expression;
import org.aspectj.org.eclipse.jdt.core.dom.MethodInvocation;

public class AOJMethodInvocation extends AOJExpression {
	
	private String methodName = "unknown";
	private String identifier = "unknown";
	
	public AOJMethodInvocation(AOJProgramElement owner, Expression expression) {		
		super(owner, expression.toString());
		if (((MethodInvocation)expression).getName() != null)
			this.methodName = ((MethodInvocation)expression).getName().getFullyQualifiedName();
		if (((MethodInvocation)expression).getExpression() != null)
			this.identifier = ((MethodInvocation)expression).getExpression().toString();
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

}



