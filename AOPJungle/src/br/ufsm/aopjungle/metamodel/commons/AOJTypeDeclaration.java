package br.ufsm.aopjungle.metamodel.commons;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.AjTypeDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.ArrayType;
import org.aspectj.org.eclipse.jdt.core.dom.PrimitiveType;
import org.aspectj.org.eclipse.jdt.core.dom.QualifiedType;
import org.aspectj.org.eclipse.jdt.core.dom.SimpleType;
import org.aspectj.org.eclipse.jdt.core.dom.Type;
import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.MetaValue;

import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration;
import br.ufsm.aopjungle.metrics.AOJMetrics;
import br.ufsm.aopjungle.metrics.AOJTypeMetrics;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@MappedSuperclass
public abstract class AOJTypeDeclaration extends AOJMember implements AOJContainer, AOJAnnotable, AOJMetrics {
	private String fullQualifiedName;
	/**
	 * Back track package for query facilities of pack and project 
	 */
	@SuppressWarnings("unused")
	@OneToOne (cascade=CascadeType.ALL)
	private AOJPackageDeclaration pack;
	/**
	 * Parent represents the original node
	 * @see use superType() to get the extends type of this type
	 */
	@Transient
	private AOJReferencedType parent;
	
	/**
	 * A normalized superType represented by AOJContainer
	 */
	@Any(metaColumn = @Column(name = "super_type"), fetch = FetchType.EAGER)
	@AnyMetaDef(idType = "long", metaType = "string", metaValues = {
			@MetaValue(value = "ASP", targetEntity = AOJAspectDeclaration.class),
			@MetaValue(value = "INT", targetEntity = AOJInterfaceDeclaration.class),
			@MetaValue(value = "CLA", targetEntity = AOJClassDeclaration.class),
			@MetaValue(value = "PHL", targetEntity = AOJContainerPlaceHolder.class)})
	@JoinColumn(name = "super_id")
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private AOJContainer superType;
	protected abstract void loadMembers(); 
	public abstract String getASTTypeName();
	public abstract String getASTTypeFullQualifiedName();

	public void loadHandler() {
		setInternalHandler(getFullQualifiedName());
	}
	
	public void loadMetrics() {
		long nOfLines = 0;//getMetrics().getNumberOfLines();
		List<AOJMetrics> members = new ArrayList<AOJMetrics>();
		getMembers().getMetricParticipants(members);
		for (AOJMetrics metricParticipant : members) 
			nOfLines += metricParticipant.getMetrics().getNumberOfLines();

		nOfLines += 3; // Add open/close curly brackets + header		
		getMetrics().setNumberOfLines(nOfLines);		
		
		long nOfMethods = getMetrics().getNumberOfMethods() + getMembers().getMethods().size();
		long nOfFields = getMetrics().getNumberOfFields() + getMembers().getAttributes().size();

		getMetrics().setNumberOfMethods(nOfMethods);
		getMetrics().setNumberOfFields(nOfFields);
	}

	public AOJTypeDeclaration(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		fullQualifiedName = getASTTypeFullQualifiedName();
//		binding = AOJBindingFactory.eINSTANCE.createBinding();
		parent = null;
		superType = null;
		
		loadName();
		loadParent();
		loadSuperType();
		loadMembers();
		loadPack();
		loadHandler();
		loadToReferenceMap();
	}
	
	private void loadSuperType() {
		if (getNode() instanceof AjTypeDeclaration) {
			Type type = ((AjTypeDeclaration)getNode()).getSuperclassType();
			setSuperType( new AOJContainerPlaceHolder(getFullyQualifiedName(type), type, this) );
		}	
	}
	
	protected void loadName() {
		setName(getASTTypeName());
	}

	private void loadPack() {
		pack = getOwner() instanceof AOJCompilationUnit ? ((AOJCompilationUnit)getOwner()).getPackage() : null;
	}

	abstract public AOJTypeMetrics getMetrics();
	
	protected AOJTypeDeclaration() {
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((fullQualifiedName == null) ? 0 : fullQualifiedName
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AOJTypeDeclaration))
			return false;
		AOJTypeDeclaration other = (AOJTypeDeclaration) obj;
		if (fullQualifiedName == null) {
			if (other.fullQualifiedName != null)
				return false;
		} else if (!fullQualifiedName.equals(other.fullQualifiedName))
			return false;
		return true;
	}

	protected abstract void loadParent(); 

	public String getMetaName() {
		return "AbstractType";
	}

	public AOJReferencedType getParent() {
		return parent;
	}

	public void setParent(AOJReferencedType parent) {
//		parent.addNumberOfDescendents(1);
		this.parent = parent;
//		findNumberOfParents(parent);
	}

	public AOJContainer getSuperType() {
		return superType;
	}

	public void setSuperType(AOJContainer superType) {
		this.superType = superType;
	}

	public String getFullQualifiedName() {
		return fullQualifiedName;
	}

	public void setFullQualifiedName (String name) {
		fullQualifiedName = name;
	}
	
	private String getFullyQualifiedName(ASTNode node) {
		if (null == node)
			return "java.lang.Object";
		if (node instanceof PrimitiveType)
			return ((PrimitiveType)node).toString();
		if (node instanceof SimpleType)
			return getFullyQualifiedNameFromImport(((SimpleType)node).getName().toString());
		if (node instanceof QualifiedType)
			return ((QualifiedType)node).getName().toString();
		if (node instanceof ArrayType)
			return ((ArrayType)node).getElementType().toString() + getBracket(((ArrayType)node).getDimensions());
		
		return "Unknown";
	}	
	
	private String getBracket(int dimensions) {
		StringBuilder sb = new StringBuilder(); 
		for (int i = 0; i < dimensions; i++) 
			sb.append("[]");
		return sb.toString();
	}
	
	private String getFullyQualifiedNameFromImport(String name) {
		String fullQualifiedName;
		AOJCompilationUnit cu = getCompilationUnit (getOwner());
		if (null != cu)
			for (AOJImportDeclaration imp : cu.getImportsDeclaration()) {
				if (imp.getCode().indexOf(name) != -1)
					return (imp.qualifiedName());	
				fullQualifiedName = findByWildcard(imp, name);
				if (! fullQualifiedName.isEmpty())
					return fullQualifiedName;
			}
		
		fullQualifiedName = findAtJavaLang(name);
		if (! fullQualifiedName.isEmpty())
			return fullQualifiedName;
		
		return getPackage().getName() + "." + name; // If no type was found, the extends type is located at the same package
	}

	private String findAtJavaLang(String name) {
		String fullQualifiedName = "java.lang." + name;
		if (forClass (fullQualifiedName))  
			return fullQualifiedName;		
		return "";
	}
	
	private String findByWildcard(AOJImportDeclaration imp, String name) {
		if (imp.getCode().indexOf("*") != -1) { // there's not * on import declaration
			String fullQualifiedName = imp.qualifiedName().replace("*", name); // replace * by type name
			if (forClass (fullQualifiedName)) // try to find by classloader 
				return fullQualifiedName;
		}	
		return "";
	}

	private boolean forClass(String fullQualifiedName) {
		boolean result = true;
		try {
			Class.forName(fullQualifiedName);
		} catch (ClassNotFoundException e) {
			result = false;
		}
		return result;
	}	
	
	private AOJCompilationUnit getCompilationUnit(AOJProgramElement owner) {
		if (owner instanceof AOJCompilationUnit)
			return (AOJCompilationUnit)owner;
		if (owner instanceof AOJProject)
			return null;
		return getCompilationUnit(owner.getOwner());
	}	
}