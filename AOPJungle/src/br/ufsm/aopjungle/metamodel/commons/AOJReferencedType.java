package br.ufsm.aopjungle.metamodel.commons;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.ArrayType;
import org.aspectj.org.eclipse.jdt.core.dom.PrimitiveType;
import org.aspectj.org.eclipse.jdt.core.dom.QualifiedType;
import org.aspectj.org.eclipse.jdt.core.dom.SimpleType;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@Entity
@DiscriminatorValue("REF")
public class AOJReferencedType extends AOJType {
	private String fullQualifiedName;
	private boolean external;
	
	protected AOJReferencedType() {

	}

	@Override
	public String toString() {
		return fullQualifiedName;
	}
	
	public AOJReferencedType(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		external = true;
		fullQualifiedName = getFullyQualifiedName (node);
	}

	private String getFullyQualifiedName(ASTNode node) {
		if (node instanceof PrimitiveType)
			return ((PrimitiveType)node).toString();
		if (node instanceof SimpleType)
			return ((SimpleType)node).getName().getFullyQualifiedName();
		if (node instanceof QualifiedType)
			return ((QualifiedType)node).getName().getFullyQualifiedName();
		if (node instanceof ArrayType)
			return ((ArrayType)node).getElementType().toString() + getBracket(((ArrayType)node).getDimensions());
		
		return "Unknown";
	}

	private String getBracket(int dimensions) {
		StringBuilder sb = new StringBuilder(); 
		for (int i = 0; i < dimensions; i++) 
			sb.append("[]");
		return sb.toString();
	}

	/**
	 * @param fullQualifiedName
	 */
	public void setFullQualifiedName(String fullQualifiedName) {
		this.fullQualifiedName = fullQualifiedName;
	}

	public String getFullQualifiedName() {
		return fullQualifiedName;
	}

	public boolean isExternal() {
		return external;
	}

	public void setExternal(boolean external) {
		this.external = external;
	}

//	@Override
//	public boolean equals(Object obj) {
//		boolean result = (obj != null) && (obj instanceof AOJReferencedType);
//		// TODO : This is an hypothetical comparison of simpleName. 
//		//        Interface Implementation does not carry binding info
//		//        We should resolve dependencies to get fullQualifiedName here, not single name
//		if (result)
//			result = getName().trim().equals(((AOJReferencedType)obj).getName().trim());
//		return result;
//	}
//	
//	@Override
//	public int hashCode() {
//		return 5150;
//	}
}
