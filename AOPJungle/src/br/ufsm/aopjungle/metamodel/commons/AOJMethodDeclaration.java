package br.ufsm.aopjungle.metamodel.commons;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.MethodDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.Name;
import org.aspectj.org.eclipse.jdt.core.dom.SingleVariableDeclaration;

import br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration;
import br.ufsm.aopjungle.util.AOJSourceService;
import br.ufsm.aopjungle.util.StringUtil;

@Entity
@DiscriminatorValue("MET")
public class AOJMethodDeclaration extends AOJBehaviourKind  {
	private boolean isConstructor;
//	@Transient // TODO : Implement
//	private AOJBindingFacade bindings;
	
	private String codeBody = "";
	private Integer statementsBody = 0;

	protected MethodDeclaration getMethodDeclaration() {
		ASTNode node = getNode();
		return (MethodDeclaration)node;
	}
	
	protected AOJMethodDeclaration() {
		
	}
	
	protected void loadOnType() { }// Placeholder for Intertype;
	
	protected void loadPosition() { }// Placeholder for Intertype;
	
	public AOJMethodDeclaration(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		loadPosition();
		loadOnType();
		loadHandler();
		loadToReferenceMap();
		loadCodeBody();

	}

	public void loadMetrics() {
		super.loadMetrics();
		getMetrics().setNumberOfStatements(getMethodDeclaration().getBody() == null ? 0 : 
		AOJSourceService.getNumberOfStatements(getMethodDeclaration().getBody().statements()));
		long nOfLines = 0; //getMetrics().getNumberOfLines();
		nOfLines += getMethodDeclaration().getBody() == null ? 0 : 
		AOJSourceService.getNumberOfStatements(getMethodDeclaration().getBody().statements());
		if (getOwner() instanceof AOJInterfaceDeclaration)
			nOfLines++; // Add header		
		else if (getModifiers().exists(AOJModifierEnum.	ABSTRACT))
			nOfLines++; // Add header		
		else
			nOfLines += 3; // Add open/close curly brackets + header		
		getMetrics().setNumberOfLines(nOfLines);
		getMetrics().setNumberOfInAffects(getBindingModel().getNumberOfIn());
		getMetrics().setNumberOfOutAffects(getBindingModel().getNumberOfOut());
	}

//	private void loadBindings(AOJBindingFacade instance) {
//		this.bindings = instance;
//	}
	
	@Override
	protected void loadParameters() {
		for (Object param : getMethodDeclaration().parameters()) 
			getParameters().add( new AOJParameter((SingleVariableDeclaration) param, this));
	}
	
	@Override
	protected void loadThrownExceptions() {
		for (Object exception : getMethodDeclaration().thrownExceptions()) 
			getThrownExceptions().add( new AOJException((Name) exception, this));
	}

	@Override
	protected void loadReturnType() {
		AOJType result;
		if (null == getMethodDeclaration().getReturnType2()) 
			result = new AOJReferencedType(null, this);
		else if (getMethodDeclaration().getReturnType2().isPrimitiveType())
			result = new AOJPrimitiveType(getMethodDeclaration().getReturnType2(), this);
		else
			result = new AOJReferencedType(getMethodDeclaration().getReturnType2(), this);
		setReturnType(result);
	}

	@Override
	protected void loadName() {
		super.setName(getMethodDeclaration().getName().toString());
	}

	@Override
	public String getMetaName() {
		return "Method";
	}
	
	public boolean isConstructor() {
		return isConstructor;
	}

	@Override
	public int getModifiersAsInteger() {
		return getMethodDeclaration().getModifiers();
	}

	@Override
	protected void loadCodeBody() {
		if (getMethodDeclaration().getBody() != null){
			setCodeBody(getMethodDeclaration().getBody().toString());
			setStatementsBody(getMethodDeclaration().getBody().statements().size());
		}
	}

	@Override
	protected void loadHandler() {
		setInternalHandler(StringUtil.buildHandler(getPackage().getName(), ((AOJTypeDeclaration)getOwner()).getName(), getSignature()));
	}

	public Integer getStatementsBody() {
		return statementsBody;
	}

	public void setStatementsBody(Integer statementsBody) {
		this.statementsBody = statementsBody;
	}

	public String getCodeBody() {
		return codeBody;
	}

	public void setCodeBody(String codeBody) {
		this.codeBody = codeBody;
	}

}