package br.ufsm.aopjungle.metamodel.commons;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;

import br.ufsm.aopjungle.metrics.AOJMetricsFactory;
import br.ufsm.aopjungle.metrics.AOJTypeMetrics;
import br.ufsm.aopjungle.util.StringUtil;

/**
 * A Simple place holder for inherited classes, since AJDT does not hold
 * inherited objects like ajTypeDeclaration types.
 * Only a few of information is available for inherited objects, since they are
 * Type types. 
 * 
 * @author Cristiano De Faveri
 *
 */
@Entity
@DiscriminatorValue("PHL")
public class AOJContainerPlaceHolder extends AOJTypeDeclaration  {
	@OneToOne (cascade=CascadeType.ALL)
	private AOJTypeMetrics metrics; 
	
	protected AOJContainerPlaceHolder() {
		
	}
	
	public AOJContainerPlaceHolder(String fullQualifiedName, ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		setFullQualifiedName(fullQualifiedName);
		metrics = AOJMetricsFactory.eINSTANCE.createTypeMetrics();
		setName(StringUtil.unQualify(fullQualifiedName));
		setInternalHandler(getFullQualifiedName());
//		loadToReferenceMap();
	}
	
	@Override
	public AOJCommonTypeMember getMembers() {
		return null;
	}

	@Override
	public int getModifiersAsInteger() {
		return 0;
	}

	@Override
	protected void loadMembers() {
		
	}

	@Override
	public String getASTTypeName() {
		return null;
	}

	@Override
	public String getASTTypeFullQualifiedName() {
		return "";
	}

	@Override
	protected void loadParent() {
		
	}

	@Override
	public void loadMetrics() {
		super.loadMetrics();
		// Other specific metrics here
	}

	@Override
	protected void loadName() {
		
		
	}

	@Override
	public AOJTypeMetrics getMetrics() {
		return metrics;
	}

}
