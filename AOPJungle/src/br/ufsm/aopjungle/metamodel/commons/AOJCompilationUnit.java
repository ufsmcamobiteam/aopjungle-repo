package br.ufsm.aopjungle.metamodel.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.AST;
import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.ASTParser;
import org.aspectj.org.eclipse.jdt.core.dom.CompilationUnit;
import org.aspectj.org.eclipse.jdt.internal.compiler.impl.CompilerOptions;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IImportDeclaration;
import org.eclipse.jdt.core.JavaModelException;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ManyToAny;
import org.hibernate.annotations.MetaValue;

import br.ufsm.aopjungle.aspectj.AOJungleVisitor;
import br.ufsm.aopjungle.log.AOJLogger;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJAnnotationTypeDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJEnumDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration;
import br.ufsm.aopjungle.metrics.AOJMetrics;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

/**
 * This class represents a wrapper for compilation units
 * 
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 *         Federal University of Santa Maria <br>
 *         Programming Languages and Database Research Group <br>
 */
@Entity
public class AOJCompilationUnit extends AOJProgramElement implements Serializable {
	@XStreamOmitField
	private static final long serialVersionUID = 1L;
	@XStreamOmitField
	@Transient
	private ICompilationUnit compilationUnit;
	@XStreamOmitField
	@Transient
	private AOJungleVisitor visitor;
	@OneToMany(cascade = CascadeType.ALL)
	// @JoinColumn(name="compilation_unit_fk")
	private List<AOJImportDeclaration> importsDeclaration = new ArrayList<AOJImportDeclaration>();
	@ManyToAny(metaColumn = @Column(name = "cunit_type"))
	@AnyMetaDef(idType = "long", metaType = "string", metaValues = { @MetaValue(targetEntity = AOJClassDeclaration.class, value = "CLA"),
			@MetaValue(targetEntity = AOJInterfaceDeclaration.class, value = "INT"), @MetaValue(targetEntity = AOJAspectDeclaration.class, value = "ASP"), })
	@Cascade({ org.hibernate.annotations.CascadeType.ALL })
	@JoinTable(name = "cunit_typeble", joinColumns = @JoinColumn(name = "cunittypeble_id"), inverseJoinColumns = @JoinColumn(name = "typeble_id"))
	private List<AOJContainer> types = new ArrayList<AOJContainer>();
	@Transient
	private AOJPackageDeclaration pack;
	@Transient
	private String source;
	@Transient
	private CompilationUnit cUnitAST;

	public AOJCompilationUnit(ICompilationUnit compilationUnit, AOJProgramElement owner) {
		super(owner);
		this.compilationUnit = compilationUnit;
		if (owner instanceof AOJPackageDeclaration)
			pack = (AOJPackageDeclaration) owner;
		setName(compilationUnit.getElementName());
		setMetaName("Compilation Unit");
		loadImports(compilationUnit);
		try {
			setSource(compilationUnit.getSource());
		} catch (JavaModelException e1) {
			AOJLogger.getLogger().error(e1);
		}

		// Create Visitors for the compilationUnit
		try {
			createVisitor();
		} catch (JavaModelException e) {
			AOJLogger.getLogger().error(e);
		}

	}

	private void loadImports(ICompilationUnit compilationUnit) {
		try {
			for (IImportDeclaration i : compilationUnit.getImports())
				getImportsDeclaration().add(new AOJImportDeclaration(i.getSource()));
		} catch (JavaModelException e) {
			AOJLogger.getLogger().error(e);
		}
	}

	public ICompilationUnit getCompilationUnit() {
		return compilationUnit;
	}

	@SuppressWarnings("unused")
	private AOJCompilationUnit() {

	}

	/**
	 * TODO: JLS3 does not reveal AnnotationTypeDeclaration Until version 1.7.2
	 * aspect does not implement JLS4 of the AST
	 * 
	 * @throws JavaModelException
	 */
	private void createVisitor() throws JavaModelException {
		// if (!this.getName().equals("AOJClassLoader.java")){
		int level = AST.JLS3;
		ASTParser parser = ASTParser.newParser(level);
		Map<String, String> options = new HashMap<String, String>();
		options.put(CompilerOptions.OPTION_Source, "1.5");
		parser.setCompilerOptions(options);
		// parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setResolveBindings(true);
		parser.setSource(compilationUnit.getSource().toCharArray());
		try {
			ASTNode tUnit = parser.createAST(null);
			cUnitAST = (CompilationUnit) tUnit;
			visitor = new AOJungleVisitor(this);
			cUnitAST.accept(visitor);
		} catch (ClassCastException e) { 	
			System.out.println("Error: Visitor for compilationUnit was not created " +  e.getMessage());
		}
	}

	public void setDeclaredTypes(List<AOJContainer> types) {
		this.types = types;
	}

	public List<AOJContainer> getDeclaredTypes() {
		return types;
	}

	public List<AOJImportDeclaration> getImportsDeclaration() {
		return importsDeclaration;
	}

	public List<AOJMetrics> getTypeMetrics() {
		List<AOJMetrics> result = new ArrayList<AOJMetrics>();

		for (AOJContainer type : getDeclaredTypes()) {
			if (type instanceof AOJMetrics)
				result.add((AOJMetrics) type);
		}

		return result;
	}

	public List<AOJAspectDeclaration> getAspects() {
		List<AOJAspectDeclaration> result = new ArrayList<AOJAspectDeclaration>();
		for (AOJContainer type : getDeclaredTypes()) {
			if (type instanceof AOJAspectDeclaration)
				result.add((AOJAspectDeclaration) type);
		}

		return result;
	}

	public List<AOJClassDeclaration> getClasses() {
		List<AOJClassDeclaration> result = new ArrayList<AOJClassDeclaration>();
		for (AOJContainer type : getDeclaredTypes()) {
			if ((type instanceof AOJClassDeclaration) && !(type instanceof AOJAspectDeclaration))
				result.add((AOJClassDeclaration) type);
		}

		return result;
	}

	public List<AOJInterfaceDeclaration> getInterfaces() {
		List<AOJInterfaceDeclaration> result = new ArrayList<AOJInterfaceDeclaration>();
		for (AOJContainer type : getDeclaredTypes()) {
			if (type instanceof AOJInterfaceDeclaration)
				result.add((AOJInterfaceDeclaration) type);
		}

		return result;
	}

	public List<AOJEnumDeclaration> getEnums() {
		List<AOJEnumDeclaration> result = new ArrayList<AOJEnumDeclaration>();
		for (AOJContainer type : getDeclaredTypes()) {
			if (type instanceof AOJEnumDeclaration)
				result.add((AOJEnumDeclaration) type);
		}

		return result;
	}

	public List<AOJAnnotationTypeDeclaration> getAnnotationDecl() {
		List<AOJAnnotationTypeDeclaration> result = new ArrayList<AOJAnnotationTypeDeclaration>();
		for (AOJContainer type : getDeclaredTypes()) {
			if (type instanceof AOJAnnotationTypeDeclaration)
				result.add((AOJAnnotationTypeDeclaration) type);
		}

		return result;
	}

	public void addType(AOJContainer type) {
		getDeclaredTypes().add(type);
		// System.out.println(type.getFullQualifiedName());
		getProject().getBindingMapping().put(type.getFullQualifiedName(), type);
	}

	public AOJPackageDeclaration getPackage() {
		return pack;
	}

	public String getSource() {
		return source;
	}

	private void setSource(String source) {
		this.source = source;
	}

	public int getLineNumber(int startPosition) {
		return cUnitAST.getLineNumber(startPosition); // - 1;
	}

	public CompilationUnit getcUnitAST() {
		return cUnitAST;
	}

	public void setcUnitAST(CompilationUnit cUnitAST) {
		this.cUnitAST = cUnitAST;
	}
}
