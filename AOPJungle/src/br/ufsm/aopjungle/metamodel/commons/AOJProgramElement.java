package br.ufsm.aopjungle.metamodel.commons;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.apache.log4j.Logger;

import com.thoughtworks.xstream.annotations.XStreamOmitField;


/**
 * @author defaveri
 * Represents the top class of all Program elements plus project entity.
 * This class should be abstract. However, since we have chosen joined table strategy for hierarchy mapping 
 * we had to switch it to a concrete one.
 *
 */
@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class AOJProgramElement {
	@Id 
	//@SequenceGenerator(name = "seqGenerator", sequenceName = "SEQ_AOPJUNGLE")
    @GeneratedValue(strategy = GenerationType.TABLE /*, generator = "seqGenerator"*/)
	private long id;
	@Transient
	private String metaName;
	@XStreamOmitField
	@OneToOne (cascade=CascadeType.ALL)
	private AOJProgramElement owner;
	private String name;
	final static Logger logger = Logger.getLogger(AOJProgramElement.class);

	public AOJProgramElement() {
		
	}
	
	public AOJProgramElement(AOJProgramElement owner) {
		this.owner = owner;
	}
	
	protected void setMetaName(String name) {
		this.metaName = name;
	}

	public String getMetaName() {
		return metaName == "" ? getClass().getName() : metaName;
	}

	public AOJProgramElement getOwner() {
		return owner;
	}

	public String getName() {
		return name;
	}
	
	public void setName (String name) {
		this.name = name;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public AOJProject getProject() {
		return (AOJProject)getUpperLevelProject(this); 
	}

	public AOJPackageDeclaration getPackage() {
		return (AOJPackageDeclaration)getUpperLevelPackage(this); 
	}

	private AOJProgramElement getUpperLevelPackage(AOJProgramElement element) {
		if (element instanceof AOJPackageDeclaration)
			return element;
		return element.getOwner() instanceof AOJPackageDeclaration ? element.getOwner() : getUpperLevelPackage(element.getOwner());
	}

	
	private AOJProgramElement getUpperLevelProject(AOJProgramElement element) {
		logger.info("Getting package name " + element.getName());
		if (element instanceof AOJProject)
			return element;
		return element.getOwner() instanceof AOJProject ? element.getOwner() : getUpperLevelProject(element.getOwner());
	}
	
}
