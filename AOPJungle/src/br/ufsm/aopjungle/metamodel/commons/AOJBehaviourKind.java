package br.ufsm.aopjungle.metamodel.commons;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.MetaValue;

import br.ufsm.aopjungle.metrics.AOJBehaviorMetrics;
import br.ufsm.aopjungle.metrics.AOJMetrics;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@MappedSuperclass
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class AOJBehaviourKind extends AOJMember implements AOJClassHolder, AOJMetrics {
	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJParameter> parameters; 
	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJException> thrownExceptions; 
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "method_commoncontainer_fk")
	private AOJCommonContainer containerHelper;
    @Any(metaColumn = @Column(name = "method_type"))
    @AnyMetaDef(idType = "long", metaType = "string", metaValues = {
    @MetaValue(value = "REF", targetEntity = AOJReferencedType.class),
    @MetaValue(value = "PRI", targetEntity = AOJPrimitiveType.class) })
    @JoinColumn(name = "returntype_id")  
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
	private AOJType returnType;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "method_metrics_fk")
    private AOJBehaviorMetrics metrics;
	@Lob
	private String code;
	private String signature;
 
    // created by JJ
   // @OneToMany(cascade = CascadeType.ALL)
	private List<AOJStatement> statements;
	
	protected AOJBehaviourKind() {

	}
	
	public AOJBehaviourKind(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		containerHelper = new AOJCommonContainer();
		metrics = AOJMetricsFactory.eINSTANCE.createBehaviorMetrics();
		loadName();
		loadParameters();
		loadReturnType();
		loadThrownExceptions();
		loadCodeBody();
		loadBindings();
		loadSignature();
	}
	
	private void loadSignature() {
		StringBuilder sb = new StringBuilder();
		StringBuffer output = getModifiers().print(new StringBuffer());
		sb.append(output);
		if (null != getReturnType()) {
			sb.append(getReturnType().toString());
			sb.append(" ");
		}	
		sb.append(getName());
		sb.append("(");
		int i = 0;
		for (AOJParameter p : getParameters()) {
			sb.append(p.getType().getName());
			sb.append(" ");
			sb.append(p.getName());
			if (i++ != getParameters().size() - 1) 
				sb.append(", ");
		}	
//		if (getParameters().size() > 0) 
//			sb.delete(sb.length() - 2, sb.length() - 1);
		
		sb.append(")");
		
		if (getThrownExceptions().size() > 0)
			sb.append(" throws ");
		
		i = 0;	
		for (AOJException e : getThrownExceptions()) {
			sb.append(e.getName());
			if (i++ != getThrownExceptions().size() - 1) 
				sb.append(", ");
		}	

		setSignature(sb.toString());
	}

	private void loadBindings() {
		
	}

	protected abstract void loadName();
	protected abstract void loadParameters();
	protected abstract void loadReturnType();
	protected abstract void loadThrownExceptions();
	protected abstract void loadCodeBody();

	protected String getSignature() {
		return signature;
	}
	
	protected void setSignature(String signature) {
		this.signature = signature;
	}
	
	public void loadMetrics() {
		long nOfLines = getMetrics().getNumberOfLines();
		for (AOJContainer c : containerHelper.getAnonymousClasses())
			if (c instanceof AOJMetrics)
				nOfLines += ((AOJMetrics)c).getMetrics().getNumberOfLines();
		for (AOJContainer c : containerHelper.getInnerClasses())
			if (c instanceof AOJMetrics)
				nOfLines += ((AOJMetrics)c).getMetrics().getNumberOfLines();
		getMetrics().setNumberOfLines(nOfLines);	
		getMetrics().setNumberOfInAffects(getBindingModel().getNumberOfIn());
		getMetrics().setNumberOfOutAffects(getBindingModel().getNumberOfOut());
	}

	public AOJBehaviorMetrics getMetrics() {
		return metrics;
	}
	
	public List<AOJParameter> getParameters() {
		if (parameters == null)
			parameters = new ArrayList<AOJParameter>();
		
		return parameters;
	}
	
	
	public List<AOJException> getThrownExceptions() {
		if (thrownExceptions == null)
			thrownExceptions = new ArrayList<AOJException>();
		return thrownExceptions;		
	}	
	
	public List<AOJContainer> getInnerClasses() {
		return containerHelper.getInnerClasses();
	}

	public List<AOJContainer> getAnonymousClasses() {
		return containerHelper.getAnonymousClasses();
	}
	
	public AOJType getReturnType() {
		return returnType;
	}

	public void setReturnType (AOJType type) {
		returnType = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public List<AOJStatement> getStatements() {
		if (statements == null)
			statements = new ArrayList<AOJStatement>();
		return statements;
	}


//	protected void loadToReferenceMap() {
//		AOJReferenceManager.getInstance().addToMap(getHandler(), this);
//	}
	
}
