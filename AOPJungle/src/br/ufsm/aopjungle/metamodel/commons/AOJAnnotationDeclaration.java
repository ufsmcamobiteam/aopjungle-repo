package br.ufsm.aopjungle.metamodel.commons;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.MarkerAnnotation;

import br.ufsm.aopjungle.metrics.AOJCommonMetrics;
import br.ufsm.aopjungle.metrics.AOJMetrics;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;

@Entity
public class AOJAnnotationDeclaration extends ASTElement implements AOJMetrics {
	@OneToOne(cascade = CascadeType.ALL)
    private AOJCommonMetrics metrics;
	
	public AOJAnnotationDeclaration(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		metrics = AOJMetricsFactory.eINSTANCE.createSimpleMetrics();
		load();
	}

	protected AOJAnnotationDeclaration() {
		
	}
	
	private void load() {
		setName(((MarkerAnnotation)getNode()).getTypeName().toString());
	}
	
	public String getMetaName() {
		return "Annotation Declaration";
	}

	@Override
	public void loadMetrics() {
		metrics.setNumberOfLines(1);
	}

	@Override
	public AOJCommonMetrics getMetrics() {
		return metrics;
	}
}
