package br.ufsm.aopjungle.metamodel.commons;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.ArrayType;
import org.aspectj.org.eclipse.jdt.core.dom.ParameterizedType;
import org.aspectj.org.eclipse.jdt.core.dom.PrimitiveType;
import org.aspectj.org.eclipse.jdt.core.dom.QualifiedType;
import org.aspectj.org.eclipse.jdt.core.dom.SimpleType;
import org.aspectj.org.eclipse.jdt.core.dom.Type;
import org.aspectj.org.eclipse.jdt.core.dom.UnionType;
import org.aspectj.org.eclipse.jdt.core.dom.WildcardType;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

@MappedSuperclass
public class AOJType extends ASTElement {
	@XStreamOmitField
	@Transient
	private Type aojType = (Type) getNode();

	protected AOJType() {

	}

	public AOJType(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		setName(loadName());
	}

	public String loadName() {
		if (null == aojType)
			return "void";
		if (aojType.isPrimitiveType())
			return ((PrimitiveType)aojType).toString();
		if (aojType.isArrayType())
			return ((ArrayType)aojType).toString();
		if (aojType.isWildcardType())
			return ((WildcardType)aojType).toString();
		if (aojType.isQualifiedType())
			return ((QualifiedType)aojType).toString();
		if (aojType.isParameterizedType())
			return ((ParameterizedType)aojType).toString();
		if (aojType.isSimpleType())
			return ((SimpleType)aojType).toString();
		if (aojType.isUnionType())
			return ((UnionType)aojType).toString();
		
		return "void";
	}
	
	public String toString() {
		return getName();
	}
}
