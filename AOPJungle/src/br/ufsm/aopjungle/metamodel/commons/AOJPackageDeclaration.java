package br.ufsm.aopjungle.metamodel.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;

import br.ufsm.aopjungle.log.AOJLogger;
import br.ufsm.aopjungle.metrics.AOJContainerMetrics;
import br.ufsm.aopjungle.metrics.AOJMetrics;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@Entity
public class AOJPackageDeclaration extends AOJProgramElement implements Serializable {
	@Transient
	private static final long serialVersionUID = 1L;
	@XStreamOmitField
	@Transient
	private IPackageFragment pack;
	@XStreamOmitField
	@OneToMany(cascade=CascadeType.ALL)
	@JoinTable(
				name="PackageCompilationUnit",
				joinColumns = @JoinColumn (name = "package_id"),
				inverseJoinColumns = @JoinColumn (name = "cunit_id"))
	private List<AOJCompilationUnit> compilationUnits;
	/**
	 * Back track field for query facilities of project field 
	 */
	@OneToOne (cascade=CascadeType.ALL)
	private AOJProject project;
	@OneToOne (cascade=CascadeType.ALL)
	private AOJContainerMetrics metrics;
	@SuppressWarnings("unused")
	private AOJPackageDeclaration() {
		
	}
	
	public AOJPackageDeclaration(IPackageFragment pack, AOJProgramElement owner) {
		super(owner);
		this.pack = pack;
		if (owner instanceof AOJProject)
			project = (AOJProject)owner;
		setName(pack.getElementName());
	    compilationUnits = new ArrayList<AOJCompilationUnit>();
		setMetaName(pack.getElementName().length() == 0 ? "Anonymous" : pack.getElementName());
		metrics = AOJMetricsFactory.eINSTANCE.createPackageMetrics();

		try {
			loadCompilationUnits();
		} catch (JavaModelException e) {
			AOJLogger.getLogger().error(e);
		}
	}
	
	public void loadMetrics() {
		long nOfClasses = 0, nOfAspects = 0, nOfInterfaces = 0, nOfEnums = 0, nOfAnnDecl = 0, nOfLines = 0;
		for (AOJCompilationUnit cunit : compilationUnits) {
			nOfClasses += cunit.getClasses().size();
			nOfAspects += cunit.getAspects().size();
			nOfInterfaces += cunit.getInterfaces().size();
			nOfEnums += cunit.getEnums().size();
			nOfAnnDecl = cunit.getAnnotationDecl().size();
			for (AOJMetrics metric : cunit.getTypeMetrics())
				nOfLines += metric.getMetrics().getNumberOfLines();
		}
		metrics.setNumberOfAspects(nOfAspects);
		metrics.setNumberOfClasses(nOfClasses);
		metrics.setNumberOfInterfaces(nOfInterfaces);
		metrics.setNumberOfEnums(nOfEnums);
		metrics.setNumberOfAnnotationDecls(nOfAnnDecl);
		metrics.setNumberOfLines(nOfLines);
	}

	synchronized public List<AOJContainer> getTypes() {
		List<AOJContainer> result = new ArrayList<AOJContainer>();
		for (AOJCompilationUnit cunit : compilationUnits) {
			result.addAll(cunit.getDeclaredTypes());
		}
		return result;
	}
	
	public List<AOJCompilationUnit> getCompilationUnits() {		
		return compilationUnits;
	}
	
	private void loadCompilationUnits() throws JavaModelException {
		// TypeDeclarations are loaded by Visitors
		if (pack.getKind() == IPackageFragmentRoot.K_SOURCE) {
			for (ICompilationUnit unit : pack.getCompilationUnits()) {
				getCompilationUnits().add(new AOJCompilationUnit(unit, this));
			}
		}	
	}

	public AOJContainerMetrics getMetrics() {
		return metrics;
	}

//	public AOJProject getProject() {
//		// We got the project traversing the owner property
//		return (AOJProject)getOwner(); 
//	}

	
}
