package br.ufsm.aopjungle.metamodel.commons;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;

import br.ufsm.aopjungle.binding.AOJBindingModel;
import br.ufsm.aopjungle.binding.AOJReferenceManager;

@MappedSuperclass
public abstract class AOJMember extends ASTElement implements AOJAnnotable, AOJBindble {
	@OneToOne (cascade=CascadeType.ALL)
	private AOJModifier modifiers;
	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJAnnotationDeclaration> annotations; 
	@OneToOne (cascade=CascadeType.ALL)
	private AOJBindingModel bindingModel = new AOJBindingModel();
	private String internalHandler; 
	
	public AOJMember(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
	}

	protected AOJMember() {
		
	}
	
	protected void loadToReferenceMap() {
		AOJReferenceManager.getInstance().addToMap(getInternalHandler(), this);
	}	
	
	public AOJModifier getModifiers() {
		if (modifiers == null)
			modifiers = new AOJModifier(getModifiersAsInteger());
		return modifiers;
	}

    public List<AOJAnnotationDeclaration> getAnnotations() {
		if (null == annotations) 
			annotations = new ArrayList<AOJAnnotationDeclaration>();
		return annotations;
	}	
	
	public abstract int getModifiersAsInteger();
	
	protected void setModifiers(AOJModifier modifiers) {
		this.modifiers = modifiers;
	}

	public AOJBindingModel getBindingModel() {
		return bindingModel;
	}

	public void setBindingModel(AOJBindingModel bindingModel) {
		this.bindingModel = bindingModel;
	}

	protected abstract void loadHandler();

	public String getInternalHandler() {
		return internalHandler;
	}

	public void setInternalHandler(String handler) {
		this.internalHandler = handler;
	}
	
}
