package br.ufsm.aopjungle.metamodel.commons;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.FieldDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.IVariableBinding;
import org.aspectj.org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.MetaValue;

import br.ufsm.aopjungle.metrics.AOJCommonMetrics;
import br.ufsm.aopjungle.metrics.AOJMetrics;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@Entity
//@ForeignKey(name="attributedeclaration_fk")
@DiscriminatorValue("ATR")
public class AOJAttributeDeclaration extends AOJMember implements AOJClassHolder, AOJMetrics  {
	@XStreamOmitField
	@Transient
	private FieldDeclaration fieldDeclaration = (FieldDeclaration) getNode();
    @Any(metaColumn = @Column(name = "attribute_type"))
    @AnyMetaDef(idType = "long", metaType = "string", metaValues = {
    @MetaValue(value = "REF", targetEntity = AOJReferencedType.class),
    @MetaValue(value = "PRI", targetEntity = AOJPrimitiveType.class) })
    @JoinColumn(name = "type_id")  
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
	private AOJType type;
	private String initValue;
	@OneToOne (cascade=CascadeType.ALL)
//	@JoinColumn(name = "method_commoncontainer_fk")
	private AOJCommonContainer containerHelper;
	@OneToOne (cascade=CascadeType.ALL)
	private AOJCommonMetrics metrics;
	@Transient 
	private IVariableBinding binding;

	protected FieldDeclaration getAttributeDeclaration() {
		return fieldDeclaration;
	}
	
	public String getMetaName() {
		return "Attribute";
	}

	public AOJType getType() {
		return type;		
	}
	
	protected AOJAttributeDeclaration() {

	}
	
	public AOJAttributeDeclaration(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		containerHelper = new AOJCommonContainer();
		metrics = AOJMetricsFactory.eINSTANCE.createSimpleMetrics();
		load();
	}
	
	public void load() {
		loadType();
		loadName();
		loadInitValue();
		loadOnType();
		loadPosition();
		loadHandler();
	}

	protected void loadOnType(){}
	protected void loadPosition(){}
	
	private void loadInitValue() {
	}

	private void loadName() {
        String fieldName = null;
        @SuppressWarnings("unchecked")
		List<VariableDeclarationFragment> fragments = getAttributeDeclaration().fragments();
        if (fragments.size() == 1) {
            VariableDeclarationFragment frag = fragments.get(0);
            fieldName = frag.getName().getFullyQualifiedName();
            setBinding(frag.resolveBinding());
        }	
        
		setName(fieldName);		
	}
	
	private void loadType() {
		if (getAttributeDeclaration().getType().isPrimitiveType())
			this.type = new AOJPrimitiveType(getAttributeDeclaration().getType(), this);
		else
			this.type = new AOJReferencedType(getAttributeDeclaration().getType(), this);		
	}

	public String getInitValue() {
		return initValue;
	}

	@Override
	public int getModifiersAsInteger() {
		return getAttributeDeclaration().getModifiers();
	}

	@Override
	public List<AOJContainer> getInnerClasses() {
		return containerHelper.getInnerClasses();
	}

	@Override
	public List<AOJContainer> getAnonymousClasses() {
		return containerHelper.getAnonymousClasses();
	}

	public IVariableBinding getBinding() {
		return binding;
	}

	private void setBinding(IVariableBinding binding) {
		this.binding = binding;
	}

	@Override
	public void loadMetrics() {
		getMetrics().setNumberOfLines(1);
		getMetrics().setNumberOfInAffects(getBindingModel().getNumberOfIn());
		getMetrics().setNumberOfOutAffects(getBindingModel().getNumberOfOut());
	}

	@Override
	public AOJCommonMetrics getMetrics() {
		return metrics;
	}

	protected void loadHandler() {
	}
}
