package br.ufsm.aopjungle.metamodel.commons;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ManyToAny;
import org.hibernate.annotations.MetaValue;

import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@Entity
public class AOJCommonContainer implements AOJClassHolder {
	@Id
	@GeneratedValue
	private long id;
    @ManyToAny(
            metaColumn = @Column( name = "anonymousclass_type" )
        )
        @AnyMetaDef(
            idType = "long", metaType = "string",
            metaValues = {
                @MetaValue( targetEntity = AOJClassDeclaration.class, value="CLA" ),
                @MetaValue( targetEntity = AOJInterfaceDeclaration.class, value="INT" ),
                @MetaValue( targetEntity = AOJAspectDeclaration.class, value="ASP" ),
            }
        )
        @Cascade( { org.hibernate.annotations.CascadeType.ALL } )
        @JoinTable(
            name = "anonymousclass_type_table", 
            joinColumns = @JoinColumn( name = "anonymousclass_id" ),
            inverseJoinColumns = @JoinColumn( name = "typeanonymous_id" )
        )
    private List<AOJContainer> anonymousClasses;
    @ManyToAny(
            metaColumn = @Column( name = "innerclass_type" )
        )
        @AnyMetaDef(
            idType = "long", metaType = "string",
            metaValues = {
                @MetaValue( targetEntity = AOJClassDeclaration.class, value="CLA" ),
                @MetaValue( targetEntity = AOJInterfaceDeclaration.class, value="INT" ),
                @MetaValue( targetEntity = AOJAspectDeclaration.class, value="ASP" ),
            }
        )
        @Cascade( { org.hibernate.annotations.CascadeType.ALL } )
        @JoinTable(
            name = "innerclass_type_table", 
            joinColumns = @JoinColumn( name = "innerclass_type_id" ),
            inverseJoinColumns = @JoinColumn( name = "typeinner_id" )
        )
    private List<AOJContainer> innerClasses;
	public List<AOJContainer> getAnonymousClasses() {
		if (null == anonymousClasses) 
			anonymousClasses = new ArrayList<AOJContainer>();

		return anonymousClasses;
	}
	public List<AOJContainer> getInnerClasses() {
		if (null == innerClasses)
			innerClasses = new ArrayList<AOJContainer>();
		return innerClasses;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}
