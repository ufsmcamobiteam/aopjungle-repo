package br.ufsm.aopjungle.metamodel.commons;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ManyToAny;
import org.hibernate.annotations.MetaValue;

import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutCFlow;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJConstructorDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration;
import br.ufsm.aopjungle.metrics.AOJMetrics;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br> 
 */
@Entity
public class AOJConcreteTypeMember extends AOJCommonTypeMember implements AOJClassHolder {
	@OneToMany(cascade = CascadeType.ALL)
	protected List<AOJPointcutDeclaration> pointcuts;
	@OneToMany(cascade = CascadeType.ALL)
	private List<AOJPointcutCFlow> cFlowPointcuts;
    @ManyToAny(
            metaColumn = @Column( name = "anonymousclass_type" )
        )
        @AnyMetaDef(
            idType = "long", metaType = "string",
            metaValues = {
                @MetaValue( targetEntity = AOJClassDeclaration.class, value="CLA" ),
                @MetaValue( targetEntity = AOJInterfaceDeclaration.class, value="INT" ),
                @MetaValue( targetEntity = AOJAspectDeclaration.class, value="ASP" ),
            }
        )
        @Cascade( { org.hibernate.annotations.CascadeType.ALL } )
        @JoinTable(
            name = "concretetype_anonymousclass_table", 
            joinColumns = @JoinColumn( name = "concretetype_anonymous_id" ),
            inverseJoinColumns = @JoinColumn( name = "anonymous_class_id" )
        )
	protected List<AOJContainer> anonymousClasses;
    @ManyToAny(
            metaColumn = @Column( name = "innerclass_type" )
        )
        @AnyMetaDef(
            idType = "long", metaType = "string",
            metaValues = {
                @MetaValue( targetEntity = AOJClassDeclaration.class, value="CLA" ),
                @MetaValue( targetEntity = AOJInterfaceDeclaration.class, value="INT" ),
                @MetaValue( targetEntity = AOJAspectDeclaration.class, value="ASP" ),
            }
        )
        @Cascade( { org.hibernate.annotations.CascadeType.ALL } )
        @JoinTable(
            name = "concretetype_innerclass_table", 
            joinColumns = @JoinColumn( name = "concretetype_inner_id" ),
            inverseJoinColumns = @JoinColumn( name = "inner_class_id" )
        )
	protected List<AOJContainer> innerClasses;
    @OneToMany(cascade = CascadeType.ALL)
	private List<AOJConstructorDeclaration> constructors;
    
	public AOJConcreteTypeMember() {
		
	}
	public List<AOJPointcutCFlow> getcFlowPointcuts() {
		if (cFlowPointcuts == null)
			cFlowPointcuts = new ArrayList<AOJPointcutCFlow>();
		return cFlowPointcuts;
	}

	public List<AOJContainer> getInnerClasses() {
		if (innerClasses == null) {
			innerClasses = new ArrayList<AOJContainer>();
		}
		
		return innerClasses;
	}

	public List<AOJPointcutDeclaration> getPointcuts() {
		if (pointcuts == null)
			pointcuts = new ArrayList<AOJPointcutDeclaration>();
		return pointcuts;
	}
	
	public void setPointcuts(List<AOJPointcutDeclaration> pointcuts) {
		this.pointcuts = pointcuts;
	}	

	public List<AOJContainer> getAnonymousClasses() {
		if (anonymousClasses == null) {
			anonymousClasses = new ArrayList<AOJContainer>();
		}
		
		return anonymousClasses;
	}

	public List<AOJConstructorDeclaration> getConstructors() {
		if (constructors == null)
			constructors = new ArrayList<AOJConstructorDeclaration>();
		return constructors;
	}
	
	
//	public List<AOJStatement> getStatements() {
//		if (statements == null)
//			statements = new ArrayList<AOJStatement>();
//		return statements;
//	}
	
	@Override
	public void getMetricParticipants(List<AOJMetrics> members) {
		super.getMetricParticipants(members);
		for (AOJPointcutDeclaration member : getPointcuts()) 
			if (member instanceof AOJMetrics)
				members.add(member);

		for (AOJConstructorDeclaration member : getConstructors()) 
			if (member instanceof AOJMetrics)
				members.add(member);
		
		for (AOJContainer member : getInnerClasses()) 
			if (member instanceof AOJMetrics)
				members.add((AOJMetrics)member);
		
		for (AOJContainer member : getAnonymousClasses()) 
			if (member instanceof AOJMetrics)
				members.add((AOJMetrics)member);
	}

	
	
}
