package br.ufsm.aopjungle.metamodel.commons;

public abstract class AOJExpression extends AOJProgramElement{
	
	private String expressionCode;
	
	public AOJExpression(AOJProgramElement owner){
		super(owner);
	}

	public AOJExpression(AOJProgramElement owner, String expressionCode){
		super(owner);
		this.expressionCode = expressionCode;
	}
	
	public String getExpressionCode() {
		return expressionCode;
	}

	public void setExpressionCode(String expressionCode) {
		this.expressionCode = expressionCode;
	}
	

	
}



