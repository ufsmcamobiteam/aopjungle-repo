package br.ufsm.aopjungle.metamodel.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.aspectj.asm.IProgramElement;
import org.aspectj.org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.ufsm.aopjungle.binding.AOJInheritanceResolver;
import br.ufsm.aopjungle.binding.AOJProgressMonitor;
import br.ufsm.aopjungle.labs.AOJBindingMapping;
import br.ufsm.aopjungle.log.AOJLogger;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration;
import br.ufsm.aopjungle.metrics.AOJContainerMetrics;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@Entity
public class AOJProject extends AOJProgramElement implements Serializable {
	@XStreamOmitField
	private static final long serialVersionUID = 1L;
	@XStreamOmitField
	@Transient
	private IProject resourceProject;
	@XStreamOmitField
	private String relativePath;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "ProjectPackages", joinColumns = @JoinColumn(name = "project_id"), inverseJoinColumns = @JoinColumn(name = "package_id"))
	private List<AOJPackageDeclaration> packages;
	@Transient
	private AOJInheritanceResolver inheritanceResolver;
	@Transient
	private AOJBindingMapping bindingMapping;
	@OneToOne(cascade = CascadeType.ALL)
	private AOJContainerMetrics metrics;
	@XStreamOmitField
	@Transient
	private AOJProgressMonitor buildMonitor;
	@SuppressWarnings("unused")
	private AOJProject() {

	}

	public AOJTypeDeclaration searchType (IProgramElement programElement) {
		return null;
	}
	
	public AOJProject(IProject resourceProject) {
		super(null);
		try {
//			ChangeLoader.change(resourceProject);
			setProject(resourceProject);
			setName(resourceProject.getName());
			setMetaName("Project");
			setRelativePath(resourceProject.getProjectRelativePath().toString());
			bindingMapping = new AOJBindingMapping();
			metrics = AOJMetricsFactory.eINSTANCE.createProjectMetrics();
			loadPackages();
//			loadInheritanceMetrics();
		} catch (JavaModelException e) {
			AOJLogger.getLogger().error(e);
		} finally {
//			ChangeLoader.unChange();	
		}
		
	}

	public void loadMetrics() {
		long nOfClasses = 0, nOfAspects = 0, nOfInterfaces = 0, 
				nOfEnums = 0, nOfAnnDecls = 0, nOfSubContainers = 0, nOfLines = 0;
		nOfSubContainers += getPackages().size();
		for (AOJPackageDeclaration pack : getPackages()) {
			nOfClasses += pack.getMetrics().getNumberOfClasses();
			nOfAspects += pack.getMetrics().getNumberOfAspects();
			nOfInterfaces += pack.getMetrics().getNumberOfInterfaces();
			nOfEnums += pack.getMetrics().getNumberOfEnums();
			nOfAnnDecls += pack.getMetrics().getNumberOfAnnotationDecls();
			nOfLines += pack.getMetrics().getNumberOfLines();
//			AOJLogger.getLogger().debug("project : " + getName() + "(" + getPackages().size() + " pkts) : Package -> " + pack.getName() + " LOC : " + pack.getMetrics().getNumberOfLines());
		}

		metrics.setNumberOfAspects(nOfAspects);
		metrics.setNumberOfClasses(nOfClasses);
		metrics.setNumberOfInterfaces(nOfInterfaces);
		metrics.setNumberOfEnums(nOfEnums);
		metrics.setNumberOfAnnotationDecls(nOfAnnDecls);
		metrics.setNumberOfSubContainers(nOfSubContainers);
		metrics.setNumberOfLines(nOfLines);
	}

	/**
	 * Try to resolve AOJungle supertype based on parent field
	 */
	private void loadInheritanceMetrics() {
		AOJInheritanceResolver resolver = new AOJInheritanceResolver(this);
		resolver.resolve();
	}

	private void setProject(IProject resourceProject) {
		this.resourceProject = resourceProject;
	}

	public IProject getResourceProject() {
		return resourceProject;
	}

	public void setName(String name) {
		super.setName(name);
	}

	public void setPackages(List<AOJPackageDeclaration> packages) {
		this.packages = packages;
	}

	public List<AOJPackageDeclaration> getPackages() {
		return packages;
	}

	public AOJPackageDeclaration getPackage(String packName) {
		for (AOJPackageDeclaration pack : packages) // It could be a map to speed up searching
			if (pack.getName().equals(packName))
				return pack;
		return null;
	}
	
	private void loadPackages() throws JavaModelException {
		// No synch with updates... improvements for the future. Be my guest :)
		// TODO: Filter only project packages
		// Use IJavaProject, IPackageFragment of jdt package, since ajdt has
		// been bugged on this part.
		packages = new ArrayList<AOJPackageDeclaration>();
		IJavaProject javaProject = JavaCore.create(resourceProject);
		IPackageFragment[] packagesNode = javaProject.getPackageFragments();
		for (IPackageFragment pack : packagesNode) {
			if ((pack.getKind() == IPackageFragmentRoot.K_SOURCE)
					&& (pack.containsJavaResources()))
				packages.add(new AOJPackageDeclaration(pack, this));
		}
	}

	public String getRelativePath() {
		return relativePath;
	}

	private void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}

	public AOJInheritanceResolver getInheritanceResolver() {
		return inheritanceResolver;
	}

	public AOJBindingMapping getBindingMapping() {
		return bindingMapping;
	}

	public List<AOJAspectDeclaration> getAspects() {
		List<AOJAspectDeclaration> result = new ArrayList<AOJAspectDeclaration>();
		for (AOJPackageDeclaration pack : getPackages())
			for (AOJCompilationUnit cUnit : pack.getCompilationUnits()) {
				result.addAll(cUnit.getAspects());
			}
		return result;
	}

	public List<AOJClassDeclaration> getClasses() {
		List<AOJClassDeclaration> result = new ArrayList<AOJClassDeclaration>();
		for (AOJPackageDeclaration pack : getPackages())
			for (AOJCompilationUnit cUnit : pack.getCompilationUnits()) {
				result.addAll(cUnit.getClasses());
			}
		return result;
	}

	public List<AOJInterfaceDeclaration> getInterface() {
		List<AOJInterfaceDeclaration> result = new ArrayList<AOJInterfaceDeclaration>();
		for (AOJPackageDeclaration pack : getPackages())
			for (AOJCompilationUnit cUnit : pack.getCompilationUnits()) {
				result.addAll(cUnit.getInterfaces());
			}
		return result;
	}

	public List<AOJContainer> getTypes() {
		List<AOJContainer> result = new ArrayList<AOJContainer>();
		for (AOJPackageDeclaration pack : getPackages())
			result.addAll(pack.getTypes());
		return result;
	}

	public AOJContainerMetrics getMetrics() {
		return metrics;
	}

	public void build() {
		try {
			buildMonitor = new AOJProgressMonitor();
			resourceProject.build(IncrementalProjectBuilder.FULL_BUILD, buildMonitor);
		} catch (CoreException e) {
			e.printStackTrace();
		}			
	}

	public AOJProgressMonitor getBuildMonitor() {
		return buildMonitor;
	}

	public void setBuildMonitor(AOJProgressMonitor buildMonitor) {
		this.buildMonitor = buildMonitor;
	}
}
