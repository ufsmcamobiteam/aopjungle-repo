package br.ufsm.aopjungle.metamodel.commons;

import java.util.List;

public interface AOJInterfaceble {
	public List<AOJReferencedType> getImplementedInterfaces();
}
