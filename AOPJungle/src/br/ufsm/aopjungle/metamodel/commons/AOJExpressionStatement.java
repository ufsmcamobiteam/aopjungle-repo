package br.ufsm.aopjungle.metamodel.commons;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.ConditionalExpression;
import org.aspectj.org.eclipse.jdt.core.dom.ExpressionStatement;
import org.aspectj.org.eclipse.jdt.core.dom.MethodInvocation;

public class AOJExpressionStatement extends AOJStatement implements AOJClassHolder{

	private AOJExpression aojExpression;
	
	private List<AOJContainer> innerClasses;
	
	private List<AOJContainer> anonymousClasses;
	
	public AOJExpressionStatement(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		ExpressionStatement e = (ExpressionStatement)node;
		
		if (e.getExpression() instanceof MethodInvocation)
			this.aojExpression = new AOJMethodInvocation(owner, e.getExpression());
		else if (e.getExpression() instanceof ConditionalExpression)
			this.aojExpression = new AOJConditionalExpression(owner, e.getExpression());
		else // default expression
			this.aojExpression = new AOJExpression(owner) {	};
			
	}

	public AOJExpression getAojExpression() {
		return aojExpression;
	}

	public void setAojExpression(AOJExpression aojExpression) {
		this.aojExpression = aojExpression;
	}

	@Override
	public List<AOJContainer> getInnerClasses() {
		if (innerClasses == null)
			innerClasses = new ArrayList<AOJContainer>();
		return innerClasses;
	}

	@Override
	public List<AOJContainer> getAnonymousClasses() {
		if (anonymousClasses == null) 
			anonymousClasses = new ArrayList<AOJContainer>();
		return anonymousClasses;
	}

}



