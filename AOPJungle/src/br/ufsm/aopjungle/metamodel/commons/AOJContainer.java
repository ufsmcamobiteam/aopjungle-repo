package br.ufsm.aopjungle.metamodel.commons;

import javax.persistence.MappedSuperclass;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;

import br.ufsm.aopjungle.metrics.AOJTypeMetrics;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@MappedSuperclass
public interface AOJContainer {
	public AOJProgramElement getOwner();
	public String getMetaName();
	public ASTNode getNode();
	public long getId();
	public String getName();
	public String getFullQualifiedName();
	public String getInternalHandler();
	public AOJReferencedType getParent();
	public AOJContainer getSuperType();
	public void setSuperType(AOJContainer type);
	public AOJModifier getModifiers();
	public AOJCommonTypeMember getMembers();
	public AOJTypeMetrics getMetrics();
}
