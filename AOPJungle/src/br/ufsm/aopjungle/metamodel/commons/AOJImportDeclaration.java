package br.ufsm.aopjungle.metamodel.commons;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AOJImportDeclaration  {
	@Id 
    @GeneratedValue(strategy = GenerationType.TABLE)	
	private long id;
	private String code;

	public AOJImportDeclaration() {}
	
	public AOJImportDeclaration(String code) {
		this.setCode(code);
	}

	public String getCode() {
		return code;
	}

	protected void setCode(String code) {
		this.code = code;
	}
	
	public String qualifiedName() {
		return getCode().substring(getCode().indexOf(" ") + 1, getCode().length() - 1);		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
