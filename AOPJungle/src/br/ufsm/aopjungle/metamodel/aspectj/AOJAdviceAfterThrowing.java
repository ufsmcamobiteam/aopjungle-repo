package br.ufsm.aopjungle.metamodel.aspectj;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;

import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;

@Deprecated
//@Entity
//@ForeignKey(name="adviceafterthrowing_fk")
//@DiscriminatorValue("AT")
public class AOJAdviceAfterThrowing extends AOJAdviceDeclaration {

	protected AOJAdviceAfterThrowing() {

	}
	
	public AOJAdviceAfterThrowing(ASTNode node, AOJProgramElement owner) {
		super(node, owner, AOJAdviceKindEnum.AFTERTHROWING);
	}

}
