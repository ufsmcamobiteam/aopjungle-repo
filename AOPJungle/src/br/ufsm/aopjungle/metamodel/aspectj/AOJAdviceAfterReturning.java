package br.ufsm.aopjungle.metamodel.aspectj;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;

import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
@Deprecated
//@Entity
//@ForeignKey(name="adviceafterreturning_fk")
//@DiscriminatorValue("AR")
public class AOJAdviceAfterReturning extends AOJAdviceDeclaration {

	protected AOJAdviceAfterReturning() {

	}
	
	public AOJAdviceAfterReturning(ASTNode node, AOJProgramElement owner) {
		super(node, owner, AOJAdviceKindEnum.AFTERRETURNING);
	}

}
