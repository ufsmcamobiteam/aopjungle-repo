package br.ufsm.aopjungle.metamodel.aspectj;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.AdviceDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.AroundAdviceDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.Name;
import org.aspectj.org.eclipse.jdt.core.dom.SingleVariableDeclaration;

import br.ufsm.aopjungle.metamodel.commons.AOJBehaviourKind;
import br.ufsm.aopjungle.metamodel.commons.AOJException;
import br.ufsm.aopjungle.metamodel.commons.AOJParameter;
import br.ufsm.aopjungle.metamodel.commons.AOJPrimitiveType;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJReferencedType;
import br.ufsm.aopjungle.metamodel.commons.AOJType;
import br.ufsm.aopjungle.metrics.AOJMetrics;
import br.ufsm.aopjungle.util.AOJSourceService;
import br.ufsm.aopjungle.util.StringUtil;

/**
 * @author Cristiano De Faveri
 * Federal University of Santa Maria
 * Programming Languages and Database Research Group 
 */

@Entity
@DiscriminatorValue("ADV")
public class AOJAdviceDeclaration extends AOJBehaviourKind {
	public static int seed = 0;
	@Enumerated(EnumType.STRING)
	private AOJAdviceKindEnum kind;
	@OneToOne(cascade=CascadeType.ALL)
	private AOJPointcutExpression pointcutExpression;
	private int position;
	
	public String getMetaName() {
		return "Advice."+kind.toString();
	}

	protected AOJAdviceDeclaration() {

	}

	protected void loadName() {
		setName(""); // Advices have no nome (except when working as annotated ones)
	}
	
	public AOJAdviceDeclaration(ASTNode node, AOJProgramElement owner, AOJAdviceKindEnum kind) {
		super(node, owner);	
		this.kind = kind;
		loadPointcutExpression();
		loadPosition();
		loadHandler();
		loadToReferenceMap();
	}
	
	protected void loadPosition() {
		setPosition(nextPosition());
	}

	private int nextPosition() {
		return ++seed;
	}

	protected void loadCodeBody() {
		setCode(getAdviceDeclaration().getBody().toString());
	}
	
	private void loadPointcutExpression() {
		pointcutExpression = new AOJPointcutExpression(getAdviceDeclaration().getPointcut());
	}

	protected void loadThrownExceptions() {
		for (Object exception : getAdviceDeclaration().thrownExceptions()) {
			getThrownExceptions().add( new AOJException((Name) exception, this));
		}
	}

	protected void loadParameters() {
		for (Object param : getAdviceDeclaration().parameters()) 
			getParameters().add( new AOJParameter((SingleVariableDeclaration) param, this));
	}
	
	protected void loadReturnType() {
		AOJType result;
		if (! (getAdviceDeclaration() instanceof AroundAdviceDeclaration))
			result = null; // No return if advice is not around
		else {
			AroundAdviceDeclaration around = (AroundAdviceDeclaration)getAdviceDeclaration();
			if (null == around.getReturnType2()) 
				result = new AOJReferencedType(null, this);
			else 
			if (around.getReturnType2().isPrimitiveType())
				result = new AOJPrimitiveType(around.getReturnType2(), this);
			else
				result = new AOJReferencedType(around.getReturnType2(), this);
		}	
		setReturnType(result);
	}	
	
	protected void setKind(AOJAdviceKindEnum kind) {
		this.kind = kind;
	}

	public AOJAdviceKindEnum getKind() {
		return kind;
	}

	public AOJPointcutExpression getPointcut() {
		return pointcutExpression;
	}

	@Override
	public int getModifiersAsInteger() {
		return getAdviceDeclaration().getModifiers();
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	private AdviceDeclaration getAdviceDeclaration() {
		ASTNode node = getNode();
		return (AdviceDeclaration)node;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void loadMetrics() {
		super.loadMetrics();
		getMetrics().setNumberOfStatements(getAdviceDeclaration().getBody() == null ? 0 : 
			AOJSourceService.getNumberOfStatements(getAdviceDeclaration().getBody().statements()));
		
		long nOfLines = 0;
		nOfLines = getAdviceDeclaration().getBody() == null ? 0 : 
			AOJSourceService.getNumberOfStatements(getAdviceDeclaration().getBody().statements());
		nOfLines += 3; // Add open/close curly brackets + header		
		getMetrics().setNumberOfLines(nOfLines);

		getMetrics().setNumberOfInAffects(getNumberOfInBindings());
		getMetrics().setNumberOfOutAffects(getNumberOfOutBindings());
	}

	public long getNumberOfOutBindings() {
		return getBindingModel().getNumberOfOut();
	}

	public long getNumberOfInBindings() {
		return getBindingModel().getNumberOfIn();
	}
	
	@Override
	protected void loadHandler() {
		setInternalHandler(StringUtil.buildHandlerAdvice(getKind(), getPackage().getName(), getOwner().getName(), getPosition()));
	}
}
