package br.ufsm.aopjungle.metamodel.aspectj;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.AroundAdviceDeclaration;

import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Deprecated
//@Entity
//@ForeignKey(name="advicearound_fk")
//@DiscriminatorValue("AA")
public class AOJAdviceAround extends AOJAdviceDeclaration {
	@XStreamOmitField
	@Transient
	private AroundAdviceDeclaration advice = (AroundAdviceDeclaration)getNode();

	protected AOJAdviceAround() {
		
	}
	
	public AOJAdviceAround(ASTNode node, AOJProgramElement owner) {
		super(node, owner, AOJAdviceKindEnum.AROUND);
	}

}