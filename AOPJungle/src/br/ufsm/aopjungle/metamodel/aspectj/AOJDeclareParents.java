package br.ufsm.aopjungle.metamodel.aspectj;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.DeclareParentsDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.TypePattern;

import br.ufsm.aopjungle.metamodel.commons.AOJMember;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.metrics.AOJCommonMetrics;
import br.ufsm.aopjungle.metrics.AOJMetrics;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;
import br.ufsm.aopjungle.util.StringUtil;

import com.thoughtworks.xstream.annotations.XStreamOmitField;
/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@Entity
@DiscriminatorValue("DEP")
public class AOJDeclareParents extends AOJMember implements AOJMetrics {
	@XStreamOmitField
	@Transient
	private DeclareParentsDeclaration declareParentsNode = ((DeclareParentsDeclaration)getNode());  
	private boolean isExtends;
	@Enumerated(EnumType.STRING)
	private AOJDeclareParentKindEnum kind;
	private String childType;
	@ElementCollection
	private List<String> parentType;	
	@OneToOne (cascade=CascadeType.ALL)
	private AOJCommonMetrics metrics; 
	public static int seed = 0;
	private int position;

	protected AOJDeclareParents() {
	
	}
	
	public AOJDeclareParents(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		parentType = new ArrayList<String>();	
		metrics = AOJMetricsFactory.eINSTANCE.createSimpleMetrics();
		load();
	}
	
	protected void loadPosition() {
		setPosition(nextPosition());
	}

	private int nextPosition() {
		return ++seed;
	}			

	@SuppressWarnings("unchecked")
	private void load() {
		setExtends(declareParentsNode.isExtends());
		if (isExtends)
			setKind(AOJDeclareParentKindEnum.ENTENDS);
		else
			setKind(AOJDeclareParentKindEnum.IMPLEMENTS);
		setChildType(declareParentsNode.getChildTypePattern().getTypePatternExpression());
		setParentType(declareParentsNode.parentTypePatterns());
		loadPosition();
		loadHandler();
		loadToReferenceMap();
	}
	
	@Override
	public int getModifiersAsInteger() {
		return declareParentsNode.getModifiers();
	}

	public boolean isExtends() {
		return isExtends;
	}
	
	public TypePattern getChildType() {
		return declareParentsNode.getChildTypePattern();
	}
	
	
	public String getChildTypeAsString() {
		return childType;
	}
	
	@SuppressWarnings("unchecked")
	public List<TypePattern> getParentType() {
		return declareParentsNode.parentTypePatterns();
	}

	public List<String> getParentTypeAsString() {
		return parentType;
	}
	
	public void setExtends(boolean isExtends) {
		this.isExtends = isExtends;
	}

	public void setChildType(String childType) {
		this.childType = childType;
	}

	public void setParentType(List<TypePattern> lParentType) {
		for (TypePattern type : lParentType)
			parentType.add(type.getTypePatternExpression());
	}

	public AOJDeclareParentKindEnum getKind() {
		return kind;
	}

	public void setKind(AOJDeclareParentKindEnum kind) {
		this.kind = kind;
	}

	@Override
	public void loadMetrics() {
		getMetrics().setNumberOfLines(1);
		getMetrics().setNumberOfInAffects(getBindingModel().getNumberOfIn());
		getMetrics().setNumberOfOutAffects(getBindingModel().getNumberOfOut());
	}

	@Override
	public AOJCommonMetrics getMetrics() {
		return metrics;
	}

	@Override
	protected void loadHandler() {
		setInternalHandler(StringUtil.buildDeclareParentHandler(((AOJTypeDeclaration)getOwner()).getFullQualifiedName(), position));
		System.out.println("Declare Parent handler = " + getInternalHandler());
	}
	
	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}		
}
