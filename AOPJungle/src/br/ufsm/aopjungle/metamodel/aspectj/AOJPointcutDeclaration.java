package br.ufsm.aopjungle.metamodel.aspectj;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.PointcutDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.SingleVariableDeclaration;

import br.ufsm.aopjungle.metamodel.commons.AOJMember;
import br.ufsm.aopjungle.metamodel.commons.AOJParameter;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.metrics.AOJCommonMetrics;
import br.ufsm.aopjungle.metrics.AOJMetrics;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@Entity
@DiscriminatorValue("PCD")
public class AOJPointcutDeclaration extends AOJMember implements AOJMetrics {
	@XStreamOmitField
	@Transient
	private PointcutDeclaration pointcutDeclaration = (PointcutDeclaration)getNode();
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="pointcut_fk")
	@Transient
	private AOJPointcutExpression pointcutExpression;
	@OneToMany(cascade=CascadeType.ALL)
	@JoinTable(
				name="pointcut_parameters",
				joinColumns = @JoinColumn (name = "pointcut_id"),
				inverseJoinColumns = @JoinColumn (name = "parameter_id"))
	private List<AOJParameter> parameters;
	private String fullQualifiedName;
	@Lob
	private String code; // replicated with expression for query easy purposes
	private boolean isAnonymous; // replicated with expression for query easy purposes
	@Enumerated(EnumType.STRING) 
	private AOJPointcutTypeEnum type; // replicated with expression for query easy purposes
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="pcutprimitives_fk")
	private List<AOJPointcutPrimitive> primitives;
	@OneToOne (cascade=CascadeType.ALL)
	private AOJCommonMetrics metrics; 

	@Override
	public String getMetaName() {
		return "Pointcut";
	};
	
	public String getName() {
		return pointcutDeclaration.getName().toString();
	};
	
	public List<AOJParameter> getParameters() {
		return parameters;
	}

	protected AOJPointcutDeclaration() {
	
	}

	public AOJPointcutDeclaration(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		pointcutExpression = new AOJPointcutExpression(pointcutDeclaration.getDesignator(), this);
		metrics = AOJMetricsFactory.eINSTANCE.createSimpleMetrics();
		parameters = new ArrayList<AOJParameter>();
		for (Object param : pointcutDeclaration.parameters()) {
			parameters.add(new AOJParameter((SingleVariableDeclaration)param, this));
		}
		load();
	}
	
	public void setParameters(List<AOJParameter> parameters) {
		this.parameters = parameters;
	}

	public AOJPointcutExpression getPointcutExpression() {
		return pointcutExpression;
	}
	
	@Override
	public int getModifiersAsInteger() {
		return pointcutDeclaration.getModifiers();
	}

	private void load() {
		setName(pointcutDeclaration.getName().toString());
		if (getOwner() instanceof AOJTypeDeclaration)
			setFullQualifiedName(((AOJTypeDeclaration)getOwner()).getFullQualifiedName() + "." + getName());
		else
			setFullQualifiedName(getName());
		setCode(pointcutExpression.getCode());
		setAnonymousPointcut(pointcutExpression.isAnonymous());
		setType(pointcutExpression.getType());
		setPrimitives(pointcutExpression.getPrimitives());
	}
	
	private void setPrimitives (List<AOJPointcutPrimitive> pointcutPrimitives) {
		this.primitives = pointcutPrimitives;
	}
	
	private void setAnonymousPointcut(boolean isAnonymousPointcut) {
		this.isAnonymous = isAnonymousPointcut;
	}

	private void setCode(String code) {
		this.code = code;
	}
	
	private void setType(AOJPointcutTypeEnum type) {
		this.type = type;
	}

	@Override
	public void loadMetrics() {
		metrics.setNumberOfLines(1);
	}

	@Override
	public AOJCommonMetrics getMetrics() {
		return metrics;
	}

	public String getFullQualifiedName() {
		return fullQualifiedName;
	}

	public void setFullQualifiedName(String fullQualifiedName) {
		this.fullQualifiedName = fullQualifiedName;
	}

	protected void loadHandler() {
	}
}
