package br.ufsm.aopjungle.metamodel.aspectj;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.DeclarePrecedenceDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.TypePattern;

import br.ufsm.aopjungle.metamodel.commons.AOJMember;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metrics.AOJCommonMetrics;
import br.ufsm.aopjungle.metrics.AOJMetrics;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
public class AOJDeclarePrecedence extends AOJMember implements AOJMetrics {
	@XStreamOmitField
	@Transient
	private DeclarePrecedenceDeclaration precedenceNode = ((DeclarePrecedenceDeclaration)getNode());  
	@ElementCollection
	private List<String> expressions;
	@OneToOne (cascade=CascadeType.ALL)
	private AOJCommonMetrics metrics; 
	public static int seed = 0;
	private int position;
	
	protected AOJDeclarePrecedence() {
	
	}
	
	public AOJDeclarePrecedence(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		expressions = new ArrayList<String>();
		metrics = AOJMetricsFactory.eINSTANCE.createSimpleMetrics();
		load();
	}

	public void load() {
		loadExpressions();
		loadPosition();
		loadHandler();		
	}
	
	protected void loadPosition() {
		setPosition(nextPosition());
	}

	private int nextPosition() {
		return ++seed;
	}		
	public void loadExpressions() {
		expressions.clear();
		for (@SuppressWarnings("rawtypes")
		Iterator it = precedenceNode.typePatterns().iterator(); it.hasNext();) {
			TypePattern typePat = (TypePattern) it.next();
			expressions.add(typePat.getTypePatternExpression());
		}
	}
	
	@Override
	public int getModifiersAsInteger() {
		return precedenceNode.getModifiers();
	}
	
	public List<String> getExpressions() {
		return expressions;
	}
	
	public String toString() {
		String result = "";
		for (String s : expressions) 
			result += " "+s;
		return result;
	}

	@Override
	public void loadMetrics() {
		metrics.setNumberOfLines(1);
	}

	@Override
	public AOJCommonMetrics getMetrics() {
		return metrics;
	}

	protected void loadHandler() {
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
	
}
