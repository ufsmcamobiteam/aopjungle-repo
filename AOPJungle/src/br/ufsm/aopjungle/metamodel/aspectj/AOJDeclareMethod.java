package br.ufsm.aopjungle.metamodel.aspectj;

import javax.persistence.Entity;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.InterTypeMethodDeclaration;

import br.ufsm.aopjungle.metamodel.commons.AOJMethodDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.util.StringUtil;

@Entity
//@ForeignKey(name="declaremethod_fk")
public class AOJDeclareMethod extends AOJMethodDeclaration {
	public static int seed = 0;
	private int position;
	private String onType;

	protected AOJDeclareMethod() {
	
	}
	
	public AOJDeclareMethod(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
	}

	@Override
	protected void loadPosition() {
		setPosition(nextPosition());
	}

	private int nextPosition() {
		return ++seed;
	}		
	
	public String getMetaName() {
		return "DeclareMethod";
	}
	
	public String getOnType() {
		return onType;
	}
	
	private void setOnType(String onType) {
		this.onType = onType;
	}

	@Override
	protected void loadOnType() {
		String type = ((InterTypeMethodDeclaration)getMethodDeclaration()).getOnType();
		System.out.println("Loading type for declare method on aspect " + type);
		
		if (type.contains(".")) // It is fully qualified
			setOnType(type);
		else
			setOnType(StringUtil.buildTypeHandler(getPackage().getName(),type)); // Uses the same package of the Aspect.
	}
	
	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}	
	
	@Override
	protected void loadHandler() {
		String sourceType = StringUtil.buildTypeHandler(getPackage().getName(), getOwner().getName());
		String targetType = getOnType(); // It should be fullyQualified type here. Fix it!
		String field = getName();
		String handler = StringUtil.buildDeclareMethodHandler(sourceType, targetType, field); 
		setInternalHandler(handler);
		System.out.println("Declare Method handler = " + getInternalHandler());
	}
}
