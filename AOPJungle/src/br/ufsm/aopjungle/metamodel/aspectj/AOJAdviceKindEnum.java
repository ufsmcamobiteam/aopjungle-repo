package br.ufsm.aopjungle.metamodel.aspectj;

public enum AOJAdviceKindEnum {
	BEFORE, 
	AFTER , 
	AFTERTHROWING, 
	AFTERRETURNING, 
	AROUND;

	private String name;
	
	private AOJAdviceKindEnum() {
		this.setName(toString());
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
