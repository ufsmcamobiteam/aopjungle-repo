package br.ufsm.aopjungle.metamodel.aspectj;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.DefaultPointcut;

@Entity
public class AOJPointcutPrimitive  {
	@Id 
    @GeneratedValue
	private long id;

	@Transient	
	private DefaultPointcut pointcutPrimitive;
	
	private String fragment;

	protected AOJPointcutPrimitive () {
		
	}
	
	public String getCode() {
		return getFragment();
	}
	
	public AOJPointcutPrimitive (DefaultPointcut pointcutPrimitive) {
		this.pointcutPrimitive = pointcutPrimitive;
		setFragment(pointcutPrimitive.getDetail());
	}

	/**
	 * @return
	 */
	private String getFragment() {
		return fragment;
	}

	/**
	 * @param fragment
	 */
	private void setFragment(String fragment) {
		this.fragment = fragment;
	}

	/**
	 * @return
	 */
	public DefaultPointcut getPointcutPrimitive() {
		return pointcutPrimitive;
	}

	/**
	 * @return
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}
}
