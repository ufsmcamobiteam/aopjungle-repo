package br.ufsm.aopjungle.metamodel.aspectj;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.CflowPointcut;
import org.hibernate.annotations.ForeignKey;

import br.ufsm.aopjungle.metamodel.commons.AOJMember;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
@DiscriminatorValue("PCC")
public class AOJPointcutCFlow extends AOJMember {
	@XStreamOmitField
	@Transient
	private CflowPointcut cflowPointcut = (CflowPointcut)getNode();
	
	protected AOJPointcutCFlow() {

	}
	
	public AOJPointcutCFlow(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
	}
	
	@Override
	public String getMetaName() {
		return cflowPointcut.isCflowBelow() ?  "Pointcut.CFlowBelow" : "Pointcut.CFlow";
	};
	
	public String getName() {
		return cflowPointcut.getBody().toString();
	}

	@Override
	public int getModifiersAsInteger() {
		return 0;
	}

	protected void loadHandler() {
	};
}
