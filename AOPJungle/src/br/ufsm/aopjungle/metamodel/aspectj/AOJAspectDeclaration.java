package br.ufsm.aopjungle.metamodel.aspectj;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.AspectDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.PointcutDesignator;
import org.aspectj.org.eclipse.jdt.core.dom.SimpleType;
import org.aspectj.org.eclipse.jdt.core.dom.Type;

import br.ufsm.aopjungle.binding.AOJReferenceManager;
import br.ufsm.aopjungle.metamodel.commons.AOJClassHolder;
import br.ufsm.aopjungle.metamodel.commons.AOJContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJContainerPlaceHolder;
import br.ufsm.aopjungle.metamodel.commons.AOJInterfaceble;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJReferencedType;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.metrics.AOJAspectMetrics;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;

/**
 * @author Cristiano De Faveri
 * Federal University of Santa Maria
 * Programming Languages and Database Research Group 
 */
@Entity
@DiscriminatorValue("ASP")
public class AOJAspectDeclaration extends AOJTypeDeclaration implements AOJClassHolder, AOJInterfaceble { 
//	@OneToOne (cascade=CascadeType.ALL)
//	private AOJCommonContainer containerHelper;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJReferencedType> implementedInterfaces;
	
	@OneToOne (cascade=CascadeType.ALL)
	protected AOJAspectMember members;
	
	@OneToOne (cascade=CascadeType.ALL)
	private AOJAspectMetrics metrics; 

	private boolean isPrivileged;
	
	protected AOJAspectDeclaration() {
		
	}
	
	public AOJAspectDeclaration(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
//		containerHelper = new AOJCommonContainer();
		members = new AOJAspectMember();	
		metrics = AOJMetricsFactory.eINSTANCE.createAspectMetrics();
		setPrivileged(getAspectDeclaration().isPrivileged());
	}

	public String getMetaName() {
		return "Aspect";
	}

	/**
	 * @return The original node of the AST
	 */
	public AspectDeclaration getAspectDeclaration() {
		ASTNode node = getNode();
		return (AspectDeclaration)node;
	}

	public boolean isPriviledge() {
		return isPrivileged;
	}

	@Override
	protected void loadMembers() {
		
	}
	
	public AOJAspectMember getMembers() {
		return members;
	}

	protected void loadParent() {
		if (getAspectDeclaration().getSuperclassType() != null) {
			setParent (new AOJReferencedType(getAspectDeclaration().getSuperclassType(), this));
			if (getAspectDeclaration().getSuperclassType().isSimpleType()) {
				String parentFullQualifiedName = ((SimpleType)getAspectDeclaration().getSuperclassType()).getName().getFullyQualifiedName();
				if ( getProject().getBindingMapping().get(parentFullQualifiedName) == null ) {
					AOJContainerPlaceHolder fakeNode = new AOJContainerPlaceHolder(parentFullQualifiedName, getAspectDeclaration().getParent(), null);
					getProject().getBindingMapping().put(parentFullQualifiedName, fakeNode);
				}
			}	
		}	
	}

	public String getASTTypeName() {
		return ((AspectDeclaration)getNode()).getName().toString();
	}

	public String getASTTypeFullQualifiedName() {
		//return getDeclaration().getName().getFullyQualifiedName(); // Unfortunately, it does not work
		StringBuilder sb = new StringBuilder();
		sb.append(getPackage().getName()).append(".").append(getAspectDeclaration().getName());
		//System.out.println("Qualified name : " + sb.toString());
		return sb.toString();

	}

	@Override	
	public int getModifiersAsInteger() {
		return getAspectDeclaration().getModifiers();
	}

	protected void loadImplementedInterfaces() {
		for (Object intf : getAspectDeclaration().superInterfaceTypes()) {
			getImplementedInterfaces().add(new AOJReferencedType((Type)intf, this));
		}
	}

	public List<AOJReferencedType> getImplementedInterfaces() { 
		if (implementedInterfaces == null)
			implementedInterfaces = new ArrayList<AOJReferencedType>();
		return implementedInterfaces;
	}

	public boolean isPrivileged() {
		return isPrivileged;
	}

	private void setPrivileged(boolean isPrivileged) {
		this.isPrivileged = isPrivileged;
	}

	public PointcutDesignator getPerClause() {
		return (PointcutDesignator)getAspectDeclaration().getPerClause();
	}

//	public int getNumberOfAffectedElements() {
//		int result = 0;
//		for (AOJAdviceDeclaration advice : getMembers().getAllAdvices())
//			result+= advice.getNumberOfModulesOut();
//		return result;
//	}

	private int getNumberOfInterTypeDeclarations() {
		return getMembers().getDeclareMethods().size() + getMembers().getDeclareFields().size() 
				+ getMembers().getDeclareParents().size() + getMembers().getDeclareErrors().size()
				+ getMembers().getDeclareWarnings().size()
				+ getMembers().getDeclareSofts().size();
	}

	private int getNumberOfCrossCuttingMembers() {
		int result = 0;
		result += getMembers().getAllAdvices().size();
		result += getMembers().getPointcuts().size();
		result += getNumberOfInterTypeDeclarations();
		return result;
	}
	
	public List<AOJContainer> getAnonymousClasses() {
		return members.getAnonymousClasses();
	}

	public List<AOJContainer> getInnerClasses() {
		return members.getInnerClasses();
	}	

	@Override
	public void loadMetrics() {
		super.loadMetrics();
//		long nOfLines = getMetrics().getNumberOfLines();
//		List<AOJMetrics> members = new ArrayList<AOJMetrics>();
//		getMembers().getMetricParticipants(members);
//		for (AOJMetrics metricParticipant : members) 
//			nOfLines += metricParticipant.getMetrics().getNumberOfLines();
		
		getMetrics().setNumberOfAdvices(getMembers().getAllAdvices().size());
		getMetrics().setNumberOfDeclImplements(getMembers().getDeclareParentsImplementation().size());
		getMetrics().setNumberOfDeclExtends(getMembers().getDeclareParentsExtension().size());
		getMetrics().setNumberOfDeclSoft(getMembers().getDeclareSofts().size());
		getMetrics().setNumberOfDeclMessage(getMembers().getDeclareWarnings().size() + getMembers().getDeclareErrors().size());
		getMetrics().setNumberOfInterField(getMembers().getDeclareFields().size());
		getMetrics().setNumberOfInterMethod(getMembers().getDeclareMethods().size());
		getMetrics().setNumberOfCrossCuttingMembers(getNumberOfCrossCuttingMembers());
		getMetrics().setNumberOfInterTypeDeclarations(getNumberOfInterTypeDeclarations());
		getMetrics().setNumberOfInAffects(getNumberOfInAffects()); 
		getMetrics().setNumberOfOutAffects(getNumberOfOutAffects());
	}

	private long getNumberOfOutAffects() {
		int result = 0;
		for (AOJAdviceDeclaration member: getMembers().getAllAdvices())
			result += member.getMetrics().getNumberOfOutAffects();
		for (AOJDeclareField member: getMembers().getDeclareFields())
			result += member.getMetrics().getNumberOfOutAffects();
		for (AOJDeclareMethod member: getMembers().getDeclareMethods())
			result += member.getMetrics().getNumberOfOutAffects();
		for (AOJDeclareParents member: getMembers().getDeclareParents())
			result += member.getMetrics().getNumberOfOutAffects();
		for (AOJDeclareSoft member: getMembers().getDeclareSofts())
			result += member.getMetrics().getNumberOfOutAffects();
		return result;
	}

	private long getNumberOfInAffects() {
		int result = 0;
		for (AOJAdviceDeclaration member: getMembers().getAllAdvices())
			result += member.getMetrics().getNumberOfInAffects();
		for (AOJDeclareField member: getMembers().getDeclareFields())
			result += member.getMetrics().getNumberOfInAffects();
		for (AOJDeclareMethod member: getMembers().getDeclareMethods())
			result += member.getMetrics().getNumberOfInAffects();
		for (AOJDeclareParents member: getMembers().getDeclareParents())
			result += member.getMetrics().getNumberOfInAffects();
		for (AOJDeclareSoft member: getMembers().getDeclareSofts())
			result += member.getMetrics().getNumberOfInAffects();
		return result;
	}

	public AOJAspectMetrics getMetrics() {
		return metrics;
	}
}
