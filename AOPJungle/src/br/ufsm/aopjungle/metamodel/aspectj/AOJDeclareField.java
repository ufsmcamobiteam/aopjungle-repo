package br.ufsm.aopjungle.metamodel.aspectj;

import javax.persistence.Entity;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.InterTypeFieldDeclaration;

import br.ufsm.aopjungle.binding.AOJReferenceManager;
import br.ufsm.aopjungle.metamodel.commons.AOJAttributeDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.util.StringUtil;

@Entity
//@ForeignKey(name="declarefield_fk")
public class AOJDeclareField extends AOJAttributeDeclaration {
	public static int seed = 0;
	private int position;
	private String onType;

	protected AOJDeclareField() {
	
	}
	
	public AOJDeclareField(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		loadToReferenceMap();
	}
	
	@Override
	protected void loadOnType() {
		setOnType(((InterTypeFieldDeclaration)getAttributeDeclaration()).getOnType());		
	}

	private void setOnType(String onType) {
		this.onType = onType;
	}

	protected void loadPosition() {
		setPosition(nextPosition());
	}

	private int nextPosition() {
		return ++seed;
	}	

	@Override
	public int getModifiersAsInteger() {
		return getAttributeDeclaration().getModifiers();
	}
	
	public String getMetaName() {
		return "DeclareField";
	}
	
	public String getOnType() {
		return onType;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
	
	@Override
	protected void loadHandler() {
		String sourceType = StringUtil.buildTypeHandler(getPackage().getName(), getOwner().getName());
		String targetType = getOnType(); // It should be fullyQualified type here. Fix ! 
		String field = getName();
		String handler = StringUtil.buildDeclareFieldHandler(sourceType, targetType, field); 
		setInternalHandler(handler);
	}
}
