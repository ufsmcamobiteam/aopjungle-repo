package br.ufsm.aopjungle.metamodel.aspectj;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.DeclareDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.PointcutDesignator;
import org.aspectj.org.eclipse.jdt.core.dom.StringLiteral;

import br.ufsm.aopjungle.metamodel.commons.AOJMember;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metrics.AOJCommonMetrics;
import br.ufsm.aopjungle.metrics.AOJMetrics;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@Entity
@DiscriminatorValue("DCE")
public class AOJDeclareCompilationEnforcement extends AOJMember implements AOJMetrics {
	@XStreamOmitField
	@Transient 
	private DeclareDeclaration compilationEnforcementNode = ((DeclareDeclaration)getNode());
	@XStreamOmitField
	@Transient 
	private PointcutDesignator pointcutDesignator;
	@OneToOne(cascade=CascadeType.ALL)		
	@JoinColumn(name="declare_enforcement_fk")
	private AOJPointcutExpression pointcutExpression;
	private String message;
	@OneToOne (cascade=CascadeType.ALL)
	private AOJCommonMetrics metrics; 
	public static int seed = 0;
	private int position;
	
	protected AOJDeclareCompilationEnforcement() {
	
	}
	
	public AOJDeclareCompilationEnforcement(ASTNode node, AOJProgramElement owner, PointcutDesignator pointcutDesignator, StringLiteral message) {
		super(node, owner);
		this.pointcutDesignator = pointcutDesignator;
		this.message = message.toString();
		metrics = AOJMetricsFactory.eINSTANCE.createSimpleMetrics();
		setPointcutExpression(new AOJPointcutExpression(pointcutDesignator));
		loadPosition();		
		loadHandler();		
	}

	protected void loadPosition() {
		setPosition(nextPosition());
	}

	private int nextPosition() {
		return ++seed;
	}		
	
	@Override
	public int getModifiersAsInteger() {
		return compilationEnforcementNode.getModifiers();
	}
	
//	public String toString() {
//		return pointcutDesignator.toString();
//	}

	public void setPointcutDesignator(PointcutDesignator pointcutDesignator) {
		this.pointcutDesignator = pointcutDesignator;
	}

	public PointcutDesignator getPointcutDesignator() {
		return pointcutDesignator;
	}
	
	public String getPointcutAsString() {
		return new AOJPointcutExpression(pointcutDesignator).getCode();
	}

	public String getMessage() {
		return message;
	}

	public AOJPointcutExpression getPointcutExpression() {
		return pointcutExpression;
	}

	private void setPointcutExpression(AOJPointcutExpression pointcutExpression) {
		this.pointcutExpression = pointcutExpression;
	}

	@Override
	public void loadMetrics() {
		metrics.setNumberOfLines(1);
	}

	@Override
	public AOJCommonMetrics getMetrics() {
		return metrics;
	}

	protected void loadHandler() {
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
}
