package br.ufsm.aopjungle.metamodel.aspectj;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.DeclareSoftDeclaration;

import br.ufsm.aopjungle.metamodel.commons.AOJMember;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metrics.AOJCommonMetrics;
import br.ufsm.aopjungle.metrics.AOJMetrics;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;
import br.ufsm.aopjungle.util.StringUtil;

import com.thoughtworks.xstream.annotations.XStreamOmitField;
@Entity
@DiscriminatorValue("DES")
public class AOJDeclareSoft extends AOJMember implements AOJMetrics {
	@XStreamOmitField
	@Transient
	private DeclareSoftDeclaration softNode = ((DeclareSoftDeclaration)getNode());  
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="declaresoft_fk")
	private AOJPointcutExpression pointcutExpression;
	@OneToOne (cascade=CascadeType.ALL)
	private AOJCommonMetrics metrics; 
	private int position;
	private String exceptionPattern;
	
	public AOJDeclareSoft() {

	}
	
	public AOJDeclareSoft(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		metrics = AOJMetricsFactory.eINSTANCE.createSimpleMetrics();
		loadExceptionPatterns();
		loadPointcuts();
		loadPosition();		
		loadHandler();
		loadToReferenceMap();
	}
	
	protected void loadPosition() {
		setPosition(nextPosition());
	}

	private int nextPosition() {
		return ++AOJDeclareParents.seed;
	}	
	
	private void loadPointcuts() {
		pointcutExpression = new AOJPointcutExpression(softNode.getPointcut());
	}

	public String getMetaName() {
		return "DeclareSoft";
	}

	@Override
	public int getModifiersAsInteger() {
		return softNode.getModifiers();
	}

	private void loadExceptionPatterns() {
		exceptionPattern = softNode.getTypePattern().getTypePatternExpression();
	}
	
	
	public String getExceptionPatterns() {
		return exceptionPattern;
	}
	
	public AOJPointcutExpression getPointcut() {
		return pointcutExpression;
	}

	@Override
	public void loadMetrics() {
		getMetrics().setNumberOfLines(1);
		getMetrics().setNumberOfInAffects(getBindingModel().getNumberOfIn());
		getMetrics().setNumberOfOutAffects(getBindingModel().getNumberOfOut());
	}

	@Override
	public AOJCommonMetrics getMetrics() {
		return metrics;
	}

	protected void loadHandler() {
		String sourceType = StringUtil.buildTypeHandler(getPackage().getName(), getOwner().getName());
		String handler = StringUtil.buildDeclareSoftHandler(sourceType, position); 
		setInternalHandler(handler);
		System.out.println("Declare Soft handler = " + getInternalHandler());
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
}
