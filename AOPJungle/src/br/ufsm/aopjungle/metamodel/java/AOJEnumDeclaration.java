package br.ufsm.aopjungle.metamodel.java;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.EnumDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.Type;

import br.ufsm.aopjungle.metamodel.commons.AOJClassHolder;
import br.ufsm.aopjungle.metamodel.commons.AOJConcreteTypeMember;
import br.ufsm.aopjungle.metamodel.commons.AOJContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJInterfaceble;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJReferencedType;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;
import br.ufsm.aopjungle.metrics.AOJTypeMetrics;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@Entity
@DiscriminatorValue("ENU")
public class AOJEnumDeclaration extends AOJTypeDeclaration implements AOJClassHolder, AOJInterfaceble {
	/*
	 * This class should be better if child of AOJClassDeclaration
	 * However, Enum is not part of AjTypeDeclaration then we need to rewrite 
	 * the hierarchy.  
	 * 
	 */
	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJReferencedType> implementedInterfaces;

//	@OneToOne (cascade=CascadeType.ALL)
//	private AOJCommonContainer containerHelper;
	
	@OneToOne (cascade=CascadeType.ALL)
	protected AOJConcreteTypeMember members;
	
	@ElementCollection
	private List<String> constants;

	@OneToOne (cascade=CascadeType.ALL)
	private AOJTypeMetrics metrics; 
	
	protected AOJEnumDeclaration() {
		
	}	
	public AOJEnumDeclaration(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
//		containerHelper = new AOJCommonContainer();
		members = new AOJConcreteTypeMember();
		metrics = AOJMetricsFactory.eINSTANCE.createTypeMetrics();
		loadImplementedInterfaces();
		loadConstants();
	}
	
	private void loadConstants() {
		constants = new ArrayList<String>();
		for (Object e : getEnumDeclaration().enumConstants())
			constants.add(e.toString());
	}
	
	public String getMetaName() {
		return "Enum";
	}
	
	public EnumDeclaration getEnumDeclaration() {
		ASTNode node = getNode();
		return (EnumDeclaration)node;
	}

	protected void loadImplementedInterfaces() {
		for (Object intf : getEnumDeclaration().superInterfaceTypes()) {
			getImplementedInterfaces().add(new AOJReferencedType((Type)intf, this));
		}
	}

	protected void loadParent() {
		// There's no explicity parent on Enums
	}

	public List<AOJReferencedType> getImplementedInterfaces() { 
		if (implementedInterfaces == null)
			implementedInterfaces = new ArrayList<AOJReferencedType>();
		return implementedInterfaces;
	}

	public String getASTTypeName() {
		return getEnumDeclaration().getName().toString();
	}

	public String getASTTypeFullQualifiedName() {
		// getDeclaration().getName().getFullyQualifiedName(); // Unfortunately, it does not work 
		StringBuilder sb = new StringBuilder();
		sb.append(getPackage().getName()).append(".").append(getEnumDeclaration().getName());
		return sb.toString();
	}

	@Override	
	public int getModifiersAsInteger() {
		return getEnumDeclaration().getModifiers();
	}

	public AOJConcreteTypeMember getMembers() {
		return members;
	}

	public List<AOJContainer> getAnonymousClasses() {
		return members.getAnonymousClasses();
	}

	public List<AOJContainer> getInnerClasses() {
		return members.getInnerClasses();
	}
	
	public List<String> getConstants() {
		return constants;
	}
	
	@Override
	protected void loadMembers() {
		
	}
	
	@Override
	public void loadMetrics() {
		super.loadMetrics();
		long lOfConstructors = getMetrics().getNumberOfConstructors() + getMembers().getConstructors().size();
		getMetrics().setNumberOfConstructors(lOfConstructors);
	}
	
	@Override
	public AOJTypeMetrics getMetrics() {
		return metrics;
	}
}
