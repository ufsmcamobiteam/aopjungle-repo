package br.ufsm.aopjungle.metamodel.java;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.AjTypeDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.SimpleType;
import org.h2.index.FunctionIndex;

import br.ufsm.aopjungle.metamodel.commons.AOJCommonTypeMember;
import br.ufsm.aopjungle.metamodel.commons.AOJContainerPlaceHolder;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJReferencedType;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;
import br.ufsm.aopjungle.metrics.AOJTypeMetrics;
import br.ufsm.aopjungle.util.AOJClassLoader;

@Entity
@DiscriminatorValue("INT")
public class AOJInterfaceDeclaration extends AOJTypeDeclaration {
	@OneToOne (cascade=CascadeType.ALL)
	protected AOJCommonTypeMember members;
	@OneToOne (cascade=CascadeType.ALL)
	private AOJTypeMetrics metrics; 
	
	private Boolean functionalInterface = null;
	private Boolean innerClass = null;
	private String unitName = null;
	
	public static void main(String[] args){
		try {
			Class<?> c = Class.forName("br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration");
			System.out.println(c.getSuperclass().getName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	protected AOJInterfaceDeclaration() {
		
	}
	
	@Override	
	public int getModifiersAsInteger() {
		return getInterfaceDeclaration().getModifiers();
	}
	
	public String getASTTypeName() {
		return getInterfaceDeclaration().getName().toString();
	}

	public String getASTTypeFullQualifiedName() {
		StringBuilder sb = new StringBuilder();
		sb.append(getPackage().getName()).append(".").append(getInterfaceDeclaration().getName());
		return sb.toString();

	}

	public AOJInterfaceDeclaration(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		members = new AOJCommonTypeMember();
		metrics = AOJMetricsFactory.eINSTANCE.createTypeMetrics();
		isFunctionalInterface();
	}

	public String getMetaName() {
		return "Interface";
	}

	protected void loadParent() {
		// Loads only first level of superinterfaces
		if (getInterfaceDeclaration().getSuperclassType() != null) {
			setParent (new AOJReferencedType(getInterfaceDeclaration().getSuperclassType(), this));
			if (getInterfaceDeclaration().getSuperclassType().isSimpleType()) {
				String parentFullQualifiedName = ((SimpleType)getInterfaceDeclaration().getSuperclassType()).getName().getFullyQualifiedName();
				if ( getProject().getBindingMapping().get(parentFullQualifiedName) == null ) {
					AOJContainerPlaceHolder fakeNode = new AOJContainerPlaceHolder(parentFullQualifiedName, getInterfaceDeclaration().getParent(), null);
					getProject().getBindingMapping().put(parentFullQualifiedName, fakeNode);
				}
			}	
	
		}	
//		if ( getDeclaration().superInterfaceTypes().size() > 0 )
//			setParent (new AOJReferencedType ( (Type)(getDeclaration().superInterfaceTypes().get(0))));		
	}

	public AOJCommonTypeMember getMembers() {
		return members;
	}

	public void setMembers(AOJCommonTypeMember members) {
		this.members = members;
	}

	@Override
	protected void loadMembers() {
		//setMembers(new AOJCommonTypeMember());
	}
	
	public AjTypeDeclaration getInterfaceDeclaration() {
		ASTNode node = getNode();
		return (AjTypeDeclaration)node;
	}

	@Override
	public void loadMetrics() {
		super.loadMetrics();
		// Other specific metrics here
	}

	@Override
	public AOJTypeMetrics getMetrics() {
		return metrics;
	}

	public Boolean isFunctionalInterface() {
		if (functionalInterface == null){
			try {

				
				if (this.getFullQualifiedName().contains("PropertyEvaluator")) {
					System.out.println(this.getFullQualifiedName()+" - "+this.getOwner().getName());
					
					//Class<?> c = Class.forName("javax.media.jai.PlanarImage");
					//System.out.println(c.isInterface());
				}
				System.out.println(this.getFullQualifiedName()+"  -  "+this.getOwner().getName());
				
				functionalInterface = false;
				
				Class<?> c = getObjectClass();
				
				if (c == null)
					return functionalInterface; 
				
				if  (c.getMethods() != null)
					functionalInterface = c.getMethods().length == 1;
	
				
			} catch (ClassNotFoundException e ) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return functionalInterface;
	}
	
	private Class <?> getObjectClass() throws ClassNotFoundException{
		
		if (isInnerClass()){
			
			String pkg = this.getFullQualifiedName().substring(0, this.getFullQualifiedName().indexOf(this.getName()));
			Class<?> c = AOJClassLoader.forName(pkg+getUnitName());
			
			for (Class<?> c1 : c.getClasses()) {
				if (this.getName().equals(c1.getSimpleName()))
					return c1;
			}
			
			return null;
			
		}else 
			return AOJClassLoader.forName(this.getFullQualifiedName());

	}
	
	public Boolean isInnerClass(){
		// if interface name is different of the unitName then is a inner class
		if (innerClass==null)
			innerClass = !this.getName().equals(getUnitName());
		return innerClass;
	}
	
	public String getUnitName(){
		// getting the name of the compilation unit
		if (unitName == null)
			unitName = this.getOwner().getName().substring(0, this.getOwner().getName().indexOf("."));
		return unitName;
	}

}
