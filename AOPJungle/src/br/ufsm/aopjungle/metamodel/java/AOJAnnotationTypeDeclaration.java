package br.ufsm.aopjungle.metamodel.java;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.AjTypeDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.SimpleType;

import br.ufsm.aopjungle.metamodel.commons.AOJCommonTypeMember;
import br.ufsm.aopjungle.metamodel.commons.AOJContainerPlaceHolder;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJReferencedType;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;
import br.ufsm.aopjungle.metrics.AOJTypeMetrics;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */

// TODO: Recall this class is very similar to Interface Declaration
//       unless atribute declaration that is not permited here
//       For the sake of simplicity we kept the same shape for both
//       then it could perhaps be inherited from AOJInterfaceDeclaration
@Entity
@DiscriminatorValue("ANO")
public class AOJAnnotationTypeDeclaration extends AOJTypeDeclaration {
	@OneToOne (cascade=CascadeType.ALL)
	protected AOJCommonTypeMember members;
	@OneToOne (cascade=CascadeType.ALL)
	private AOJTypeMetrics metrics; 

	public AOJAnnotationTypeDeclaration() {
	
	}

	public AOJAnnotationTypeDeclaration(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		members = new AOJCommonTypeMember();
		metrics = AOJMetricsFactory.eINSTANCE.createTypeMetrics();
	}
	
	@Override	
	public int getModifiersAsInteger() {
		return getAOJAnnotationTypeDeclaration().getModifiers();
	}
	
	public String getASTTypeName() {
		return getAOJAnnotationTypeDeclaration().getName().toString();
	}

	public String getASTTypeFullQualifiedName() {
		StringBuilder sb = new StringBuilder();
		sb.append(getPackage().getName()).append(".").append(getAOJAnnotationTypeDeclaration().getName());
		return sb.toString();
	}	
	
	@Override
	public AOJCommonTypeMember getMembers() {
		return members;
	}

	@Override
	protected void loadMembers() {
		// Not required here
	}

	@Override
	protected void loadParent() {
		// Loads only first level of superinterfaces
		if (getAOJAnnotationTypeDeclaration().getSuperclassType() != null) {
			setParent (new AOJReferencedType(getAOJAnnotationTypeDeclaration().getSuperclassType(), this));
			if (getAOJAnnotationTypeDeclaration().getSuperclassType().isSimpleType()) {
				String parentFullQualifiedName = ((SimpleType)getAOJAnnotationTypeDeclaration().getSuperclassType()).getName().getFullyQualifiedName();
				if ( getProject().getBindingMapping().get(parentFullQualifiedName) == null ) {
					AOJContainerPlaceHolder fakeNode = new AOJContainerPlaceHolder(parentFullQualifiedName, getAOJAnnotationTypeDeclaration().getParent(), null);
					getProject().getBindingMapping().put(parentFullQualifiedName, fakeNode);
				}
			}
		}	
	}

	@Override
	public void loadMetrics() {
		super.loadMetrics();
		// Other specific metrics here
	}
	
	public AjTypeDeclaration getAOJAnnotationTypeDeclaration() {
		ASTNode node = getNode();
		return (AjTypeDeclaration)node;
	}

	public String getMetaName() {
		return "@Annotation";
	}

	@Override
	public AOJTypeMetrics getMetrics() {
		return metrics;
	}

}
