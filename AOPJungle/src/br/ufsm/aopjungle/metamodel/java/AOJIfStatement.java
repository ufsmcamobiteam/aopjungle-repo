package br.ufsm.aopjungle.metamodel.java;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.IfStatement;

import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJStatement;

public class AOJIfStatement extends AOJStatement {

	private String code;
	
	
	public AOJIfStatement(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		IfStatement e = (IfStatement)node;
		setCode(e.toString());
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}