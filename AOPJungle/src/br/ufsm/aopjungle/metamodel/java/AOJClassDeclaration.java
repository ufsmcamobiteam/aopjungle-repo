package br.ufsm.aopjungle.metamodel.java;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.Type;
import org.aspectj.org.eclipse.jdt.core.dom.TypeDeclaration;

import br.ufsm.aopjungle.metamodel.commons.AOJClassHolder;
import br.ufsm.aopjungle.metamodel.commons.AOJConcreteTypeMember;
import br.ufsm.aopjungle.metamodel.commons.AOJContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJInterfaceble;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJReferencedType;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;
import br.ufsm.aopjungle.metrics.AOJTypeMetrics;

/**
 * @author Cristiano De Faveri <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br> 
 */
@Entity
@DiscriminatorValue("CLA")
public class AOJClassDeclaration extends AOJTypeDeclaration implements AOJClassHolder, AOJInterfaceble {
	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJReferencedType> implementedInterfaces;

//	@OneToOne (cascade=CascadeType.ALL)
//	private AOJCommonContainer containerHelper;
	
	@OneToOne (cascade=CascadeType.ALL)
	protected AOJConcreteTypeMember members;
	@OneToOne (cascade=CascadeType.ALL)
	private AOJTypeMetrics metrics; 
	
	protected AOJClassDeclaration() {
		
	}
	
	public AOJClassDeclaration(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
//		containerHelper = new AOJCommonContainer();
		members = new AOJConcreteTypeMember();
		metrics = AOJMetricsFactory.eINSTANCE.createTypeMetrics();
		loadImplementedInterfaces();
	}

	protected void loadImplementedInterfaces() {
		for (Object intf : getClassDeclaration().superInterfaceTypes()) {
			getImplementedInterfaces().add(new AOJReferencedType((Type)intf, this));
		}
	}

	protected void loadParent() {
		//System.out.println("Load Parent type : " + getDeclaration().getName() + ":: superType : " + getDeclaration().getSuperclassType());
		if (null != getClassDeclaration().getSuperclassType()) {
//			System.out.println(getClassDeclaration().getTypes());
			setParent ( new AOJReferencedType(getClassDeclaration().getSuperclassType(), this) );
//			// Save parent on binding map (extends can be from an API outside the system); Create a fake node to keep the compatibility
//			// TODO : Improve, if it is not SimpleType ?
//			if (getClassDeclaration().getSuperclassType().isSimpleType()) {
//				String parentFullQualifiedName = ((SimpleType)getClassDeclaration().getSuperclassType()).getName().getFullyQualifiedName();
//				if ( getProject().getBindingMapping().get(parentFullQualifiedName) == null ) {
//					AOJContainerPlaceHolder fakeNode = new AOJContainerPlaceHolder(parentFullQualifiedName, getClassDeclaration().getParent(), null);
//					getProject().getBindingMapping().put(parentFullQualifiedName, fakeNode);
//				}	
//			}	
		}	
	}

	public List<AOJReferencedType> getImplementedInterfaces() { 
		if (implementedInterfaces == null)
			implementedInterfaces = new ArrayList<AOJReferencedType>();
		return implementedInterfaces;
	}

	public String getASTTypeName() {
		return getClassDeclaration().getName().toString();
	}

	public String getASTTypeFullQualifiedName() {
		// getDeclaration().getName().getFullyQualifiedName(); // Unfortunately, it does not work 
		StringBuilder sb = new StringBuilder();
		sb.append(getPackage().getName()).append(".").append(getClassDeclaration().getName());
		//System.out.println("Qualified name : " + sb.toString());
		return sb.toString();
	}

	@Override	
	public int getModifiersAsInteger() {
		return getClassDeclaration().getModifiers();
	}

	@Override
	protected void loadMembers() {
		//setMembers(new AOJConcreteTypeMember());
	}
	
	public AOJConcreteTypeMember getMembers() {
		return members;
	}

	public List<AOJContainer> getAnonymousClasses() {
		return members.getAnonymousClasses();
	}

	public List<AOJContainer> getInnerClasses() {
		return members.getInnerClasses();
	}
	
	public TypeDeclaration getClassDeclaration() {
		ASTNode node = getNode();
		return (TypeDeclaration)node;
	}

	public String getMetaName() {
		return "Class";
	}

	@Override
	public void loadMetrics() {
		super.loadMetrics();
		long lOfConstructors = getMetrics().getNumberOfConstructors() + getMembers().getConstructors().size();
		getMetrics().setNumberOfConstructors(lOfConstructors);
	}

	@Override
	public AOJTypeMetrics getMetrics() {
		return metrics;
	}
}