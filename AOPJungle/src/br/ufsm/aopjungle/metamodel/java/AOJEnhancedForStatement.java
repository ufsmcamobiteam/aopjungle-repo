package br.ufsm.aopjungle.metamodel.java;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.EnhancedForStatement;

import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJStatement;


public class AOJEnhancedForStatement extends AOJStatement {

	private String code;
	private String body;

	public AOJEnhancedForStatement(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		EnhancedForStatement e = (EnhancedForStatement)node;
		setCode(e.toString());
		setBody(e.getBody().toString());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
}



