package br.ufsm.aopjungle.metamodel.java;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.AnonymousClassDeclaration;

import br.ufsm.aopjungle.metamodel.commons.AOJCommonContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJCompilationUnit;
import br.ufsm.aopjungle.metamodel.commons.AOJConcreteTypeMember;
import br.ufsm.aopjungle.metamodel.commons.AOJContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJImportDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.metrics.AOJMetricsFactory;
import br.ufsm.aopjungle.metrics.AOJTypeMetrics;
import br.ufsm.aopjungle.util.AOJClassLoader;

/**
 * @author Cristiano De Faveri
 * 
 * This class represents anonymous classes in the system
 * I really do not know why AnonymousClassDeclaration is not a TypeDeclaration
 * since it could be represented by it.
 * 
 * AnonymousClassDeclaration descends from ASTNode thus 
 * there's not so much to do it
 * 
 */

@Entity
@DiscriminatorValue("ANN")
public class AOJAnonymousClassDeclaration extends AOJTypeDeclaration  {

	/**
	 * The list of declared annotations for this type This field should be in an upper type. However due to hibernate limitations in save polymorphic list associations we decide to replicate this field in types since we have few of them.
	 */

	@OneToOne (cascade=CascadeType.ALL)
	@JoinColumn(name = "anonymousclass_fk")
	private AOJCommonContainer containerHelper;
	
	@OneToOne (cascade=CascadeType.ALL)
	@JoinColumn(name = "aclass_member_fk")
	protected AOJConcreteTypeMember members;
	@OneToOne (cascade=CascadeType.ALL)
	private AOJTypeMetrics metrics; 
	
	private Boolean functionalInterface = Boolean.FALSE;
	private Boolean _interface = Boolean.FALSE;
	private String code;
	private String instanceTypeName;


	protected AOJAnonymousClassDeclaration() {
		
	}
	
	public AOJAnonymousClassDeclaration(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		//AnonymousClassDeclaration n = (AnonymousClassDeclaration) node;
		String simpleName = getSimpleName(node.getParent().toString());
		setInstanceTypeName(simpleName);
		setCode(node.getParent().getParent().getParent().toString());
		containerHelper = new AOJCommonContainer();
		members = new AOJConcreteTypeMember();
		metrics = AOJMetricsFactory.eINSTANCE.createTypeMetrics();
		Class<?> clazz = getObjectClass();
		
		if (clazz != null){
			setInterface(clazz.isInterface());
			setFunctionalInterface(verifyingFunctionalInterface(clazz));
		}
		
	}
	
	private boolean verifyingFunctionalInterface(Class clazz) {
		int  numberOfMethods = 0;
		if (! isInterface() )
			return false;
		if (clazz.getMethods().length == 0)
			return false;
		
		for (Method m : clazz.getMethods()){
			if (m.getName().equalsIgnoreCase("equals") && m.getReturnType().getName().equalsIgnoreCase("boolean")){
				
			} else 	if (Modifier.isStatic(m.getModifiers())) {
				
			} else			
				numberOfMethods++; 
		}
		if (numberOfMethods != 1)
			return false;
		
		return true;
		

	}
	
	private Class<?> getObjectClass(){
		String fullName = getFullyQualifiedNameFromImport(getInstanceTypeName());
		try {
			return Class.forName(fullName);
		} catch (ClassNotFoundException e) {
			try {
				return AOJClassLoader.forName(fullName);
			} catch (ClassNotFoundException e1) {
				return null;
			}

		}
	}
	
	public Boolean isInterface(){
		return _interface;
	}
	
	public void setInterface(Boolean bool){
		_interface = bool;
	}
	
	private String getSimpleName(String code){
		code = code.replaceAll("\\(", " "); // tirando os parenteses
		code = code.replaceAll("\\<", " "); // tirando os parenteses
		code = code.replaceAll("\\>", " "); // tirando os parenteses
		code = code.replaceAll("\\s+"," "); // tirando espaços em branco entre as palavras
		
		int cont = 0; 
		for (String s : code.split(" ")) {
			cont++;
			if (s.equals("new"))
				return code.split(" ")[cont];
		}
			
		return "";
	}

	
	private String getFullyQualifiedNameFromImport(String name) {
		String fullQualifiedName;
		AOJCompilationUnit cu = getCompilationUnit(getOwner());
		if (null != cu)
			for (AOJImportDeclaration imp : cu.getImportsDeclaration()) {
				if (imp.getCode().indexOf(name) != -1)
					return (imp.qualifiedName());	
				fullQualifiedName = findByWildcard(imp, name);
				if (! fullQualifiedName.isEmpty())
					return fullQualifiedName;
			}
		
		fullQualifiedName = findAtJavaLang(name);
		if (! fullQualifiedName.isEmpty())
			return fullQualifiedName;
		
		return getPackage().getName() + "." + name; // If no type was found, the extends type is located at the same package
	}
	
	private String findAtJavaLang(String name) {
		String fullQualifiedName = "java.lang." + name;
		if (forClass (fullQualifiedName))  
			return fullQualifiedName;		
		return "";
	}
	
	private String findByWildcard(AOJImportDeclaration imp, String name) {
		if (imp.getCode().indexOf("*") != -1) { // there's not * on import declaration
			String fullQualifiedName = imp.qualifiedName().replace("*", name); // replace * by type name
			if (forClass (fullQualifiedName)) // try to find by classloader 
				return fullQualifiedName;
		}	

		return "";
	}

	private boolean forClass(String fullQualifiedName) {
		try {
			Class.forName(fullQualifiedName);
			return true;
		} catch (ClassNotFoundException e) {
			return false;
		}
	}
	
	private AOJCompilationUnit getCompilationUnit(AOJProgramElement owner) {
		if (owner instanceof AOJCompilationUnit)
			return (AOJCompilationUnit)owner;
		if (owner instanceof AOJProject)
			return null;
		return getCompilationUnit(owner.getOwner());
	}

	protected void loadParent() {
		// no parent		
	}

	public String getASTTypeName() {
		return "Anonymous";
	}

	public String getASTTypeFullQualifiedName() {
		return "Anonymous";
	}

	@Override	
	public int getModifiersAsInteger() {
		return 0;
	}

	@Override
	protected void loadMembers() {
		//setMembers(new AOJConcreteTypeMember());
	}
	
	/**
	 * @return
	 */
	public AOJConcreteTypeMember getMembers() {
		return members;
	}

//	public List<AOJAnnotationDeclaration> getAnnotations() {
//		return containerHelper.getAnnotations();
//	}

	public List<AOJContainer> getAnonymousClasses() {
		return containerHelper.getAnonymousClasses();
	}

	public List<AOJContainer> getInnerClasses() {
		return containerHelper.getInnerClasses();
	}
	 
	/**
	 * @return the ASTNode cast to AnonymousClassDeclaration
	 * This is the difference between a anonymous class and a regular class
	 */
	public AnonymousClassDeclaration getAnonymousClassDeclaration() {
		return (AnonymousClassDeclaration) getNode();
	}

	public String getMetaName() {
		return "Class";
	}
	
	@Override
	public void loadMetrics() {
		super.loadMetrics();
		// Other specific metrics here
	}

	@Override
	public AOJTypeMetrics getMetrics() {
		return metrics;
	}
	
	public Boolean isFunctionalInterface() {
		return functionalInterface;
	}

	public void setFunctionalInterface(Boolean functionalInterface) {
		this.functionalInterface = functionalInterface;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getInstanceTypeName() {
		return instanceTypeName;
	}

	public void setInstanceTypeName(String instanceTypeName) {
		this.instanceTypeName = instanceTypeName;
	}

}
