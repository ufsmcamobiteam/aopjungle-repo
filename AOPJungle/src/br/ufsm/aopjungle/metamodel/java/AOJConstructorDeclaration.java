package br.ufsm.aopjungle.metamodel.java;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.MethodDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.Name;
import org.aspectj.org.eclipse.jdt.core.dom.SingleVariableDeclaration;

import br.ufsm.aopjungle.metamodel.commons.AOJBehaviourKind;
import br.ufsm.aopjungle.metamodel.commons.AOJClassHolder;
import br.ufsm.aopjungle.metamodel.commons.AOJException;
import br.ufsm.aopjungle.metamodel.commons.AOJParameter;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.util.AOJSourceService;
import br.ufsm.aopjungle.util.StringUtil;

@Entity
@DiscriminatorValue("CTR")
public class AOJConstructorDeclaration extends AOJBehaviourKind implements AOJClassHolder {
	protected AOJConstructorDeclaration() {
		
	}

	@Override
	public String getMetaName() {
		return "Constructor";
	}
	
	public AOJConstructorDeclaration(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		loadHandler();
		loadToReferenceMap();
		
//		loadBindings(AOJBindingFacade.getInstance());
//		containerHelper = new AOJCommonContainer();		
	}

//	private void loadBindings(AOJBindingFacade instance) {
//		// TODO Auto-generated method stub
//	}

	protected void loadParameters() {
		for (Object param : getConstructorDeclaration().parameters()) 
			getParameters().add( new AOJParameter((SingleVariableDeclaration) param, this));
	}

	@Override
	protected void loadName() {
		super.setName(getConstructorDeclaration().getName().toString());
	}

	@Override
	public int getModifiersAsInteger() {
		return ((MethodDeclaration) getNode()).getModifiers();
	}
	
	protected MethodDeclaration getConstructorDeclaration() {
		ASTNode node = getNode();
		return (MethodDeclaration)node;
	}

	@Override
	protected void loadReturnType() {
		setReturnType(null);
	}

	@Override
	protected void loadThrownExceptions() {
		for (Object exception : getConstructorDeclaration().thrownExceptions()) 
			getThrownExceptions().add( new AOJException((Name) exception, this));
	}

	@Override
	protected void loadCodeBody() {
		setCode(getConstructorDeclaration().getBody().toString());
	}

	@Override
	public void loadMetrics() {
		getMetrics().setNumberOfStatements(getConstructorDeclaration().getBody() == null ? 0 : 
			AOJSourceService.getNumberOfStatements(getConstructorDeclaration().getBody().statements()));
		long nOfLines = 0;
		nOfLines = getConstructorDeclaration().getBody() == null ? 0 : 
			AOJSourceService.getNumberOfStatements(getConstructorDeclaration().getBody().statements());
		nOfLines += 3; // Add open/close curly brackets	+ header	
		getMetrics().setNumberOfLines(nOfLines);
	}

	@Override
	protected void loadHandler() {
		setInternalHandler(StringUtil.buildHandler(getPackage().getName(), ((AOJTypeDeclaration)getOwner()).getName(), getSignature()));
	}
}
