package br.ufsm.aopjungle.hql;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Transient;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import br.ufsm.aopjungle.aojql.console.AOJQLQuery;
import br.ufsm.aopjungle.aojql.console.AOJQLTreeNode;
import br.ufsm.aopjungle.aojql.console.AOJQLTreeParent;
import br.ufsm.aopjungle.aojql.console.AOJResultSet;
import br.ufsm.aopjungle.exception.AOJQueryFailException;
import br.ufsm.aopjungle.log.AOJLogger;

public class AOJHQLEngine implements AOJQLQuery {
	private Session session;
	private AOJResultSet resultSet; 
	private static final String BANNER = 
			"\n**********************************************"
          + "\n***** Federal University of Santa Maria  *****"		  
		  + "\n***** HQL Console - V0.1                 *****"
		  + "\n**********************************************";
	private static final String CURSOR = "\nHQL> ";
	
	public AOJHQLEngine (SessionFactory sessionFactory) {
		//sessionFactory.close();
		session = sessionFactory.openSession();
		resultSet = new AOJResultSet();	
	}
	
	private void prepareResults (Query query) {
		resultSet.clear();
		List<Object> resultHQLQuery = query.list();
		Iterator<Object> it = resultHQLQuery.iterator();
		
		// Fill values
		while(it.hasNext()) {
			  Object obj1 = it.next();			  
			  if (obj1 instanceof Object[]) 
				  for (Object obj2 : (Object[])obj1) 
					  loadValues(obj2);
			  else    
				  loadValues(obj1);
		}	
	}

	private boolean isComplex(Object obj1) {
		return !(obj1 instanceof Boolean) && !(obj1 instanceof Number) && !(obj1 instanceof String); 
	}

	private void loadValues (Object obj) {
		if (! isComplex(obj)) 
			resultSet.getRoot().addChild(new AOJQLTreeNode(String.format("%s(%s)", "Primitive",obj.toString())));
		else {
			AOJQLTreeParent node = new AOJQLTreeParent(obj.getClass().getName());
			resultSet.getRoot().addChild(node);
			List<Field> fields = getFields(obj);
			for (Field field : fields) {
				try {
					node.addChild(new AOJQLTreeNode(String.format("%s(%s)", field.getName() , field.get(obj))));
				} catch (IllegalArgumentException e) {
					AOJLogger.getLogger().error(e);
				} catch (IllegalAccessException e) {
					AOJLogger.getLogger().error(e);
				}
			}
		}	
	}

	private List<Field> getFields(Object obj) {
		return getAllFields(obj.getClass());
	}
	
	public List<Field> getAllFields(Class<?> type) {
		List<Field> result = new ArrayList<Field>();
	    for (Field field: type.getDeclaredFields()) {
	    	field.setAccessible(true);
	    	if (field.getAnnotation(Transient.class) == null) 
	    		result.add(field);
	    	}
	    if (type.getSuperclass() != null) 
	        result.addAll(getAllFields(type.getSuperclass()));
	    return result;
	}

	@Override
	public AOJResultSet execute(String query, List objectList) throws AOJQueryFailException {
		try {
			Query qry = session.createQuery(query);
			prepareResults(qry);
		} catch (HibernateException e) {
			throw new AOJQueryFailException(e);
		}	
		return resultSet;
	}

	public String getCursor() {
		return CURSOR;
	}

	public String getBanner() {
		return BANNER;
	}

	@Override
	public AOJResultSet execute(String query) throws AOJQueryFailException {
		return execute (query, null);
	}
}
