package br.ufsm.aopjungle.hql;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import br.ufsm.aopjungle.aojql.console.AOJQLEngineFactory;
import br.ufsm.aopjungle.aojql.console.AOJResultSetView;
import br.ufsm.aopjungle.exception.AOJQueryFailException;
import br.ufsm.aopjungle.util.HibernateUtil;
 
public class AOJHQLConsole { 
	
	private String banner = "\n***************************************************"
						  + "\n*****    HQL Console for AOPJungle Model      *****"
						  + "\n***************************************************";
	private String cursor = "\n<HQLQuery -> "; 
	
	private AOJHQLEngine engine;
	private AOJResultSetView resultSetView;
			
	public AOJHQLConsole(AOJResultSetView view) {
		this.resultSetView = view;
		engine = AOJQLEngineFactory.createHQLEngine(HibernateUtil.getSessionFactory());
		System.out.println(banner);
		cursor();
	}
	
	public void cursor() {
		boolean loop = true;
		
		while (loop) {
			System.out.print(cursor);
			InputStreamReader reader = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(reader);
			try {
				String query = br.readLine();
				loop = !(query.equalsIgnoreCase("quit"));
				if (loop) {
					try {
						resultSetView.print(engine.execute(query));
					} catch (AOJQueryFailException e) {
						System.out.println("Query Error : \n Cause : " + e.getCause().getMessage());
					}
				}	
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("Bye !");
	}
}
