package br.ufsm.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import br.ufsm.aopjungle.util.AOJSourceService;

public class AOJSourceServiceTest {

	@Test
	public void testGetNumberOfLinesReader() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetNumberOfLinesCharArray() {
		assertEquals(2, AOJSourceService.getNumberOfLines("aa \n bb \r".toCharArray()));
		assertEquals(0, AOJSourceService.getNumberOfLines("\r".toCharArray()));
		assertEquals(0, AOJSourceService.getNumberOfLines("\n\r".toCharArray()));
		assertEquals(0, AOJSourceService.getNumberOfLines("\n\n \r".toCharArray()));
		assertEquals(0, AOJSourceService.getNumberOfLines(" \n \n \r".toCharArray()));
		assertEquals(1, AOJSourceService.getNumberOfLines("\n aa \n \r".toCharArray()));
		assertEquals(10, AOJSourceService.getNumberOfLines(
				("for(int i = 0; i < GAMESIZEY; i++) {\n" +
				"boolean complete = true;\n" +
				"for(int j = 0; j < GAMESIZEX; j++) {\n" +
				"if(gameBoard[j][i] == Blocks.EMPTY)\n" +
				"complete = false;\n" +
				"}\n" +
				"if(complete) {\n" +
				"blocks.deleteLine(i, gameBoard);\n" +
				"}\n" +
				"}\n")	
				.toCharArray()));
	}

}
